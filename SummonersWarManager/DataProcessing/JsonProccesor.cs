﻿using Framework;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataProcessing {

	public class JsonProcessor {

		public enum RuneKeys {
			[Description("occupied_type")]
			OCCUPIED_TYPE,

			[Description("extra")]
			EXTRA,

			[Description("sell_value")]
			SELL_VALUE,

			[Description("pri_eff")]
			PRI_EFF,

			[Description("prefix_eff")]
			PREFIX_EFF,

			[Description("slot_no")]
			SLOT_NO,

			[Description("rank")]
			RANK,

			[Description("occupied_id")]
			OCCUPIED_ID,

			[Description("sec_eff")]
			SEC_EFF,

			[Description("wizard_id")]
			WIZARD_ID,

			[Description("upgrade_curr")]
			UPGRADE_CURR,

			[Description("rune_id")]
			RUNE_ID,

			[Description("base_value")]
			BASE_VALUE,

			[Description("class")]
			CLASS,

			[Description("set_id")]
			SET_ID,

			[Description("upgrade_limit")]
			UPGRADE_LIMIT
		}

		
		private string _jsonData;

		public JsonProcessor(string jsonData) {

			_jsonData = jsonData;
			
		}

		public List<Rune> ProcessAllRunes() {
			dynamic deserializedData = JsonConvert.DeserializeObject(_jsonData);
			return ParseAllRunesFromUnknownJsonObject(deserializedData);
		}

		private List<Rune> ParseAllRunesFromUnknownJsonObject(dynamic jsonObject, List<Rune> parsedRunes = null) {

			if (parsedRunes == null) {
				parsedRunes = new List<Rune>();
			}

			Rune rune = null;

			if (jsonObject is JObject) {
				rune = GetRuneFromJsonObject(jsonObject);
			}

			if (rune != null) {
				parsedRunes.Add(rune);
			} else if (jsonObject is JObject || jsonObject is JArray || jsonObject is JProperty) {
				foreach (var child in jsonObject) {
					ParseAllRunesFromUnknownJsonObject(child, parsedRunes);
				}
			}

			return parsedRunes;

		}

		private Rune GetRuneFromJsonObject(JObject jsonObject) {

			var properties = jsonObject.Properties();
			foreach (RuneKeys key in Enum.GetValues(typeof(RuneKeys))) {
				if (!properties.Any(p => p.Name.Equals(key.Description(), StringComparison.CurrentCultureIgnoreCase))) {
					return null;
				}
			}

			int occupiedType = jsonObject.Value<int>(RuneKeys.OCCUPIED_TYPE.Description());
			int extra = jsonObject.Value<int>(RuneKeys.EXTRA.Description());
			int sellValue = jsonObject.Value<int>(RuneKeys.SELL_VALUE.Description());

			int[] priEff = new int[2];
			dynamic jsonPriEff = jsonObject.GetValue(RuneKeys.PRI_EFF.Description());
			priEff[0] = jsonPriEff[0];
			priEff[1] = jsonPriEff[1];

			int[] prefixEff = new int[2];
			dynamic jsonPrefixEff = jsonObject.GetValue(RuneKeys.PREFIX_EFF.Description());
			prefixEff[0] = jsonPrefixEff[0];
			prefixEff[1] = jsonPrefixEff[1];

			int slotNo = jsonObject.Value<int>(RuneKeys.SLOT_NO.Description());
			int rank = jsonObject.Value<int>(RuneKeys.RANK.Description());
			long occupiedId = jsonObject.Value<long>(RuneKeys.OCCUPIED_ID.Description());

			int[,] effects = new int[4, 4];
			var i = 0;
			dynamic jsonEffects = jsonObject.GetValue(RuneKeys.SEC_EFF.Description());
			foreach (var effect in jsonEffects) {
				for (var j = 0; j < 4; j++) {
					effects[i, j] = effect[j];
				}
				i++;
			}

			long wizardId = jsonObject.Value<long>(RuneKeys.WIZARD_ID.Description());
			int upgradeCurr = jsonObject.Value<int>(RuneKeys.UPGRADE_CURR.Description());
			long runeId = jsonObject.Value<long>(RuneKeys.RUNE_ID.Description());
			int baseValue = jsonObject.Value<int>(RuneKeys.BASE_VALUE.Description());
			int classValue = jsonObject.Value<int>(RuneKeys.CLASS.Description());
			int setId = jsonObject.Value<int>(RuneKeys.SET_ID.Description());
			int upgradeLimit = jsonObject.Value<int>(RuneKeys.UPGRADE_LIMIT.Description());

			var rune = new Rune {
				OccupiedType = occupiedType,
				Extra = extra,
				SellValue = sellValue,
				PrimaryEffect = new RuneEffect {
					Stat = (RuneEnums.StatType)priEff[0],
					Value = priEff[1]
				},
				Slot = slotNo,
				Rank = rank,
				OccupiedId = occupiedId,
				WizardId = wizardId,
				CurrentLevel = upgradeCurr,
				Id = runeId,
				BaseValue = baseValue,
				Stars = classValue,
				Set = (RuneEnums.SetType)setId,
				MaxLevel = upgradeLimit
			};

			if (prefixEff[0] > 0) {
				rune.StaticEffect = new RuneEffect {
					Stat = (RuneEnums.StatType)prefixEff[0],
					Value = prefixEff[1]
				};
			}

			for (var k = 0; k < effects.GetLength(0); k++) {
				if (effects[k, 0] > 0) {
					rune.AdditionalEffects[k] = new RuneEffect {
						Stat = (RuneEnums.StatType)effects[k, 0],
						Value = effects[k, 1]
					};
				}
			}

			return rune;


		}


	}
}
