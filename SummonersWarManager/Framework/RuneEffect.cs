﻿namespace Framework {

	public class RuneEffect {

		public RuneEnums.StatType Stat { get; set; }

		public int Value { get; set; }

	}

}
