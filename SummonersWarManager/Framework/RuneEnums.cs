﻿using System.ComponentModel;

namespace Framework {

	public class RuneEnums {

		public enum Grade {
			Common = 0,
			Uncommon = 1,
			Rare = 2,
			Hero = 3,
			Legend = 4
		}

		public enum SetType {

			Unknown = 0,
			Energy = 1,
			Guard = 2,
			Swift = 3,
			Blade = 4,
			Rage = 5,
			Focus = 6,
			Endure = 7,
			Fatal = 8,
			Despair = 10,
			Vampire = 11,
			Enhance = 12,
			Violent = 13,
			Nemesis = 14,
			Will = 15,
			Shield = 16,
			Revenge = 17,
			Destroy = 18,
			Fight = 19,
			Determination = 20,
			Accuracy = 22,
			Tolerance = 23

		}

		public enum StatType {

			[Description("???")]
			Unknown = 0,

			[Description("HP+")]
			HPFlat = 1,

			[Description("HP%")]
			HPPercent = 2,

			[Description("ATK+")]
			ATKFlat = 3,

			[Description("ATK%")]
			ATKPercent = 4,

			[Description("DEF+")]
			DEFFlat = 5,

			[Description("DEF%")]
			DEFPercent = 6,

			[Description("SPD")]
			SPD = 8,

			[Description("Crit Rate")]
			CR = 9,

			[Description("Crit Damage")]
			CD = 10,

			[Description("RES")]
			RES = 11,

			[Description("ACC")]
			ACC = 12

		}

	}
}
