﻿using System;
using System.ComponentModel;

namespace Framework {
	public static class RuneEnumsExtensions {

		public static TAttribute GetAttribute<TAttribute>(this Enum value)
		where TAttribute : Attribute {
			var type = value.GetType();
			var memInfo = type.GetMember(value.ToString());
			var attributes = memInfo[0].GetCustomAttributes(typeof(TAttribute), false);
			return (attributes.Length > 0) ? (TAttribute)attributes[0] : null;
		}

		public static string Description(this Enum value) {
			return value.GetAttribute<DescriptionAttribute>()?.Description;
		}

	}
}
