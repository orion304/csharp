﻿namespace Framework {

	public class Rune {

		public int OccupiedType { get; set; }

		public int Extra { get; set; }

		public long Id { get; set; }

		public int SellValue { get; set; }

		public RuneEffect PrimaryEffect { get; set; }

		public RuneEffect StaticEffect { get; set; }

		public RuneEffect[] AdditionalEffects { get; set; } = new RuneEffect[4];

		public int Slot { get; set; }

		public int CurrentLevel { get; set; }

		public int MaxLevel { get; set; }

		public int Rank { get; set; }

		public long OccupiedId { get; set; }

		public long WizardId { get; set; }

		public int BaseValue { get; set; }

		public int Stars { get; set; }

		public RuneEnums.SetType Set { get; set; }

	}
}
