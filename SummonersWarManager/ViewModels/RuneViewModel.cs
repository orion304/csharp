﻿using Framework;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace ViewModels {
	public class RuneViewModel : INotifyPropertyChanged {

		private Rune _rune;

		public RuneViewModel(Rune rune) {
			_rune = rune;
		}

		public int OccupiedType {
			get { return _rune.OccupiedType; }
			set {
				_rune.OccupiedType = value;
				OnPropertyChanged();
			}
		}

		public int Extra {
			get { return _rune.Extra; }
			set {
				_rune.Extra = value;
				OnPropertyChanged();
			}
		}

		public long Id {
			get { return _rune.Id; }
			set {
				_rune.Id = value;
				OnPropertyChanged();
			}
		}

		public int SellValue {
			get { return _rune.SellValue; }
			set {
				_rune.SellValue = value;
				OnPropertyChanged();
			}
		}

		public RuneEffect PrimaryEffect {
			get { return _rune.PrimaryEffect; }
			set {
				_rune.PrimaryEffect = value;
				OnPropertyChanged();
			}
		}

		public RuneEffect StaticEffect {
			get { return _rune.StaticEffect; }
			set {
				_rune.StaticEffect = value;
				OnPropertyChanged();
			}
		}

		public RuneEffect[] AdditionalEffects {
			get { return _rune.AdditionalEffects; }
			set {
				_rune.AdditionalEffects = value;
				OnPropertyChanged();
			}
		}

		public int Slot {
			get { return _rune.Slot; }
			set {
				_rune.Slot = value;
				OnPropertyChanged();
			}
		}

		public int CurrentLevel {
			get { return _rune.CurrentLevel; }
			set {
				_rune.CurrentLevel = value;
				OnPropertyChanged();
			}
		}

		public int MaxLevel {
			get { return _rune.MaxLevel; }
			set {
				_rune.MaxLevel = value;
				OnPropertyChanged();
			}
		}

		public int Rank {
			get { return _rune.Rank; }
			set {
				_rune.Rank = value;
				OnPropertyChanged();
			}
		}

		public long OccupiedId {
			get { return _rune.OccupiedId; }
			set {
				_rune.OccupiedId = value;
				OnPropertyChanged();
			}
		}

		public long WizardId {
			get { return _rune.WizardId; }
			set {
				_rune.WizardId = value;
				OnPropertyChanged();
			}
		}

		public int BaseValue {
			get { return _rune.BaseValue; }
			set {
				_rune.BaseValue = value;
				OnPropertyChanged();
			}
		}

		public int Stars {
			get { return _rune.Stars; }
			set {
				_rune.Stars = value;
				OnPropertyChanged();
			}
		}

		public RuneEnums.SetType Set {
			get { return _rune.Set; }
			set {
				_rune.Set = value;
				OnPropertyChanged();
			}
		}

		public event PropertyChangedEventHandler PropertyChanged;

		public void OnPropertyChanged([CallerMemberName] string propertyName = "") {
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}
