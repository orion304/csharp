﻿using DataProcessing;
using System.Windows;

namespace SummonersWarManager {
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window {

		private const string FilePath = @"C:\Users\Tokuro\Documents\SW\8200451.json";

		public MainWindow() {
			InitializeComponent();

			Test();
		}

		private void Test() {
			var text = System.IO.File.ReadAllText(FilePath);
			var processor = new JsonProcessor(text);

			var runes = processor.ProcessAllRunes();
		}

	}
}
