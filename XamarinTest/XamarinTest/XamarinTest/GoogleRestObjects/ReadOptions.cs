﻿namespace XamarinTest.GoogleRestObjects {
	public class ReadOptions {

		public ReadConsitency ReadConsistency { get; set; }

		public enum ReadConsitency {
			READ_CONSISTENCY_UNSPECIFIED,
			STRONG,
			EVENTUAL
		}

	}
}
