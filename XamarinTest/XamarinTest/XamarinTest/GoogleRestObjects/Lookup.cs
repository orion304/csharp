﻿using System.Collections.Generic;

namespace XamarinTest.GoogleRestObjects {
	public class Lookup {

		public ReadOptions ReadOptions { get; set; }

		public List<Key> Keys { get; set; } = new List<Key>();

	}
}
