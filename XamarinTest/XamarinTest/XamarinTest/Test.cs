﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PCLCrypto;
using System;
using System.Net.Http;
using System.Text;
using XamarinTest.GoogleRestObjects;

namespace XamarinTest {
	public class Test {

		private const string PrivateKey = @"MIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCsnWN764JrnFPTQ1ceV5ISfJNHDzjdMpZIAaDNA5NVE5RF9y6j75rzQW4MxKUYzbHkOwAfpeW/BQXcXHDS1fC9eRPBRqy0aQGCcCJMp/YlldKna1/cEfZJNG4Z0e3+Qefl/Eh6VsFa5eRH1Mm68ZtTCzO2YpF6hJWZgQjiotyQcFBNlvVXla/SlkAjOWoAHcp9ATLmtgRgagPfu72I2Wf4FmZ/YTj81quzYsBj7DpcTa9JkIMb4D2kYRPXiL4/WShs8QABU8Ky/L4t+dsUqvi+rYXQdV8k4nzHm2ePSEhXgw3682L8B4xp2sphuHEveOCBdfbo6hSgbGoNAMkBF+4JAgMBAAECggEADdxThTtbjSWP65pwL+0LOzXKo72arNRnkd+jmGLNnVrFoLqhAaP3VsotupO9yBiYTMHaGDB7mtOd7Vx06NEqFCTZS79PlesKpn03m90u3ETEFhJ+r463rpgpsSQUzsJfCy4vabd2rkRgiM4S5tKIzOnM3Lvcf7COzLU6T+yQ9n4Ub7xk8AnrrOM6u+zGNlnAJqQjWbrHQZDE47t3IA6xzxPXuyNFSsmNtnCZQ1Y4p2I7cm0Y8yqlBYynMXvqXxfnLyQHpfaRYO4sOiMOz4xoh+rR4/UHpOyRwEq1Yh8QuKhwX+jOXHko2yEYWbBIOg3WwWYhNvz8z1XLD7wFgTv7AQKBgQDjTUN78jrdAesV97z4RD+qOEcnAPC8bYo1nczRj4jQ6vwyrhJtBJyFDJ+NwaccPCu+NABkVrN2Yh7BhXPWQRq2Qjr+rbXZhG1K7YW8GEorMRsfELVrtk/DGR+kOsfU1PZeJ+tYptqsHOJELb8JrxlFPUPm9T7++SIUKCY9bPK/wQKBgQDCaI779xr3ENZhf02K82Yj8k5komny39P8pDrRDBOeYi1OKRC0FOMfhmwJ2OZ6KKa0gxyduUCE/lD5/ztqp/TtoOLd/E+ZLvip/OUH+JjvyLVc6La0BiTZgLvYf9kRVNB/dARORxdbzIwqhAPRlQt7fSprWiGlGFa+zEJR669ASQKBgF+7YNrJEIWYkzxzVMFzbSTyAvWobbvOntEqHuN8AnGLJqDps5T6xjk7Mc/wacN4jCvVlvyrj3YiqTV0UW9BiFCO4G33F4n/PlC6bQLlcAd07SlOTjqJVFqlMDBpGVTIROMvCO4TiDHJEHsKkVUwzAHfZcPWMqwMnY/DwSdJxYuBAoGAKQl9NT7CGGkbd9UxfyVF6Eybs659AuGwk+Hu8HXGJma+/YK5nenSz7LGV4XLkPnHxs7uWXTjY69zVHJGDouJrieJ/CXof4Nkxi02A2q8jdEmR4hwGKw7VxZPkLHcFkZ7BIxu5D8pdR1GcSsQy0rU86pGLAcIlxr+JKRlU3mWstECgYAKIetNotJODrAKAbLDwOV9Viurtd6kNF6S3cHuyHD/FYZCK/41DXzQACVz1XCOzmRKi6w0x529kivZHmZwqnqvWTV1ofMPpkrz3I4fiwFgf9cwPY0GVwzrrR+wrp/FN2oGXUr7VwcEP51iXDHLT75ijiA+5hzanLr0J02S3hoc+Q==";
		private const string PublicKey1 = @"w0+yalkfTIYaH6EJOviTGyT782s/dQh2T1uO0HjlzoCdKduFnoMyufP1vruycP6M0PiaZaeZTZLzWP6oGIJfwn/XFw8S/Lpg9WvhMbnR9z6MbPCW6GRYaZany+5WT4P2BHH7tMSBRlbw2RgYhKi4eXBFnCXonFwxcHmTofVRPe1IP/hFqnNdR6MnGeC45UX5RpdlGyiRanAYp6fhDBfHXyzV1ycD4xHk+Dqv9uWpkrvpyfNHiHO8VnKwSN/Aj/I6XR+vCD7urdv3kaNPV/9/6+l2UZfODVkhChRg8JH5CURQLi/xzAQwl7MfbkFMymhzNcQ/mlI9pp6ESZIYQiu6aQ==";
		private const string PublicKey2 = @"MIIC+jCCAeKgAwIBAgIIb2JDY4EuWWcwDQYJKoZIhvcNAQEFBQAwIDEeMBwGA1UEAxMVMTA3NzI0MjY1NzkxMjE3MjU2MDY0MB4XDTE4MDcxMzAyNTMzOVoXDTI4MDcxMDAyNTMzOVowIDEeMBwGA1UEAxMVMTA3NzI0MjY1NzkxMjE3MjU2MDY0MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEArJ1je+uCa5xT00NXHleSEnyTRw843TKWSAGgzQOTVROURfcuo++a80FuDMSlGM2x5DsAH6XlvwUF3Fxw0tXwvXkTwUastGkBgnAiTKf2JZXSp2tf3BH2STRuGdHt/kHn5fxIelbBWuXkR9TJuvGbUwsztmKReoSVmYEI4qLckHBQTZb1V5Wv0pZAIzlqAB3KfQEy5rYEYGoD37u9iNln+BZmf2E4/Nars2LAY+w6XE2vSZCDG+A9pGET14i+P1kobPEAAVPCsvy+LfnbFKr4vq2F0HVfJOJ8x5tnj0hIV4MN+vNi/AeMadrKYbhxL3jggXX26OoUoGxqDQDJARfuCQIDAQABozgwNjAMBgNVHRMBAf8EAjAAMA4GA1UdDwEB/wQEAwIHgDAWBgNVHSUBAf8EDDAKBggrBgEFBQcDAjANBgkqhkiG9w0BAQUFAAOCAQEAgHn6Xd7lJSX8X+JuQPatYaTlLMqDIZirME/B7jQOFP7aucafWrCGeI5GG4uQTkw8Wg9TrNsutXKznf8U+1scghUdI/SxJKJZvPStZZh1smAD8XubSM+70T4F4+A0kmjWYWFHp780hMktc2Kp5IK9GOoMxamJ/XQ901DiBriW+4gDHKyGcDjyyRHxkGCFKL/FjIlZmB7odxxiyDtNPUHaPSCd7SHeB2hO1OdVsmbtZ6QD5pHZtKbiURefklNc03PeG2yWK0M6IuRUmWkZqpDybjug2ZhvVg92Wm+04qIH2rSa8+/AgGJG3Hpa4zYnX/JlgdtfGhfcSbmNrsMy41ILow==";

		private const string Username = "Jonathan.L.Wheeler@gmail.com";
		private const string Password = "RKu8lCRpb32ws1KW2TAw";

		private const string UrlBase = @"https://datastore.googleapis.com";
		private const string ProjectId = "infra-inkwell-210102";

		private const string Email = @"lme-509@infra-inkwell-210102.iam.gserviceaccount.com";
		private static HttpClient _client;

		private static readonly DateTime UnixStart = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
		private const int Hour = 60 * 60;

		public static void Do() {

			_client = new HttpClient();
			_client.MaxResponseContentBufferSize = 256000;

			var jwtHeader = @"{""alg"":""RS256"",""typ"":""JWT""}";
			var encodedJwtHeader = Encode(jwtHeader);

			var scope = @"https://www.googleapis.com/auth/datastore";
			var aud = @"https://www.googleapis.com/oauth2/v4/token";
			var currentTime = ConvertToUnixTimestamp(DateTime.Now);
			var expTime = currentTime + Hour;

			var jwtClaim = $@"{{""iss"":""{Email}"",""scope"":""{scope}"",""aud"":""{aud}"",""exp"":{expTime},""iat"":{currentTime}}}";
			var encodedJwtClaim = Encode(jwtClaim);

			encodedJwtHeader = EncodeSafe64(encodedJwtHeader);
			encodedJwtClaim = EncodeSafe64(encodedJwtClaim);

			var signature = TestHash($"{encodedJwtHeader}.{encodedJwtClaim}");
			signature = EncodeSafe64(signature);

			var jwt = $"{encodedJwtHeader}.{encodedJwtClaim}.{signature}";

			//var auth = Convert.ToBase64String(Encoding.UTF8.GetBytes($"{Username}:{Password}"));
			//_client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", auth);
			//TestGet();
			//return;

			TestAuth(jwt);
			return;

		}

		private static string EncodeSafe64(string value) {
			return value.TrimEnd('=').Replace("+", "-").Replace("/", "_");
		}


		private static async void TestAuth(string auth) {
			//var grantType = @"urn:ietf:params:oauth:grant-type:jwt-bearer";
			var grantType = @"urn%3Aietf%3Aparams%3Aoauth%3Agrant-type%3Ajwt-bearer";
			//var content = $@"{{""grant_type"":""{grantType}"",""assertion"":""{auth}""}}";
			var content = $@"grant_type=urn%3Aietf%3Aparams%3Aoauth%3Agrant-type%3Ajwt-bearer&assertion={auth}";

			//var result = await _client.PostAsync(@"https://www.googleapis.com/oauth2/v4/token", new StringContent(content, Encoding.UTF8, "application/json"));
			var result = await _client.PostAsync(@"https://www.googleapis.com/oauth2/v4/token", new StringContent(content, Encoding.UTF8, "application/x-www-form-urlencoded"));
			var jsonContent = await result.Content.ReadAsStringAsync();

			//var jsonObject = JsonConvert.DeserializeObject(jsonContent);
			var jsonObject = JObject.Parse(jsonContent);
			var token = jsonObject["access_token"].ToString();

			var json = @"{  ""readOptions"": {    ""readConsistency"": ""STRONG""  },  ""keys"": [    {      ""partitionId"": {        ""projectId"": ""infra-inkwell-210102""      },      ""path"": [        {          ""kind"": ""WorkMessage"",          ""id"": ""5634472569470976""        }      ]    }  ]}";
			var content2 = new StringContent(json, Encoding.UTF8, "application/json");
			var result2 = await _client.PostAsync($"{UrlBase}/v1/projects/{ProjectId}:lookup?access_token={token}", content2);

			jsonContent = await result2.Content.ReadAsStringAsync();
		}

		private static string TestHash(string auth) {
			var input = Encoding.UTF8.GetBytes(auth);
			var mac = WinRTCrypto.AsymmetricKeyAlgorithmProvider.OpenAlgorithm(AsymmetricAlgorithm.RsaSignPkcs1Sha256);
			var key = mac.ImportKeyPair(Convert.FromBase64String(PrivateKey));
			var hash = WinRTCrypto.CryptographicEngine.Sign(key, input);

			//var test = VerifyHash(input, hash);

			return WinRTCrypto.CryptographicBuffer.EncodeToBase64String(hash);
		}

		private static bool VerifyHash(byte[] data, byte[] signature) {
			var mac = WinRTCrypto.AsymmetricKeyAlgorithmProvider.OpenAlgorithm(AsymmetricAlgorithm.RsaSignPkcs1Sha256);
			//var key = mac.ImportPublicKey(Convert.FromBase64String(PublicKey2));
			var key = mac.ImportPublicKey(WinRTCrypto.CryptographicBuffer.ConvertStringToBinary(PublicKey2, Encoding.UTF8));
			//var keyMaterial = Convert.FromBase64String(PublicKey1);
			//var cryptoKey = mac.ImportPublicKey(keyMaterial);
			return WinRTCrypto.CryptographicEngine.VerifySignature(key, data, signature);
		}

		private static string HashAndSign(string data) {
			byte[] byteData = Encoding.UTF8.GetBytes(data);
			return CryptographyExtensions.RsaEncryption.HashAndSign(byteData, PrivateKey);
		}

		private static int ConvertToUnixTimestamp(DateTime date) {
			var diff = date.ToUniversalTime() - UnixStart;
			return (int) Math.Floor(diff.TotalSeconds);
		}

		private static string Encode(string value) {
			return Convert.ToBase64String(Encoding.UTF8.GetBytes(value));
		}

		private static async void TestGet() {

			const string uri = @"https://ft-jonathan-l-wheeler.oraclecloud2.dreamfactory.com/api/v2/db/";
			var result2 = await _client.GetAsync(uri);
			var jsonContent = await result2.Content.ReadAsStringAsync();
			return;

			var lookup = new Lookup() {
				ReadOptions = new ReadOptions { ReadConsistency = ReadOptions.ReadConsitency.STRONG }
			};

			//var json = JsonConvert.SerializeObject(null);
			var json = @"{  ""readOptions"": {    ""readConsistency"": ""STRONG""  },  ""keys"": [    {      ""partitionId"": {        ""projectId"": ""infra-inkwell-210102""      },      ""path"": [        {          ""kind"": ""WorkMessage"",          ""id"": ""5634472569470976""        }      ]    }  ]}";
			var content = new StringContent(json, Encoding.UTF8, "application/json");
			var result = await _client.PostAsync($"{UrlBase}/v1/projects/{ProjectId}:lookup", content);

		}

	}
}
