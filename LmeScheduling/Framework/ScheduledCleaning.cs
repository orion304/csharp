﻿using System;
using System.Collections.Generic;

namespace Framework {

	[Serializable]
	public class ScheduledCleaning {
		
		public Client Client { get; set; }

		public List<Employee> Employees { get; set; }

		public DateTime CleaningDate { get; set; }

	}
}
