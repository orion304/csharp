﻿using System;

namespace Framework {

	[Serializable]
	public class Employee {

		public string Name { get; set; }

		public string PhoneNumber { get; set; }

		public bool IsSalaried { get; set; }

		public double HourlyWage { get; set; }

		public double WeeklySalary { get; set; }

	}
}
