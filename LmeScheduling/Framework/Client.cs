﻿using System;
using static Framework.Enums;

namespace Framework {

	[Serializable]
	public class Client {

		public CustomerPayType CustomerPayType { get; set; }

		public double AmountChargedPerCleaning { get; set; }

		public string Name { get; set; }

		public DayOfWeek NormalCleaningDay { get; set; }

		public int CleaningFrequencyWeeks { get; set; }



	}
}
