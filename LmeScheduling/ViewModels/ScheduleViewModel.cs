﻿using Framework;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Input;

namespace ViewModels {
	public class ScheduleViewModel : INotifyPropertyChanged {

		private ICommand _loadScheduleCommand;
		public ICommand LoadScheduleCommand => _loadScheduleCommand ?? (_loadScheduleCommand = new ActionCommand(LoadSchedule));

		private ICommand _saveScheduleCommand;
		public ICommand SaveScheduleCommand => _saveScheduleCommand ?? (_saveScheduleCommand = new ActionCommand(SaveSchedule));

		private List<ScheduledCleaning> _scheduledCleanings;

		public ScheduleViewModel() {
			LoadSchedule();
		}

		private void LoadSchedule() {
			// todo
		}

		private void SaveSchedule() {
			// todo
		}

		public event PropertyChangedEventHandler PropertyChanged;
		private void OnPropertyChanged([CallerMemberName] string propertyName = "") {
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}
