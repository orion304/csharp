﻿using System;

namespace ViewModels {
	public class EmployeeDayScheduleViewModel {

		public DateTime Date { get; set; }

		public DayOfWeek Day => Date.DayOfWeek;

	}
}
