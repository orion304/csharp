﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Trees {
    class Board {

        private readonly Grid parentGrid;
        private readonly Grid grid;
        private readonly int rows;
        private readonly int columns;

        private readonly Cell[,] cells;

        public Board(Grid grid, int rows, int columns) {
            this.parentGrid = grid;
            this.grid = new Grid();
            this.parentGrid.Children.Add(this.grid);
            this.rows = rows;
            this.columns = columns;

            cells = new Cell[rows, columns];

            InitializeGrid();
            InitializeCells();

        }

        private void InitializeGrid() {
            for (int i = 0; i < rows; i++) {
                RowDefinition row = new RowDefinition();
                grid.RowDefinitions.Add(row);


            }

            for (int j = 0; j < columns; j++) {
                ColumnDefinition column = new ColumnDefinition();
                grid.ColumnDefinitions.Add(column);
            }

            //Rectangle rectangle = new Rectangle();
            //rectangle.Fill = new SolidColorBrush(Colors.Yellow);
            //rectangle.Stroke = new SolidColorBrush(Colors.Black);
            //Grid.SetRow(rectangle, i);
            //Grid.SetColumn(rectangle, j);

            //grid.Children.Add(rectangle);
        }

        private void InitializeCells() {

            Region testRegion = new Region();
            testRegion.Color = Colors.Blue;

            Cell cell;
            for (int i = 0; i < rows; i++) {
                for (int j = 0; j < columns; j++) {

                    cell = new Cell(grid, i, j, testRegion);

                }
            }

        }

    }
}
