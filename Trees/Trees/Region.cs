﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace Trees {
    class Region {

        private static readonly Color[] availableColors = new Color[] { Colors.LightBlue, Colors.Yellow, Colors.Gray};
        private static readonly Random random = new Random();

        private static List<Color> unusedColors = new List<Color>();

        private Color color;
        public Color Color {
            get { return color; }
            set {
                color = value;
                ColorBrush = new SolidColorBrush(color);
            }
        }

        public SolidColorBrush ColorBrush { get; private set; }

        public List<Cell> Cells { get; set; }

        public Region() {

            if (unusedColors.Count == 0) {
                unusedColors.AddRange(availableColors);
            }

            int index = random.Next(unusedColors.Count);
            color = unusedColors[index];
            unusedColors.RemoveAt(index);

        }

    }
}
