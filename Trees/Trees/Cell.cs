﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Trees {
    class Cell {

        private static readonly SolidColorBrush border = new SolidColorBrush(Colors.Black);
        private static readonly SolidColorBrush white = new SolidColorBrush(Colors.White);

        private Label treeLabel;
        public bool HasTree { get {
                return treeLabel.IsVisible;
            }
        }

        private Label blockedLabel;
        public bool IsBlocked { get {
                return blockedLabel.IsVisible;
            }
        }

        public Rectangle Rectangle { get; set; }

        public Region Region { get; set; }

        private readonly int row, column;

        public Cell(Grid grid, int row, int column, Region region) {
            Region = region;
            this.row = row;
            this.column = column;

            Rectangle = new Rectangle {
                Fill = region.ColorBrush,
                Stroke = border
            };

            Rectangle.MouseUp += Rectangle_MouseUp;
            
            Grid.SetRow(Rectangle, row);
            Grid.SetColumn(Rectangle, column);
            grid.Children.Add(Rectangle);

            treeLabel = new Label {
                Content = "O",
                HorizontalAlignment = System.Windows.HorizontalAlignment.Center,
                VerticalAlignment = System.Windows.VerticalAlignment.Center,
                Foreground = white,
                Visibility = System.Windows.Visibility.Hidden,
                IsHitTestVisible = false
            };

            Grid.SetRow(treeLabel, row);
            Grid.SetColumn(treeLabel, column);
            grid.Children.Add(treeLabel);

            blockedLabel = new Label {
                Content = "·",
                HorizontalAlignment = System.Windows.HorizontalAlignment.Center,
                VerticalAlignment = System.Windows.VerticalAlignment.Center,
                Foreground = white,
                Visibility = System.Windows.Visibility.Hidden,
                IsHitTestVisible = false
            };

            Grid.SetRow(blockedLabel, row);
            Grid.SetColumn(blockedLabel, column);
            grid.Children.Add(blockedLabel);
        }

        private void Rectangle_MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e) {
           
            if (IsBlocked) {
                treeLabel.Visibility = System.Windows.Visibility.Visible;
                blockedLabel.Visibility = System.Windows.Visibility.Hidden;
            } else if (HasTree) {
                treeLabel.Visibility = System.Windows.Visibility.Hidden;
                blockedLabel.Visibility = System.Windows.Visibility.Hidden;
            } else {
                treeLabel.Visibility = System.Windows.Visibility.Hidden;
                blockedLabel.Visibility = System.Windows.Visibility.Visible;
            }

        }
    }
}
