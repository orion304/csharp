﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Trees {
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window {

        private SolidColorBrush[] boardColors = {
            new SolidColorBrush(System.Windows.Media.Colors.LightGray),
            new SolidColorBrush(System.Windows.Media.Colors.Yellow),
            new SolidColorBrush(System.Windows.Media.Colors.Blue),
            new SolidColorBrush(System.Windows.Media.Colors.LightBlue),
            new SolidColorBrush(System.Windows.Media.Colors.Orange),
            new SolidColorBrush(System.Windows.Media.Colors.Purple),
            new SolidColorBrush(System.Windows.Media.Colors.DarkGray)
        };

        private Board board;

        public MainWindow() {
            InitializeComponent();

            Rows.Text = "6";
            Columns.Text = "6";
            Colors.Text = "3";
        }

        private void Rows_TextChanged(object sender, TextChangedEventArgs e) {
            ChangeBoardSetup();
        }

        private void Columns_TextChanged(object sender, TextChangedEventArgs e) {
            ChangeBoardSetup();
        }

        private void Colors_TextChanged(object sender, TextChangedEventArgs e) {
            ChangeBoardSetup();
        }

        private void ChangeBoardSetup() {
            if (int.TryParse(Rows.Text, out int numberOfRows) && numberOfRows > 0
                && int.TryParse(Columns.Text, out int numberOfColumns) && numberOfColumns > 0
                && int.TryParse(Colors.Text, out int numberOfColors) && numberOfColors > 0) {

                BottomRight.Children.Clear();
                board = new Board(BottomRight, numberOfRows, numberOfColumns);

            }

            ChangeColorSetup();
        }

        private void ChangeColorSetup() {

            if (int.TryParse(Colors.Text, out int numberOfColors) && numberOfColors > 0) {

                while (Left.RowDefinitions.Count > numberOfColors) {

                    Left.RowDefinitions.RemoveAt(Left.RowDefinitions.Count - 1);

                }

                while (Left.RowDefinitions.Count < numberOfColors) {

                    Left.RowDefinitions.Add(new RowDefinition());

                }

            }

        }
    }
}
