﻿using Newtonsoft.Json.Linq;
using PCLCrypto;
using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace LmeEmployeeAssistantMobile.GoogleApi {
	public static class OAuth2 {

		private const string PrivateKey = @"MIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCsnWN764JrnFPTQ1ceV5ISfJNHDzjdMpZIAaDNA5NVE5RF9y6j75rzQW4MxKUYzbHkOwAfpeW/BQXcXHDS1fC9eRPBRqy0aQGCcCJMp/YlldKna1/cEfZJNG4Z0e3+Qefl/Eh6VsFa5eRH1Mm68ZtTCzO2YpF6hJWZgQjiotyQcFBNlvVXla/SlkAjOWoAHcp9ATLmtgRgagPfu72I2Wf4FmZ/YTj81quzYsBj7DpcTa9JkIMb4D2kYRPXiL4/WShs8QABU8Ky/L4t+dsUqvi+rYXQdV8k4nzHm2ePSEhXgw3682L8B4xp2sphuHEveOCBdfbo6hSgbGoNAMkBF+4JAgMBAAECggEADdxThTtbjSWP65pwL+0LOzXKo72arNRnkd+jmGLNnVrFoLqhAaP3VsotupO9yBiYTMHaGDB7mtOd7Vx06NEqFCTZS79PlesKpn03m90u3ETEFhJ+r463rpgpsSQUzsJfCy4vabd2rkRgiM4S5tKIzOnM3Lvcf7COzLU6T+yQ9n4Ub7xk8AnrrOM6u+zGNlnAJqQjWbrHQZDE47t3IA6xzxPXuyNFSsmNtnCZQ1Y4p2I7cm0Y8yqlBYynMXvqXxfnLyQHpfaRYO4sOiMOz4xoh+rR4/UHpOyRwEq1Yh8QuKhwX+jOXHko2yEYWbBIOg3WwWYhNvz8z1XLD7wFgTv7AQKBgQDjTUN78jrdAesV97z4RD+qOEcnAPC8bYo1nczRj4jQ6vwyrhJtBJyFDJ+NwaccPCu+NABkVrN2Yh7BhXPWQRq2Qjr+rbXZhG1K7YW8GEorMRsfELVrtk/DGR+kOsfU1PZeJ+tYptqsHOJELb8JrxlFPUPm9T7++SIUKCY9bPK/wQKBgQDCaI779xr3ENZhf02K82Yj8k5komny39P8pDrRDBOeYi1OKRC0FOMfhmwJ2OZ6KKa0gxyduUCE/lD5/ztqp/TtoOLd/E+ZLvip/OUH+JjvyLVc6La0BiTZgLvYf9kRVNB/dARORxdbzIwqhAPRlQt7fSprWiGlGFa+zEJR669ASQKBgF+7YNrJEIWYkzxzVMFzbSTyAvWobbvOntEqHuN8AnGLJqDps5T6xjk7Mc/wacN4jCvVlvyrj3YiqTV0UW9BiFCO4G33F4n/PlC6bQLlcAd07SlOTjqJVFqlMDBpGVTIROMvCO4TiDHJEHsKkVUwzAHfZcPWMqwMnY/DwSdJxYuBAoGAKQl9NT7CGGkbd9UxfyVF6Eybs659AuGwk+Hu8HXGJma+/YK5nenSz7LGV4XLkPnHxs7uWXTjY69zVHJGDouJrieJ/CXof4Nkxi02A2q8jdEmR4hwGKw7VxZPkLHcFkZ7BIxu5D8pdR1GcSsQy0rU86pGLAcIlxr+JKRlU3mWstECgYAKIetNotJODrAKAbLDwOV9Viurtd6kNF6S3cHuyHD/FYZCK/41DXzQACVz1XCOzmRKi6w0x529kivZHmZwqnqvWTV1ofMPpkrz3I4fiwFgf9cwPY0GVwzrrR+wrp/FN2oGXUr7VwcEP51iXDHLT75ijiA+5hzanLr0J02S3hoc+Q==";
		private const string Scopes = @"https://www.googleapis.com/auth/datastore";
		private const string Aud = @"https://www.googleapis.com/oauth2/v4/token";
		private const string Email = @"lme-509@infra-inkwell-210102.iam.gserviceaccount.com";

		private const string GrantType = @"urn%3Aietf%3Aparams%3Aoauth%3Agrant-type%3Ajwt-bearer";

		private static readonly string EncodedJwtHeader = EncodeSafe64(@"{""alg"":""RS256"",""typ"":""JWT""}");
		private static readonly DateTime UnixStart = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
		private const int RefreshPeriod = 60 * 59;
		private const int Hour = 60 * 60;

		private static HttpClient HttpClient => _httpClient ?? (_httpClient = new HttpClient());
		private static HttpClient _httpClient;

		private static DateTime _currentExpireTime;

		public static string AccessToken { get; private set; }

		public static bool IsAccessTokenValid => DateTime.Now < _currentExpireTime;

		public static async Task RefreshAccessToken() {
			if (IsAccessTokenValid) return;

			var now = DateTime.Now;
			var expireTime = now.AddSeconds(RefreshPeriod);

			var currentTime = ConvertToUnixTimestamp(now);
			var expTime = currentTime + Hour;
			
			var jwtClaim = $@"{{""iss"":""{Email}"",""scope"":""{Scopes}"",""aud"":""{Aud}"",""exp"":{expTime},""iat"":{currentTime}}}";
			var encodedJwtClaim = EncodeSafe64(jwtClaim);

			var block = $"{EncodedJwtHeader}.{encodedJwtClaim}";
			var signature = RsaSignPkcs1Sha256(PrivateKey, block);
			signature = ReplaceNonUrlFileSafeCharacters(signature);

			var jwt = $"{block}.{signature}";

			var authContent = $@"grant_type={GrantType}&assertion={jwt}";

			var post = await HttpClient.PostAsync(Aud, new StringContent(authContent, Encoding.UTF8, Constants.UrlMediaType));
			var jsonContent = await post.Content.ReadAsStringAsync();

			var jsonObject = JObject.Parse(jsonContent);
			AccessToken = (string) jsonObject["access_token"];
			_currentExpireTime = expireTime;
		}

		private static string ReplaceNonUrlFileSafeCharacters(string value) {
			return value.TrimEnd('=').Replace("+", "-").Replace("/", "_");
		}

		private static string EncodeSafe64(string value) {
			value = Convert.ToBase64String(Encoding.UTF8.GetBytes(value));
			return ReplaceNonUrlFileSafeCharacters(value);
		}

		private static int ConvertToUnixTimestamp(DateTime date) {
			var diff = date.ToUniversalTime() - UnixStart;
			return (int)Math.Floor(diff.TotalSeconds);
		}

		private static string RsaSignPkcs1Sha256(string privateKey, string data) {
			var input = Encoding.UTF8.GetBytes(data);
			var algorithm = WinRTCrypto.AsymmetricKeyAlgorithmProvider.OpenAlgorithm(AsymmetricAlgorithm.RsaSignPkcs1Sha256);
			var key = algorithm.ImportKeyPair(Convert.FromBase64String(privateKey));
			var hash = WinRTCrypto.CryptographicEngine.Sign(key, input);

			return WinRTCrypto.CryptographicBuffer.EncodeToBase64String(hash);
		}

	}
}
