﻿namespace LmeEmployeeAssistantMobile.GoogleApi.RestObjects.BeginTransaction {
	public class ReadOptions {

		public ReadConsitency ReadConsistency { get; set; }

		public enum ReadConsitency {
			READ_CONSISTENCY_UNSPECIFIED,
			STRONG,
			EVENTUAL
		}

	}
}
