﻿namespace LmeEmployeeAssistantMobile.GoogleApi.RestObjects.BeginTransaction {
	public class BeginTransactionRequestBody {

		private static readonly BeginTransactionRequestBody _standardReadWrite = new BeginTransactionRequestBody();
		public static BeginTransactionRequestBody StandardReadWrite = _standardReadWrite;

		public TransactionOptions transactionOptions { get; set; } = new TransactionOptions();

	}
}
