﻿namespace LmeEmployeeAssistantMobile.GoogleApi.RestObjects.BeginTransaction {
	public class TransactionOptions {

		public ReadWrite readWrite { get; set; } = new ReadWrite();

	}
}
