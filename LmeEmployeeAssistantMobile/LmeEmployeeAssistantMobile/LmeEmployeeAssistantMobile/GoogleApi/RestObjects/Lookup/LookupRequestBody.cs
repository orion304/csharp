﻿using System;
using System.Collections.Generic;

namespace LmeEmployeeAssistantMobile.GoogleApi.RestObjects.Lookup {
	public class LookupRequestBody {

		public static LookupRequestBody CreateLookupRequest(string kind, Int64 id) {
			var requestBody = new LookupRequestBody();
			var key = new Key {
				partitionId = new PartitionId()
			};
			key.path.Add(new PathElementId { kind = kind, id = id });
			requestBody.keys.Add(key);

			return requestBody;
		}

		//public ReadOptions ReadOptions { get; set; }
		public List<Key> keys { get; set; } = new List<Key>();

	}
}
