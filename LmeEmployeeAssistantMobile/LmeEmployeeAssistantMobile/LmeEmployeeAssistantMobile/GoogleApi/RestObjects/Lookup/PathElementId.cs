﻿
using System;

namespace LmeEmployeeAssistantMobile.GoogleApi.RestObjects.Lookup {
	public class PathElementId : PathElement {

		public Int64 id { get; set; }

	}
}
