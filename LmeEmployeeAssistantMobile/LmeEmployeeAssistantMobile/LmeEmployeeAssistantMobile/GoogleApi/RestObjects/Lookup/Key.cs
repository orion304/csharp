﻿using System.Collections.Generic;

namespace LmeEmployeeAssistantMobile.GoogleApi.RestObjects.Lookup {
	public class Key {

		public PartitionId partitionId { get; set; }
		public List<PathElement> path { get; set; } = new List<PathElement>();

	}
}
