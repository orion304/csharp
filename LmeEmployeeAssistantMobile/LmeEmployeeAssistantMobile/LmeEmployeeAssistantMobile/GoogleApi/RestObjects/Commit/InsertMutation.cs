﻿namespace LmeEmployeeAssistantMobile.GoogleApi.RestObjects.Commit {
	public class InsertMutation : Mutation {

		public Entity insert { get; set; }

	}
}
