﻿using System;

namespace LmeEmployeeAssistantMobile.GoogleApi.RestObjects.Commit {
	public class WorkMessage {

		public static WorkMessage CreateWorkMessage(string name, DateTime time, bool start, bool manual) {
			return new WorkMessage {
				Name = new StringValue { stringValue = name },
				Time = new TimestampValue { timestampValue = time },
				Start = new BooleanValue { booleanValue = start },
				Manual = new BooleanValue { booleanValue = manual }
			};
		}

		public StringValue Name { get; set; }

		public TimestampValue Time { get; set; }

		public BooleanValue Start { get; set; }

		public BooleanValue Manual { get; set; }

	}
}
