﻿using System.Collections.Generic;

namespace LmeEmployeeAssistantMobile.GoogleApi.RestObjects.Commit {
	public class CommitRequestBody {


		public static CommitRequestBody CreateCommitRequestInsert(string transaction, string kind, object entity) {
			var body = new CommitRequestBody {
				transaction = transaction
			};

			var key = new Lookup.Key {
				partitionId = new Lookup.PartitionId()
			};
			key.path.Add(new Lookup.PathElement {
				kind = kind
			});

			body.mutations.Add(new InsertMutation {
				insert = new Entity {
					key = key,
					properties = entity
				}
			});

			return body;
		}

		public List<Mutation> mutations { get; set; } = new List<Mutation>();
		public string transaction { get; set; }


	}
}
