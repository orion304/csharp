﻿using System.Collections.Generic;

namespace LmeEmployeeAssistantMobile.GoogleApi.RestObjects.Commit {
	public class Entity {

		public Lookup.Key key { get; set; }

		public object properties { get; set; }

	}
}
