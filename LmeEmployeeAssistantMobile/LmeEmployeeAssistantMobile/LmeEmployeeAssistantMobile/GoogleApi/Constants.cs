﻿namespace LmeEmployeeAssistantMobile.GoogleApi {
	internal static class Constants {

		public const string UrlBase = @"https://datastore.googleapis.com";
		public const string ProjectId = "infra-inkwell-210102";
		
		public const string UrlMediaType = @"application/x-www-form-urlencoded";
		public const string JsonMediaType = @"application/json";

	}
}
