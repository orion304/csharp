﻿using LmeEmployeeAssistantMobile.GoogleApi.RestObjects;
using LmeEmployeeAssistantMobile.GoogleApi.RestObjects.BeginTransaction;
using LmeEmployeeAssistantMobile.GoogleApi.RestObjects.Commit;
using LmeEmployeeAssistantMobile.GoogleApi.RestObjects.Lookup;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace LmeEmployeeAssistantMobile.GoogleApi {
	public static class Datastore {

		private static readonly string Projects = $@"/v1/projects/{Constants.ProjectId}";
		private static readonly string BaseUrl = $"{Constants.UrlBase}{Projects}";

		private static readonly string LookupUrl = $"{BaseUrl}:lookup";
		private static readonly string BeginTransactionUrl = $"{BaseUrl}:beginTransaction";
		private static readonly string CommitUrl = $"{BaseUrl}:commit";

		private static HttpClient HttpClient => _httpClient ?? (_httpClient = new HttpClient());
		private static HttpClient _httpClient;

		public static async Task<string> Lookup() {
			var requestBody = LookupRequestBody.CreateLookupRequest("WorkMessage", 5634472569470976);
				
			var json = JsonConvert.SerializeObject(requestBody);

			await OAuth2.RefreshAccessToken();

			var post = await HttpClient.PostAsync(AppendAccessToken(LookupUrl), GetJsonContent(requestBody));
			var content = await post.Content.ReadAsStringAsync();

			return content;
		}

		public static async Task<string> BeginTransaction() {

			await OAuth2.RefreshAccessToken();

			var post = await HttpClient.PostAsync(AppendAccessToken(BeginTransactionUrl), GetJsonContent(BeginTransactionRequestBody.StandardReadWrite));

			var json = await GetContent(post);
			var transaction = (string)json["transaction"];

			return transaction;

		}

		private static HttpContent GetJsonContent(object obj) {
			return new StringContent(JsonConvert.SerializeObject(obj), Encoding.UTF8, Constants.JsonMediaType);
		}

		public static async Task<HttpResponseMessage> CommitNewWorkMessage(string name, DateTime time, bool start, bool manual) {
			var transaction = await BeginTransaction();

			

			var workMessage = WorkMessage.CreateWorkMessage(name, DateTime.Now, start, manual);

			var commitBody = CommitRequestBody.CreateCommitRequestInsert(transaction, "WorkMessage", workMessage);

			var t =JsonConvert.SerializeObject(commitBody);

			await OAuth2.RefreshAccessToken();
			var post = await HttpClient.PostAsync(AppendAccessToken(CommitUrl), GetJsonContent(commitBody));
			var json = await GetContent(post);

			return post;
		}

		private static async Task<JObject> GetContent(HttpResponseMessage message) {
			var content = await message.Content.ReadAsStringAsync();
			return JObject.Parse(content);
		}

		private static string AppendAccessToken(string url) {
			return $"{url}?access_token={OAuth2.AccessToken}";
		}

	}
}
