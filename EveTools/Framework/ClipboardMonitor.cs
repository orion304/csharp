﻿using System;

namespace Framework {
	public static class ClipboardMonitor {

		public static event EventHandler ClipboardChanged;

		private static string _clipboardText = string.Empty;

		public static string ClipboardText {
			get {
				lock (_clipboardText) {
					return _clipboardText;
				}
			}
			set {

				lock (_clipboardText) {
					_clipboardText = value;
					OnClipboardChanged();
				}
			}
		}

		private static void OnClipboardChanged() {
			ClipboardChanged?.Invoke(null, null);
		}


	}
}
