﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Views {
	/// <summary>
	/// Interaction logic for Dscan.xaml
	/// </summary>
	public partial class Dscan : UserControl {
		public Dscan() {
			InitializeComponent();
			Browser.Navigate("https://adashboard.info/intel/local/view/pCGPahN2");
		}
	}
}
