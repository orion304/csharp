﻿using mshtml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ViewModels;

namespace Views {
	/// <summary>
	/// Interaction logic for DotLanRealTime.xaml
	/// </summary>
	public partial class DotLanRealTime : UserControl {

		private DotLanRealTimeViewModel _viewModel;

		public DotLanRealTime() {
			_viewModel = new DotLanRealTimeViewModel();
			this.DataContext = _viewModel;
			InitializeComponent();

			_viewModel.PropertyChanged += _viewModel_PropertyChanged;
			UpdateView();
		}

		private void _viewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e) {
			UpdateView();
		}

		private void UpdateView() {
			if (!Equals(Browser.Source, _viewModel.Url)) {
				Browser.Navigate(_viewModel.Url);
			}
		}

		private void Browser_LoadCompleted(object sender, NavigationEventArgs e) {
			var doc = Browser.Document as HTMLDocument;

			RemoveElement(doc, "header");
			RemoveElement(doc, "poweredby");
			RemoveElement(doc, "bottomupdates");
			RemoveElement(doc, "sky");
			
			foreach (HTMLDivElement element in doc.getElementsByTagName("div")) {
				if (element.className == "cc-cookies ") {
					element.outerHTML = "";
				}
			}
		}

		private static void RemoveElement(HTMLDocument document, string id) {
			if (document == null) return;

			var element = document.getElementById(id);
			if (element == null) return;

			element.outerHTML = "";
		}
		
	}
}
