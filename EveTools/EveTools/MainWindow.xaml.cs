﻿using Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace EveTools {
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window {

		private const int WM_DRAWCLIPBOARD = 0x0308;

		[DllImport("User32.dll", CharSet = CharSet.Auto)]
		public static extern IntPtr SetClipboardViewer(IntPtr hWndNewViewer);

		[DllImport("User32.dll", CharSet = CharSet.Auto)]
		public static extern IntPtr SendMessage(IntPtr hWndNewViewer, int msg, IntPtr wParam, IntPtr lParam);

		private IntPtr hWndNextViewer;

		public MainWindow() {
			InitializeComponent();
		}

		private IntPtr WinProc(IntPtr hwnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled) {
			switch (msg) {

				case WM_DRAWCLIPBOARD:

					// pass the message to the next viewer. 
					SendMessage(hWndNextViewer, msg, wParam, lParam);

					if (Clipboard.ContainsText()) {
						var text = Clipboard.GetText();
						ClipboardMonitor.ClipboardText = text;
					}

					break;
			}

			return IntPtr.Zero;
		}

		private void Window_Loaded(object sender, RoutedEventArgs e) {
			var interop = new WindowInteropHelper(this);
			var hWndSource = HwndSource.FromHwnd(interop.Handle);

			hWndSource.AddHook(WinProc);
			hWndNextViewer = SetClipboardViewer(hWndSource.Handle);
		}
	}
}
