﻿using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace ViewModels {
	public class DotLanRealTimeViewModel : INotifyPropertyChanged {

		private string _url = "http://evemaps.dotlan.net/map/Black_Rise/Nisuwa#jumps";

		public string Url {
			get { return _url; }
			set {
				_url = value;
				OnPropertyChanged();
			}
		}


		public event PropertyChangedEventHandler PropertyChanged;

		private void OnPropertyChanged([CallerMemberName] string propertyName = "") {
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}
