﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WoWGoldFarming {
	class Item {

		private const System.Globalization.NumberStyles style = System.Globalization.NumberStyles.Any;
		private static readonly System.Globalization.CultureInfo culture = System.Globalization.CultureInfo.CreateSpecificCulture("en-US");

		public string _RealmName;
		public DateTime _ExportTime;
		public string _PMktPriceDate;
		public string _Reserved;
		public string _ItemId;
		public string _ItemName;
		public int _MarketPriceCoppers;
		public int _Quantity;
		public float _MarketPrice;
		public float _MinimumPrice;
		public float _MedianMarketPrice14Day;
		public float _MedianMarketPriceStdDev;
		public float _PMktPrice14Day;
		public float _PMktPriceStdDev;
		public float _DailyPriceChange;
		public float _AvgDailyPosted;
		public float _AvgEstimatedDailySold;
		public float _EstimatedDemand;

		public float _GoldVolume;

		public string ItemId {
			get { return _ItemId; }
		}

		public string ItemName {
			get { return _ItemName; }
		}

		public float GoldVolume {
			get { return _GoldVolume; }
		}

		public float MedianMarketPrice14Day {
			get { return _MedianMarketPrice14Day; }
		}

		public float MedianMarketPriceStdDev {
			get { return _MedianMarketPriceStdDev; }
		}

		public float AvgDailyPosted {
			get { return _AvgDailyPosted; }
		}

		public float AvgEstimatedDailySold {
			get { return _AvgEstimatedDailySold; }
		}

		public float EstimatedDemand {
			get { return _EstimatedDemand; }
		}

		public Item(string rawData) {
			string[] splits = rawData.Split('\t');

			_RealmName = splits[0];
			DateTime.TryParse(splits[1], out _ExportTime);
			_PMktPriceDate = splits[2];
			_Reserved = splits[3];
			_ItemId = splits[4];
			_ItemName = splits[5];
			int.TryParse(splits[6], out _MarketPriceCoppers);
			int.TryParse(splits[7], out _Quantity);
			float.TryParse(splits[8], style, culture, out _MarketPrice);
			float.TryParse(splits[9], style, culture, out _MinimumPrice);
			float.TryParse(splits[10], style, culture, out _MedianMarketPrice14Day);
			float.TryParse(splits[11], style, culture, out _MedianMarketPriceStdDev);
			float.TryParse(splits[12], style, culture, out _PMktPrice14Day);
			float.TryParse(splits[13], style, culture, out _PMktPriceStdDev);
			float.TryParse(splits[14], style, culture, out _DailyPriceChange);
			float.TryParse(splits[15], style, culture, out _AvgDailyPosted);
			float.TryParse(splits[16], style, culture, out _AvgEstimatedDailySold);
			float.TryParse(splits[17], style, culture, out _EstimatedDemand);

			_GoldVolume = _MedianMarketPrice14Day * _AvgEstimatedDailySold;
		}

	}
}
