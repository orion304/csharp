﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WoWGoldFarming {
	class Analyzer {

		private const string DataFilePath = @"C:\Users\Tokuro\Google Drive\Indie\Python Programs\WoW\illidan.txt";

		public List<Item> AllItems { get; private set; }

		public Analyzer() {
			string[] lines = System.IO.File.ReadAllLines(DataFilePath);

			AllItems = new List<Item>();
			Item item;
			foreach (string line in lines) {
				item = new Item(line);
				AllItems.Add(item);
			}
		}

		public List<Item> GetBestItems() {
			List<Item> items = new List<Item>();
			items.AddRange(AllItems.Where(i => i._EstimatedDemand > 0.3 && i._MedianMarketPriceStdDev / i._MedianMarketPrice14Day < .3 && i._AvgEstimatedDailySold >= 1));
			//items.Sort((i, j) => j._GoldVolume.CompareTo(i._GoldVolume));
			items.Sort((i, j) => j._GoldVolume.CompareTo(i._GoldVolume));
			return items;
		}

	}
}
