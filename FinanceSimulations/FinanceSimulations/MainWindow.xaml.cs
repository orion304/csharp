﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace FinanceSimulations {

	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window {

        private double startMoney = 6000;

		public MainWindow() {
			InitializeComponent();

            Console.WriteLine(Loan.GetLoanPayment(1000000, 4.125, 360));

			OldMain();
            GetThresholds();

		}

		private void GetThresholds() {
			double moneyEarnedPerMonth = 633.63 + 3748.70;
			//double nashvilleHousePrice = 200000;
			double nashvilleApartmentCost = 1295;
			double nashvilleHouseDownPaymentPercent = 0.035;

			List<string> lines = new List<string>();
			double moneyOnStudentLoans;
			for (double nashvilleHousePrice = 150000; nashvilleHousePrice <= 250000; nashvilleHousePrice += 10000) {
				for (int i = 1; i < 2; i++) {
					moneyOnStudentLoans = i == 1 ? 342 : 500;
					List<Simulation> simulations = RunSimulations(startMoney, nashvilleApartmentCost, nashvilleHousePrice, nashvilleHouseDownPaymentPercent, moneyOnStudentLoans, moneyEarnedPerMonth);

					simulations.Sort((s1, s2) => s2.FinancialData.Last().NetWorth.CompareTo(s1.FinancialData.Last().NetWorth));
					lines.Add(string.Format("{0}\t{1}\t{2}\t{3:0.00}", nashvilleHousePrice, moneyOnStudentLoans, simulations.First().FinancialData.Last().NetWorth, simulations.First().StartingPriorities));
				}
			}

			System.IO.File.WriteAllLines("thresholds.txt", lines);
		}

		private void OldMain() {

			double moneyEarnedPerMonth = 633.63 + 3748.70;
			double nashvilleHousePrice = 180000;
			double nashvilleApartmentCost = 1295;
			double nashvilleHouseDownPaymentPercent = 0.035;
            double moneyOnStudentLoans = 342; // 500;

			List<Simulation.Priority> seq = new List<Simulation.Priority>();
			foreach (Simulation.Priority priority in Enum.GetValues(typeof(Simulation.Priority))) {
                if (priority == Simulation.Priority.SellCookevilleHouse) {
                    continue;
                }
				seq.Add(priority);
			}

			Dictionary<int, Simulation> bests = new Dictionary<int, Simulation>();
			Simulation bestSimulation;

			List<Simulation> simulations = new List<Simulation>();

			double downPercent;

			foreach (IList<Simulation.Priority> permu in Simulation.Permutate(seq, seq.Count)) {

				if (!Simulation.IsPriorityListValid(permu)) {
					continue;
				}

				for (int j = 1; j < 2; j++) {

					for (int k = 2; k < 3; k++) {

						

						Simulation simulation = new Simulation(startMoney, nashvilleApartmentCost, nashvilleHousePrice, nashvilleHouseDownPaymentPercent, moneyOnStudentLoans, moneyEarnedPerMonth, true);
                        simulation.MinimumCookevillePayments = true;
                        simulation.buyHouseWithSpecialLoan = j == 0;

                        simulation.Priorities.AddRange(permu);
						simulation.RunSimulation();
						simulations.Add(simulation);

						for (int i = 0; i < simulation.FinancialData.Count; i++) {

							if (bests.ContainsKey(i)) {
								bestSimulation = bests[i];

								if (simulation.FinancialData[i].NetWorth > bestSimulation.FinancialData[i].NetWorth) {
									bests[i] = simulation;
								}

							} else {
								bests.Add(i, simulation);
							}

						}

					}
				}
			}

			List<int> keys = new List<int>();
			keys.AddRange(bests.Keys);
			keys.Sort();
			int lastKey = keys.Last();

			simulations.Sort((sim1, sim2) => sim2.FinancialData[lastKey].NetWorth.CompareTo(sim1.FinancialData[lastKey].NetWorth));

			double bestEndNetWorth = simulations.Max(i => i.FinancialData[lastKey].NetWorth);
            List<string> glanceLines = new List<string>();
            glanceLines.Add(string.Format("{0:0.00}", bestEndNetWorth));
            glanceLines.AddRange(simulations.Select(i => string.Join("\t", i.StartingPriorities, string.Format("{0:0.00}", i.FinancialData[lastKey].NetWorth - bestEndNetWorth).Replace(",", "."))));
			System.IO.File.WriteAllLines("glance.txt", glanceLines);

			string fileText = "";
			double bestNetWorth = 0;

			string header = string.Join("\t", simulations.Select(i => i.StartingPriorities));
			header = string.Join("\t", "Month", header);

			List<string> lines = new List<string>();
			string line;

			foreach (int key in keys) {
				bestSimulation = bests[key];
				bestNetWorth = bestSimulation.FinancialData[key].NetWorth;
				//Console.WriteLine(string.Format("Best for month {0} - {1} with a net worth of {2}", key, bestSimulation.StartingPriorities, bestSimulation.FinancialData[key].NetWorth));

				line = string.Join("\t", simulations.Select(i => string.Format("{0:0.00}", i.FinancialData[key].NetWorth - bestNetWorth)));
				lines.Add(string.Join("\t", key, line));
			}

			fileText = header + "\n" + string.Join("\n", lines);
			System.IO.File.WriteAllText("results.txt", fileText);

			FinancesDataGrid.ItemsSource = bests[keys.Last()].FinancialData;
			SimulationsDataGrid.ItemsSource = simulations;

		}

		private List<Simulation> RunSimulations(double startingMoney,
			double nashvilleApartmentCost,
			double nashvilleHomePrice,
			double nashvilleHomeDownPaymentPercent,
			double moneyTowardsStudentLoans,
			double moneyEarnedPerMonth) {

			List<Simulation.Priority> seq = new List<Simulation.Priority>();
			foreach (Simulation.Priority priority in Enum.GetValues(typeof(Simulation.Priority))) {
				seq.Add(priority);
			}

			List<Simulation> simulations = new List<Simulation>();

			foreach (IList<Simulation.Priority> permu in Simulation.Permutate(seq, seq.Count)) {

				if (!Simulation.IsPriorityListValid(permu)) {
					continue;
				}

				for (int j = 1; j < 2; j++) {

					Simulation simulation = new Simulation(startingMoney, nashvilleApartmentCost, nashvilleHomePrice, nashvilleHomeDownPaymentPercent, moneyTowardsStudentLoans, moneyEarnedPerMonth, true);
                    simulation.MinimumCookevillePayments = true;
                    simulation.buyHouseWithSpecialLoan = j == 0;
					simulation.Priorities.AddRange(permu);
					simulation.RunSimulation();
					simulations.Add(simulation);

				}
			}

			return simulations;

		}

		private void SimulationsDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e) {
			FinancesDataGrid.ItemsSource = ((Simulation)SimulationsDataGrid.SelectedItem).FinancialData;
		}
	}
}
