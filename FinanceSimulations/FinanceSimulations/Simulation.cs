﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinanceSimulations {
    class Simulation {

        private static readonly int monthsToRunSimulation = 360;
        private static readonly double priusPayment = 439.55;
        private static readonly double roguePayment = 412.50;
		private static readonly double normalCookevilleHomePayment = 700;// 786.99 + Loan.GetCookevilleHomeLoan().EstimatedMonthlyExpenses;
        private static readonly double jamieNoycePayment = 256.86;
        private static readonly double jonathanNoycePayment = 57.80;
        private static readonly double iraInvestmentAmount = 188;
        private static readonly double apy = 0.02;
        private static readonly double minStudentLoanPaymentPer = 42.63;
        private readonly double closingCosts = 0;

        private static void RotateRight(IList<Priority> sequence, int count) {
            Priority tmp = sequence[count - 1];
            sequence.RemoveAt(count - 1);
            sequence.Insert(0, tmp);
        }

        public static IEnumerable<IList<Priority>> Permutate(IList<Priority> sequence, int count) {
            if (count == 1) yield return sequence;
            else {
                for (int i = 0; i < count; i++) {
                    foreach (IList<Priority> perm in Permutate(sequence, count - 1))
                        yield return perm;
                    RotateRight(sequence, count);
                }
            }
        }

        public enum Priority {
            BuyHouse,
            PayOffCookevillePMI,
            PayOffNashvillePMI,
            PayOffCookevilleHouse,
            PayOffNashvilleHouse,
            PayOffStudentLoans,
            BeginIRAContributions,
            Invest,
            SellCookevilleHouse
        }


        public static bool IsPriorityListValid(IList<Priority> priorities) {

			if (priorities.First() != Priority.BuyHouse) {
				return false;
			}

            //Can't pay off a nashvile house you haven't bought yet
            if (priorities.IndexOf(Priority.PayOffNashvilleHouse) < priorities.IndexOf(Priority.BuyHouse) || priorities.IndexOf(Priority.PayOffNashvillePMI) < priorities.IndexOf(Priority.BuyHouse)) {
                return false;
            }

            //Can't pay off a house THEN its PMI
            if (priorities.IndexOf(Priority.PayOffNashvilleHouse) < priorities.IndexOf(Priority.PayOffNashvillePMI)) {
                return false;
            }

            if (priorities.IndexOf(Priority.PayOffCookevilleHouse) < priorities.IndexOf(Priority.PayOffCookevillePMI)) {
                return false;
            }

            //if (priorities.IndexOf(Priority.Invest) < priorities.Count - 2) {
            //    return false;
            //}

            return true;

        }

        public string StartingPriorities { get; private set; }

        private bool _MinimumCookevillePayments;
        public bool MinimumCookevillePayments { get {
                return _MinimumCookevillePayments;
            }
            set {
                if (value) {
					cookevilleHomePayment = 699.17; //+ Loan.GetCookevilleHomeLoan().EstimatedMonthlyExpenses;
                } else {
                    cookevilleHomePayment = normalCookevilleHomePayment;
                }
                _MinimumCookevillePayments = value;
            } }

        public List<Finances> FinancialData = new List<Finances>();
        public List<Priority> Priorities = new List<Priority>();
		public List<Priority> ActualPriorities = new List<Priority>();

        private Loan cookevilleHomeLoan;
        private Loan nashvilleHomeLoan = null;
        private List<Loan> studentLoans;
        private Loan jonathanNoyce;
        private Loan jamieNoyce;
        private Loan prius;
        private Loan rogue;

        private List<Loan> allLoans = new List<Loan>();

        private int currentMonth = 0;
		public int debtFreeMonth = 0;
		public int cookevillePMIMonth = 0;
		public int nashvillePMIMonth = 0;
        private double nashvilleApartmentCost;
        private double nashvilleHomePrice;
        private double nashvilleHomeDownPaymentPercent;
        private double moneyTowardsStudentLoans;
        private double moneyEarnedPerMonth;
        private double cookevilleHomePayment = normalCookevilleHomePayment;

        private double balance;
        private double remainingBalance;
		private double amountUsuallyPaid;
        private double neededDownPayment;
        private string cycleNotes;

        public bool payLoansHighInterestToLow;
        public bool payLoansHighToLow;
        public bool buyHouseWithSpecialLoan;

        private double invested;
		private bool contributeToIRA;// = true;

		private Loan.LoanType loanType;
		private double interestRate;
		private double mortgageInsuraceRate = 0.0085;

        public Simulation(double startingMoney,
            double nashvilleApartmentCost,
            double nashvilleHomePrice,
            double nashvilleHomeDownPaymentPercent,
            double moneyTowardsStudentLoans,
            double moneyEarnedPerMonth,
            bool payLoansHighInterestToLow,
			Loan.LoanType loanType) {

            balance = startingMoney;

            this.nashvilleApartmentCost = nashvilleApartmentCost;
            this.nashvilleHomePrice = nashvilleHomePrice;

			//this.nashvilleHomeDownPaymentPercent = nashvilleHomeDownPaymentPercent;
			switch (loanType) {
				case Loan.LoanType.FHA:
					this.closingCosts = 0.03 * nashvilleHomePrice;
					interestRate = 4.375;
					this.nashvilleHomeDownPaymentPercent = 0.035;
					mortgageInsuraceRate = 0.0085;
					break;
				case Loan.LoanType.Conventional:
					this.closingCosts = 0.03 * nashvilleHomePrice;
					interestRate = 4.625;
					this.nashvilleHomeDownPaymentPercent = 0.05;
					mortgageInsuraceRate = 0.0053;
					break;
				case Loan.LoanType.FHANoClosing:
					this.closingCosts = 0;
					interestRate = 4.875;
					this.nashvilleHomeDownPaymentPercent = 0.035;
					mortgageInsuraceRate = 0.0085;
					break;
				case Loan.LoanType.ConventionalNoClosing:
					this.closingCosts = 0;
					interestRate = 5.25;
					this.nashvilleHomeDownPaymentPercent = 0.05;
					mortgageInsuraceRate = 0.0053;
					break;
				default:
					break;
			}
			
            
            this.moneyTowardsStudentLoans = moneyTowardsStudentLoans;
            this.moneyEarnedPerMonth = moneyEarnedPerMonth;
            this.payLoansHighInterestToLow = payLoansHighInterestToLow;
			this.loanType = loanType;

            neededDownPayment = this.nashvilleHomeDownPaymentPercent * nashvilleHomePrice + closingCosts;

            cookevilleHomeLoan = Loan.GetCookevilleHomeLoan();
            studentLoans = Loan.GetStudentLoans();
            jonathanNoyce = Loan.GetJonathanNoyce();
            jamieNoyce = Loan.GetJamieNoyce();
            prius = Loan.GetPriusLoan();
            rogue = Loan.GetRogueLoan();

            allLoans.Add(cookevilleHomeLoan);
            allLoans.AddRange(studentLoans);
            allLoans.Add(jonathanNoyce);
            allLoans.Add(jamieNoyce);
            allLoans.Add(prius);
            allLoans.Add(rogue);

            allLoans.Sort((i, j) => j.InterestRate.CompareTo(i.InterestRate));

        }

        public void RunSimulation() {

			StartingPriorities = string.Join(", ", Priorities);

			//if (payLoansHighInterestToLow) {
			//	StartingPriorities = string.Join(", ", StartingPriorities, "PayAllLoansFromHighInterestToLow");
			//} else if (payLoansHighToLow) {
			//             StartingPriorities = string.Join(", ", StartingPriorities, "PayAllLoansFromHighToLow");
			//         }

			StartingPriorities = loanType + " - " + StartingPriorities;
			StartingPriorities += string.Format(", {0:0.00} down", neededDownPayment);
			StartingPriorities += string.Format(", {0:0.00}%", interestRate);

            double housePayment;
            if (buyHouseWithSpecialLoan) {
                housePayment = Loan.GetNashvilleSpecialHomeLoan(nashvilleHomePrice, nashvilleHomeDownPaymentPercent, interestRate).GetPaymentAmount();
            } else {
                housePayment = Loan.GetNashvilleHomeLoan(nashvilleHomePrice, nashvilleHomeDownPaymentPercent, interestRate, mortgageInsuraceRate).GetPaymentAmount();
            }
            StartingPriorities += string.Format(", {0:0.00} estimated house payment", housePayment);
            
            //if (MinimumCookevillePayments) {
            //    StartingPriorities += ", Minumum Cookeville Payments";
            //}

            if (buyHouseWithSpecialLoan) {
                StartingPriorities += ", Loan Portfolio";
            }

			cycleNotes = "";
            FinancialData.Add(GetFinances());

            while (currentMonth < monthsToRunSimulation) {
                cycleNotes = "";

                NextMonth();

                FinancialData.Add(GetFinances());

            }
        }

        private void NextMonth() {
            currentMonth += 1;

            Priority? currentPriority;

            do {
                currentPriority = GetCurrentPriority();

                if (currentPriority == Priority.BeginIRAContributions) {
                    contributeToIRA = true;
                }

                if (currentPriority == Priority.SellCookevilleHouse) {
                    balance += 120000 - closingCosts - cookevilleHomeLoan.CurrentPrinciple;

                    allLoans.Remove(cookevilleHomeLoan);
                    cookevilleHomeLoan = null;
                }

            } while (IsPrioritySatisfied(currentPriority));

            balance += moneyEarnedPerMonth;
            remainingBalance = 0;
			amountUsuallyPaid = 0;

            if (nashvilleHomeLoan == null) {
                balance -= nashvilleApartmentCost;
            }

            if (cookevilleHomeLoan == null) {
                balance -= 819;
            }

            ClearDeadStudentLoans();

            if (contributeToIRA) {
                balance -= iraInvestmentAmount;
                invested += 2 * iraInvestmentAmount;
            }

            invested *= (1 + apy / 12);

            switch (currentPriority) {
                default:
                case null:

                    balance -= PayStudentLoans(moneyTowardsStudentLoans);

                    if (nashvilleHomeLoan != null) {
                        balance -= nashvilleHomeLoan.TryToMakePayment(nashvilleHomeLoan.GetPaymentAmount());
                    }

                    if (cookevilleHomeLoan != null) {
                        balance -= cookevilleHomeLoan.TryToMakePayment(cookevilleHomePayment);
                    }
                    balance -= jamieNoyce.TryToMakePayment(jamieNoycePayment);
                    balance -= jonathanNoyce.TryToMakePayment(jonathanNoycePayment);
                    balance -= prius.TryToMakePayment(priusPayment);
                    balance -= rogue.TryToMakePayment(roguePayment);

                    remainingBalance = balance;
					amountUsuallyPaid = 0;
                    if (currentPriority == Priority.Invest) {
                        invested += balance;
                        balance = 0;
                    }

                    break;

                case Priority.BuyHouse:

                    balance -= PayStudentLoans(moneyTowardsStudentLoans);

                    if (nashvilleHomeLoan != null) {
                        balance -= nashvilleHomeLoan.TryToMakePayment(nashvilleHomeLoan.GetPaymentAmount());
                    }

                    if (cookevilleHomeLoan != null) {
                        balance -= cookevilleHomeLoan.TryToMakePayment(cookevilleHomePayment);
                    }
                    balance -= jamieNoyce.TryToMakePayment(jamieNoycePayment);
                    balance -= jonathanNoyce.TryToMakePayment(jonathanNoycePayment);
                    balance -= prius.TryToMakePayment(priusPayment);
                    balance -= rogue.TryToMakePayment(roguePayment);

                    remainingBalance = balance;
					amountUsuallyPaid = 0;
                    if (buyHouseWithSpecialLoan || balance >= neededDownPayment) {
                        if (buyHouseWithSpecialLoan) {
                            nashvilleHomeLoan = Loan.GetNashvilleSpecialHomeLoan(nashvilleHomePrice, 0);
                            balance -= 1000;
                        } else {
                            nashvilleHomeLoan = Loan.GetNashvilleHomeLoan(nashvilleHomePrice, nashvilleHomeDownPaymentPercent, interestRate, mortgageInsuraceRate);
                            balance -= neededDownPayment;
                        }
                        
                        allLoans.Add(nashvilleHomeLoan);
                        allLoans.Sort((i, j) => j.InterestRate.CompareTo(i.InterestRate));
                        
                    }

                    break;
                case Priority.PayOffCookevilleHouse:

                    if (payLoansHighInterestToLow) {
                        PayAllLoansHighInterestToLow();
                        break;
                    }

                    balance -= PayStudentLoans(moneyTowardsStudentLoans);

                    if (nashvilleHomeLoan != null) {
                        balance -= nashvilleHomeLoan.TryToMakePayment(nashvilleHomeLoan.GetPaymentAmount());
                    }

                    balance -= jamieNoyce.TryToMakePayment(jamieNoycePayment);
                    balance -= jonathanNoyce.TryToMakePayment(jonathanNoycePayment);
                    balance -= prius.TryToMakePayment(priusPayment);
                    balance -= rogue.TryToMakePayment(roguePayment);

                    remainingBalance = balance;
					amountUsuallyPaid = cookevilleHomeLoan.GetPaymentAmount();
                    if (cookevilleHomeLoan != null) {
                        balance -= cookevilleHomeLoan.TryToMakePayment(balance);
                    }

                    break;

                case Priority.PayOffCookevillePMI:

                    balance -= PayStudentLoans(moneyTowardsStudentLoans);

                    if (nashvilleHomeLoan != null) {
                        balance -= nashvilleHomeLoan.TryToMakePayment(nashvilleHomeLoan.GetPaymentAmount());
                    }

                    balance -= jamieNoyce.TryToMakePayment(jamieNoycePayment);
                    balance -= jonathanNoyce.TryToMakePayment(jonathanNoycePayment);
                    balance -= prius.TryToMakePayment(priusPayment);
                    balance -= rogue.TryToMakePayment(roguePayment);

                    remainingBalance = balance;
					amountUsuallyPaid = cookevilleHomeLoan.GetPaymentAmount();
                    if (cookevilleHomeLoan != null) {
                        balance -= cookevilleHomeLoan.TryToMakePayment(balance);
                    }

                    break;
                case Priority.PayOffNashvilleHouse:
                

                    if (payLoansHighInterestToLow) {
                        PayAllLoansHighInterestToLow();
                        break;
                    }

                    balance -= PayStudentLoans(moneyTowardsStudentLoans);

                    if (cookevilleHomeLoan != null) {
                        balance -= cookevilleHomeLoan.TryToMakePayment(cookevilleHomePayment);
                    }
                    balance -= jamieNoyce.TryToMakePayment(jamieNoycePayment);
                    balance -= jonathanNoyce.TryToMakePayment(jonathanNoycePayment);
                    balance -= prius.TryToMakePayment(priusPayment);
                    balance -= rogue.TryToMakePayment(roguePayment);

                    remainingBalance = balance;
					amountUsuallyPaid = nashvilleHomeLoan.GetPaymentAmount();
                    if (nashvilleHomeLoan != null) {
                        balance -= nashvilleHomeLoan.TryToMakePayment(balance);
                    } else {
                        Priorities.Insert(0, Priority.BuyHouse);
                    }

                    break;

                case Priority.PayOffNashvillePMI:

					if (ActualPriorities.Count == 2 && ActualPriorities[0] == Priority.BuyHouse && ActualPriorities[1] == Priority.PayOffCookevillePMI) {

					}

                    balance -= PayStudentLoans(moneyTowardsStudentLoans);

                    if (cookevilleHomeLoan != null) {
                        balance -= cookevilleHomeLoan.TryToMakePayment(cookevilleHomePayment);
                    }
                    balance -= jamieNoyce.TryToMakePayment(jamieNoycePayment);
                    balance -= jonathanNoyce.TryToMakePayment(jonathanNoycePayment);
                    balance -= prius.TryToMakePayment(priusPayment);
                    balance -= rogue.TryToMakePayment(roguePayment);

                    remainingBalance = balance;
					amountUsuallyPaid = nashvilleHomeLoan.GetPaymentAmount();
                    if (nashvilleHomeLoan != null) {
                        balance -= nashvilleHomeLoan.TryToMakePayment(balance);
                    } else {
                        Priorities.Insert(0, Priority.BuyHouse);
                    }

                    break;
                case Priority.PayOffStudentLoans:

                    if (payLoansHighInterestToLow) {
                        PayAllLoansHighInterestToLow();
                        break;
                    }

                    if (nashvilleHomeLoan != null) {
                        balance -= nashvilleHomeLoan.TryToMakePayment(nashvilleHomeLoan.GetPaymentAmount());
                    }

                    if (cookevilleHomeLoan != null) {
                        balance -= cookevilleHomeLoan.TryToMakePayment(cookevilleHomePayment);
                    }
                    balance -= prius.TryToMakePayment(priusPayment);
                    balance -= rogue.TryToMakePayment(roguePayment);

                    remainingBalance = balance;
					amountUsuallyPaid = 0;

					if (studentLoans.Count > 0) {
						amountUsuallyPaid += moneyTowardsStudentLoans;
					}
					if (jamieNoyce.CurrentPrinciple >= 0) {
						amountUsuallyPaid += jamieNoyce.GetPaymentAmount();
					}
					if (jonathanNoyce.CurrentPrinciple >= 0) {
						amountUsuallyPaid += jonathanNoyce.GetPaymentAmount();
					}
                    if ((studentLoans.Count > 0 && studentLoans[0].InterestRate > jamieNoyce.InterestRate) || (jamieNoyce.CurrentPrinciple <= 0 && jonathanNoyce.CurrentPrinciple <= 0)) {
                        balance -= jamieNoyce.TryToMakePayment(jamieNoycePayment);
                        balance -= jonathanNoyce.TryToMakePayment(jonathanNoycePayment);

                        balance -= PayStudentLoans(balance);
                    } else {
                        balance -= PayStudentLoans(moneyTowardsStudentLoans);

                        if (jamieNoyce.CurrentPrinciple > jonathanNoyce.CurrentPrinciple) {
                            balance -= jonathanNoyce.TryToMakePayment(jonathanNoycePayment);
                            balance -= jamieNoyce.TryToMakePayment(balance);
                        } else {
                            balance -= jamieNoyce.TryToMakePayment(jamieNoycePayment);
                            balance -= jonathanNoyce.TryToMakePayment(balance);
                        }
                    }

                    break;
            }

            IsPrioritySatisfied(currentPriority);

        }

        private void PayAllLoansHighInterestToLow() {
            if (payLoansHighToLow) {
                allLoans.Sort((i, j) => (j.CurrentPrinciple * j.InterestRate).CompareTo(i.CurrentPrinciple * i.InterestRate));
            }

            Loan loan;
            for (int i = allLoans.Count - 1; i > 0; i--) {
                loan = allLoans[i];

                if (loan == jamieNoyce) {
                    balance -= loan.TryToMakePayment(jamieNoycePayment);
                } else if (loan == jonathanNoyce) {
                    balance -= loan.TryToMakePayment(jonathanNoycePayment);
                } else if (loan == prius) {
                    balance -= loan.TryToMakePayment(priusPayment);
                } else if (loan == rogue) {
                    balance -= loan.TryToMakePayment(roguePayment);
                } else if (loan == cookevilleHomeLoan) {
                    balance -= loan.TryToMakePayment(cookevilleHomePayment);
                } else if (loan == nashvilleHomeLoan) {
                    balance -= loan.TryToMakePayment(loan.GetPaymentAmount());
                } else {
                    balance -= loan.TryToMakePayment(minStudentLoanPaymentPer);
                }

            }

			if (!allLoans.Contains(nashvilleHomeLoan) && nashvilleHomeLoan != null) {
				balance -= nashvilleHomeLoan.EstimatedMonthlyExpenses;
			}

			if (!allLoans.Contains(cookevilleHomeLoan) && cookevilleHomeLoan != null) {
				balance -= cookevilleHomeLoan.EstimatedMonthlyExpenses;
			}

			remainingBalance = balance;
			
            if (allLoans.Count > 0) {
				amountUsuallyPaid = allLoans[0].GetPaymentAmount();
				balance -= allLoans[0].TryToMakePayment(balance);
            }
			
			string paidOff = string.Join(", ", allLoans.Where(i => i.CurrentPrinciple <= 0).Select(i => string.Format("Principle: {0}   Interest: {1}", i.InitialPrinciple, i.InterestRate * 1200)));
			if (!string.IsNullOrWhiteSpace(paidOff)) {
				cycleNotes += string.Format("- Paid off: {0} -", paidOff);
			}

            allLoans.RemoveAll(i => i.CurrentPrinciple <= 0);

        }

        private void ClearDeadStudentLoans() {
            Loan studentLoan;
            for (int i = studentLoans.Count - 1; i >= 0; i--) {
                studentLoan = studentLoans[i];
                if (studentLoan.CurrentPrinciple <= 0) {
                    studentLoans.RemoveAt(i);
                }
            }
        }

        private double PayStudentLoans(double studentLoanPayments) {

            double moneySpent = 0;

            double excess = studentLoanPayments - minStudentLoanPaymentPer * studentLoans.Count;
            double payment;
            double paid;

            foreach (Loan studentLoan in studentLoans) {
                payment = minStudentLoanPaymentPer + excess;
                paid = studentLoan.TryToMakePayment(payment);
                excess = payment - paid;
                moneySpent += paid;
            }

            return moneySpent;
        }

        private Priority? GetCurrentPriority() {
            Priority? currentPriority = null;

            if (Priorities.Count() > 0) {
                currentPriority = Priorities[0];
            }

            return currentPriority;
        }

        private bool IsPrioritySatisfied(Priority? priority) {

			foreach (Priority i in Priorities) {
				if (ActualPriorities.Contains(i)) {
					continue;
				}

				switch (i) {
                    case Priority.SellCookevilleHouse:
                        if (cookevilleHomeLoan == null) {
                            ActualPriorities.Add(i);
                            cycleNotes += "- Sold Cookeville House -";
                        }
                        break;
                    case Priority.BuyHouse:
						if (nashvilleHomeLoan != null) {
							ActualPriorities.Add(i);
                            cycleNotes += string.Format("- Bought House {0:0.00}-", nashvilleHomeLoan.GetPaymentAmount());
                        }
                        break;
					case Priority.PayOffCookevilleHouse:
						if (cookevilleHomeLoan == null || cookevilleHomeLoan.CurrentPrinciple <= 0) {
							ActualPriorities.Add(i);
                            cycleNotes += "- Paid off Cookeville House -";
                        }
						break;
					case Priority.PayOffNashvilleHouse:
						if (nashvilleHomeLoan != null && nashvilleHomeLoan.CurrentPrinciple <= 0) {
							ActualPriorities.Add(i);
                            cycleNotes += "- Paid off nashville house -";
                        }
						break;
					case Priority.PayOffStudentLoans:
						if (studentLoans.Count == 0 && jamieNoyce.CurrentPrinciple <= 0 && jonathanNoyce .CurrentPrinciple <= 0) {
							ActualPriorities.Add(i);
                            cycleNotes += "- Paid off student loans -";
                        }
						break;
					case Priority.PayOffCookevillePMI:
						if (cookevilleHomeLoan == null || !cookevilleHomeLoan.HasMortgageInsurance()) {
							ActualPriorities.Add(i);
                            cycleNotes += "- Paid off Cookeville PMI -";

                            if (MinimumCookevillePayments) {
                                cookevilleHomePayment -= 105.89;
                            }
						}
						break;
					case Priority.PayOffNashvillePMI:
						if (nashvilleHomeLoan != null && !nashvilleHomeLoan.HasMortgageInsurance()) {
							ActualPriorities.Add(i);
                            cycleNotes += "- Paid off Nashville PMI -";
                        }
						break;
					case Priority.BeginIRAContributions:
						if (contributeToIRA) {
							ActualPriorities.Add(i);
                            cycleNotes += "- Contibuting to IRA -";
                        }
						break;
					default:
						break;
				}
			}


            switch (priority) {
                case null:
                    return false;
                case Priority.SellCookevilleHouse:
                    if (cookevilleHomeLoan == null) {
                        Priorities.RemoveAt(0);
                        
                        return true;
                    }
                    break;
                case Priority.BuyHouse:
                    if (nashvilleHomeLoan != null) {
                        Priorities.RemoveAt(0);
                        
                        return true;
                    }
                    break;
                case Priority.PayOffCookevilleHouse:
                    if (cookevilleHomeLoan == null || cookevilleHomeLoan.CurrentPrinciple <= 0) {
                        Priorities.RemoveAt(0);
                        
                        return true;
                    }
                    break;
                case Priority.PayOffNashvilleHouse:
                    if (nashvilleHomeLoan != null && nashvilleHomeLoan.CurrentPrinciple <= 0) {
                        Priorities.RemoveAt(0);
                        
                        return true;
                    }
                    break;
                case Priority.PayOffStudentLoans:
                    if (studentLoans.Count == 0) {
                        Priorities.RemoveAt(0);
                        
                        return true;
                    }
                    break;
                case Priority.PayOffCookevillePMI:
                    if (cookevilleHomeLoan == null || !cookevilleHomeLoan.HasMortgageInsurance()) {
                        Priorities.RemoveAt(0);
                        
                        return true;
                    }
                    break;
                case Priority.PayOffNashvillePMI:
                    if (!nashvilleHomeLoan.HasMortgageInsurance()) {
                        Priorities.RemoveAt(0);
                        
                        return true;
                    }
                    break;
                case Priority.BeginIRAContributions:
                    if (contributeToIRA) {
                        Priorities.RemoveAt(0);
                        
                        return true;
                    }
                    break;
                default:
                    return false;
            }

            return false;
        }

        private Finances GetFinances() {
            Finances finances = new Finances();
            finances.Balance = Math.Round(balance, 2);
            finances.HasHouse = nashvilleHomeLoan != null;
            finances.Month = currentMonth;
            finances.NetWorth = Math.Round(GetNetWorth(), 2);
            finances.Notes = cycleNotes;

            double studentLoanAmount = 0;
            foreach (Loan studentLoan in studentLoans) {
                studentLoanAmount += studentLoan.CurrentPrinciple;
            }
            studentLoanAmount += jamieNoyce.CurrentPrinciple;
            studentLoanAmount += jonathanNoyce.CurrentPrinciple;
            finances.StudentLoans = Math.Round(studentLoanAmount, 2);

            finances.CookevilleLoan = cookevilleHomeLoan == null ? 0 : Math.Round(cookevilleHomeLoan.CurrentPrinciple, 2);
            finances.NashvilleLoan = nashvilleHomeLoan == null ? 0 : Math.Round(nashvilleHomeLoan.CurrentPrinciple, 2);
            finances.PriusLoan = Math.Round(prius.CurrentPrinciple, 2);
            finances.RogueLoan = Math.Round(rogue.CurrentPrinciple, 2);

            finances.Invested = Math.Round(invested, 2);

            finances.RemainingBalance = Math.Round(remainingBalance, 2);
			finances.AmountUsuallyPaid = Math.Round(amountUsuallyPaid, 2);
            remainingBalance = 0;
			amountUsuallyPaid = 0;

			if (debtFreeMonth == 0 && finances.TotalDebt <= 0) {
				debtFreeMonth = currentMonth;
				StartingPriorities += ", " + debtFreeMonth;
			}

			if (cookevillePMIMonth == 0 && cookevilleHomeLoan != null && !cookevilleHomeLoan.HasMortgageInsurance()) {
				cookevillePMIMonth = currentMonth;
				StartingPriorities += ", CPMI " + cookevillePMIMonth; 
			}

			if (nashvillePMIMonth == 0 && nashvilleHomeLoan != null && !nashvilleHomeLoan.HasMortgageInsurance()) {
				nashvillePMIMonth = currentMonth;
				StartingPriorities += ", NPMI " + nashvillePMIMonth;
			}

			return finances;
        }

        private double GetNetWorth() {
            double netWorth = balance - (cookevilleHomeLoan == null ? 0 : cookevilleHomeLoan.CurrentPrinciple + cookevilleHomeLoan.Worth);

            foreach (Loan studentLoan in studentLoans) {
                netWorth -= studentLoan.CurrentPrinciple;
            }

            netWorth -= jamieNoyce.CurrentPrinciple;
            netWorth -= jonathanNoyce.CurrentPrinciple;

            if (nashvilleHomeLoan != null) {
                netWorth += nashvilleHomeLoan.Worth - nashvilleHomeLoan.CurrentPrinciple;
            }

            netWorth += invested;

            return netWorth;
        }

    }
}
