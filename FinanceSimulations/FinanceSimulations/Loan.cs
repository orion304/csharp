﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinanceSimulations {
    class Loan {

		public enum LoanType {
			FHA,
			Conventional,
			FHANoClosing,
			ConventionalNoClosing
		}

        public static double GetPMI(double loanAmount) {
            return 0.0127 * loanAmount / 12;
        }

        public static double GetLoanPayment(double loanAmount, double interestRate, int durationMonths) {
            double interest = interestRate / 1200;
            double r = Math.Pow(1 + interest, durationMonths);
            return loanAmount * interest * r / (r - 1);
        }

        public static Loan GetCookevilleHomeLoan() {
            return new Loan(88173.66, 4.125, 323, 123.17-42, 105.89, 100000, 100000);
        }

        public static Loan GetNashvilleHomeLoan(double homePrice, double downPaymentPercent = 0.05, double interestRate = 4.375, double mortgageInsurancePercent = 0.0085) {
			//return new Loan((1 - downPaymentPercent) * homePrice, interestRate, 360, 0, 0, (1 - downPaymentPercent) * homePrice, homePrice);
			return new Loan((1 - downPaymentPercent) * homePrice, interestRate, 360, (homePrice * 0.01 + 1200) / 12 , homePrice * mortgageInsurancePercent / 12, (1 - downPaymentPercent) * homePrice, homePrice);
        }

        public static Loan GetNashvilleSpecialHomeLoan(double homePrice, double downPaymentPercent = 0.05, double interestRate = 5.125) {
            return new Loan((1 - downPaymentPercent) * homePrice, interestRate, 360, homePrice * estimatedMonthlyExpensesForHomeFactor, 0, homePrice, homePrice);
        }

        public static Loan GetJonathanNoyce() {
            return new Loan(3000, 5);
        }

        public static Loan GetJamieNoyce() {
            return new Loan(15000, 5);
        }

        public static Loan GetPriusLoan() {
            return new Loan(11867.40, 0);
        }

        public static Loan GetRogueLoan() {
            return new Loan(23512.50, 0);
        }

        public static List<Loan> GetStudentLoans() {
            List<Loan> loans = new List<Loan>();

            loans.Add(new Loan(2677.28, 4.250));
            loans.Add(new Loan(1218.17, 6.550));
            loans.Add(new Loan(4552.38, 3.150));
            loans.Add(new Loan(1102.69, 6.550));
            loans.Add(new Loan(4603.60, 3.150));
            loans.Add(new Loan(1031.32, 6.550));
            loans.Add(new Loan(4614.60, 3.610));
            loans.Add(new Loan(3663.06, 5.160));

            loans.Sort((loan1, loan2) => loan2.InterestRate.CompareTo(loan1.InterestRate));

            return loans;
        }

        private static readonly double mortgageInsuranceThreshold = 0.8;
        private static readonly double estimatedMonthlyExpensesForHomeFactor = 0.0152173913043 / 12;
        private static readonly double estimatedMortgageInsuraceFactor = 0.013 / 12;

        public double CurrentPrinciple { get; private set; }
        public double Worth { get; private set; }
        public double InterestRate { get; private set; }
		public double EstimatedMonthlyExpenses { get; set; }
		public double InitialPrinciple { get; set; }

        private int durationMonths;
        private double mortgageInsurance;
        private double defaultPaymentAmount;

        public Loan(double currentPrinciple, double interestRate, int durationMonths = 0, double estimatedMonthlyExpenses = 0, double mortgageInsurance = 0, double initialPrinciple = 0, double worth = 0) {
            if (initialPrinciple > 0) {
                this.InitialPrinciple = initialPrinciple;
            } else {
                this.InitialPrinciple = currentPrinciple;
            }

            CurrentPrinciple = currentPrinciple;
            Worth = worth;
            this.InterestRate = interestRate / 1200;
            this.durationMonths = durationMonths;
            this.EstimatedMonthlyExpenses = estimatedMonthlyExpenses;
            this.mortgageInsurance = mortgageInsurance;

            defaultPaymentAmount = GetLoanPayment(initialPrinciple, interestRate, durationMonths) + estimatedMonthlyExpenses + mortgageInsurance;
        }

        public void MakePayment(double payment) {
            if (mortgageInsurance > 0 && (CurrentPrinciple / InitialPrinciple) < mortgageInsuranceThreshold) {
                defaultPaymentAmount -= mortgageInsurance;
                mortgageInsurance = 0;
            }

            double interest = CurrentPrinciple * InterestRate;

			if (CurrentPrinciple <= 0 && payment > 0) {
				//System.Diagnostics.Debugger.Break();
			}

            CurrentPrinciple -= (payment - interest - EstimatedMonthlyExpenses - mortgageInsurance);

            if (mortgageInsurance > 0 && (CurrentPrinciple / InitialPrinciple) < mortgageInsuranceThreshold) {
                defaultPaymentAmount -= mortgageInsurance;
                mortgageInsurance = 0;
            }
        }

        public double GetPayoffAmount() {
            return CurrentPrinciple * (1 + InterestRate);
        }

        public double TryToMakePayment(double payment) {
            double necessaryPayoff = GetPayoffAmount() + EstimatedMonthlyExpenses + mortgageInsurance;
            if (payment > necessaryPayoff) {
                payment = necessaryPayoff;
            }

            MakePayment(payment);

            return payment;
        }

        public double GetPaymentAmount() {
            return defaultPaymentAmount;
        }

        public bool HasMortgageInsurance() {
            return mortgageInsurance > 0;
        }

    }
}
