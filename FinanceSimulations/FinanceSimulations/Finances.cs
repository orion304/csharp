﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinanceSimulations {
    class Finances {

        public int Month { get; set; }
        public double Balance { get; set; }
        public double NetWorth { get; set; }
		public double Invested { get; set; }
		public double StudentLoans { get; set; }
        public double CookevilleLoan { get; set; }
        public double NashvilleLoan { get; set; }
        public double PriusLoan { get; set; }
        public double RogueLoan { get; set; }
        public double RemainingBalance { get; set; }
		public double AmountUsuallyPaid { get; set; }
        public bool HasHouse { get; set; }
        public string Notes { get; set; }
		public double TotalDebt {
			get {
				return StudentLoans + CookevilleLoan + NashvilleLoan + PriusLoan + RogueLoan;
			}
		}
        


    }
}
