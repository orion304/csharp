﻿using Framework.Entities;
using System;

namespace Framework {
	public class WorkPeriod {

		public WorkPeriod(WorkMessage from, WorkMessage to) {
			StartTime = from.Time;
			StartLongitude = from.Longitude;
			StartLatitude = from.Latitude;

			EndTime = to.Time;
			EndLatitude = to.Latitude;
			EndLongitude = to.Longitude;
		}

		public DateTime StartTime { get; set; }

		public DateTime EndTime { get; set; }

		public double StartLongitude { get; set; }
		public double StartLatitude { get; set; }

		public double EndLongitude { get; set; }
		public double EndLatitude { get; set; }

		public double DurationHours => EndTime.Subtract(StartTime).TotalHours;

	}
}
