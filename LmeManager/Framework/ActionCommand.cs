﻿using System;
using System.Windows.Input;

namespace Framework {
	public class ActionCommand1P <T> : ICommand {
		public event EventHandler CanExecuteChanged;

		private Action<T> _action;

		public ActionCommand1P(Action<T> action) {
			_action = action;
		}

		public bool CanExecute(object parameter) {
			return true;
		}

		public void Execute(object parameter) {
			_action?.Invoke((T)parameter);
		}
	}
}
