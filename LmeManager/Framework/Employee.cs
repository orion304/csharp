﻿using Framework.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Framework {
	public class Employee {

		private const bool HandleRounding = true;

		public static readonly Dictionary<string, Employee> Employees = new Dictionary<string, Employee>();

		public static Employee Create(string name) {
			if (Employees.ContainsKey(name)) {
				return Employees[name];
			}

			var employee = new Employee { Name = name };
			Employees[name] = employee;
			return employee;
		}

		private readonly List<WorkMessage> _rawWorkMessages = new List<WorkMessage>();

		private Employee() { }

		public string Name { get; set; }

		public bool HasErrors { get; set; }

		public List<WorkPeriod> WorkPeriods { get; set; } = new List<WorkPeriod>();

		public double TotalHoursWorked => WorkPeriods.Sum(p => p.DurationHours);

		public bool IsFinished => !_hasStarted;

		private bool _hasStarted;
		private WorkMessage _startMessage;

		public void AddWorkMessage(WorkMessage message) {
			_rawWorkMessages.Add(message);

			if (_hasStarted && message.Start) {
				HasErrors = true;
				_startMessage = message;
				FlagLastTwoWorkMessagesAsSuspect();
				return;
			}

			if (!_hasStarted && !message.Start) {
				HasErrors = true;
				FlagLastTwoWorkMessagesAsSuspect();
				return;
			}

			if (message.Start) {
				_hasStarted = true;
				_startMessage = message;
			} else {
				_hasStarted = false;

				var workPeriod = new WorkPeriod(_startMessage, message);
				WorkPeriods.Add(workPeriod);
				_startMessage = null;
			}
		}

		private void FlagLastTwoWorkMessagesAsSuspect() {
			var count = _rawWorkMessages.Count;

			var index1 = count - 1;
			var index2 = count - 2;

			FlagWorkMessageAtIndexAsSuspect(index1);
			FlagWorkMessageAtIndexAsSuspect(index2);
		}

		private void FlagWorkMessageAtIndexAsSuspect(int index) {
			if (index < 0) return;

			_rawWorkMessages[index].Suspect = true;
		}

		public List<WorkPeriod> GetPeriodsWorkedByDate(DateTime date) {
			return WorkPeriods.Where(p => p.StartTime.Date.Equals(date.Date)).ToList();
		}

		public double GetHoursWorkedOnDate(DateTime date) {
			var hours = WorkPeriods.Where(p => p.StartTime.Date.Equals(date.Date)).Sum(p => p.DurationHours);
			return HandleRounding ? Math.Round(hours * 4) / 4 : hours;
		}

	}
}
