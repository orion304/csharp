﻿using Framework.Entities;
using Google.Cloud.Datastore.V1;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace Framework.GoogleApi {
	public static class Datastore {

		private const string ProjectId = "infra-inkwell-210102";
		private const string ServiceAccountApiFileName = @"ServiceAccountApi.json";
		private const string WorkMessageKind = "WorkMessage";

		private static readonly string ServiceAccountApiResource = $@"Framework.{ServiceAccountApiFileName}";
		private static readonly string AppDataFolder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "LmeManager");

		private static readonly DatastoreDb _db;

		static Datastore() {
			var directoryInfo = new DirectoryInfo(AppDataFolder);
			if (!directoryInfo.Exists) directoryInfo.Create();


			var filePath = Path.Combine(AppDataFolder, ServiceAccountApiFileName);
			WriteResourceToFile(ServiceAccountApiResource, filePath);
			Environment.SetEnvironmentVariable("GOOGLE_APPLICATION_CREDENTIALS", filePath);

			_db = DatastoreDb.Create(ProjectId);
		}

		public static List<WorkMessage> GetWorkMessagesBetweenDates(DateTime from, DateTime to) {
			
			var query = new Query(WorkMessageKind) {
				Filter =  Filter.And(
					Filter.GreaterThanOrEqual("Time", from.ToUniversalTime()),
					Filter.LessThan("Time", to.ToUniversalTime())
				),
				Order = { { "Time", PropertyOrder.Types.Direction.Ascending } }
			};
			var results = _db.RunQuery(query);

			return results.Entities.Select(entity => WorkMessage.Create(entity)).ToList();

		}

		public static void WriteResourceToFile(string resourceName, string fileName) {
			using (var resource = Assembly.GetExecutingAssembly().GetManifestResourceStream(resourceName)) {
				using (var file = new FileStream(fileName, FileMode.Create, FileAccess.Write)) {
					resource.CopyTo(file);
				}
			}
		}

	}
}
