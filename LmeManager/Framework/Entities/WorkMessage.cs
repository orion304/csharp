﻿using Google.Cloud.Datastore.V1;
using System;

namespace Framework.Entities {
	public class WorkMessage {

		public static WorkMessage Create(Entity entity) {
			var workMessage = new WorkMessage();

			if (entity.Properties.TryGetValue("Name", out var value)) {
				workMessage.Name = value.StringValue;
			}
			if (entity.Properties.TryGetValue("Time", out value)) {
				workMessage.Time = value.TimestampValue.ToDateTime();
			}
			if (entity.Properties.TryGetValue("Start", out value)) {
				workMessage.Start = value.BooleanValue;
			}
			if (entity.Properties.TryGetValue("Manual", out value)) {
				workMessage.Manual = value.BooleanValue;
			}
			if (entity.Properties.TryGetValue("Location", out value)) {
				workMessage.Latitude = value.GeoPointValue.Latitude;
				workMessage.Longitude = value.GeoPointValue.Longitude;
			}

			return workMessage;
		}

		public string Name { get; set; }

		public DateTime Time { get; set; }

		public bool Start { get; set; }

		public bool Manual { get; set; }

		public double Latitude { get; set; }

		public double Longitude { get; set; }

		public bool Suspect { get; set; }

	}
}
