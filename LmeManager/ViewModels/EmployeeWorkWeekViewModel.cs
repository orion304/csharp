﻿using Framework;
using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace ViewModels {
	public class EmployeeWorkWeekViewModel : INotifyPropertyChanged {

		private Employee _employee;

		public EmployeeWorkWeekViewModel(Employee employee, DateTime from) {
			_employee = employee;
			Name = employee.Name;

			for (var i = 0; i < 7; i++) {
				var currentDate = from.AddDays(i);
				var hours = employee.GetHoursWorkedOnDate(currentDate);

				switch (currentDate.DayOfWeek) {
					case DayOfWeek.Sunday:
						WeekendHours += hours;
						break;
					case DayOfWeek.Monday:
						MondayHours += hours;
						break;
					case DayOfWeek.Tuesday:
						TuesdayHours += hours;
						break;
					case DayOfWeek.Wednesday:
						WednesdayHours += hours;
						break;
					case DayOfWeek.Thursday:
						ThursdayHours += hours;
						break;
					case DayOfWeek.Friday:
						FridayHours += hours;
						break;
					case DayOfWeek.Saturday:
						WeekendHours += hours;
						break;
					default:
						break;
				}
			}
		}

		private string _name;

		public string Name {
			get { return _name; }
			set {
				_name = value;
				OnPropertyChanged();
			}
		}

		private double _mondayHours;

		public double MondayHours {
			get { return _mondayHours; }
			set {
				_mondayHours = value;
				OnPropertyChanged();
				OnPropertyChanged(nameof(TotalHours));
			}
		}

		private double _tuesdayHours;

		public double TuesdayHours {
			get { return _tuesdayHours; }
			set {
				_tuesdayHours = value;
				OnPropertyChanged();
				OnPropertyChanged(nameof(TotalHours));
			}
		}
		private double _wednesdayHours;

		public double WednesdayHours {
			get { return _wednesdayHours; }
			set {
				_wednesdayHours = value;
				OnPropertyChanged();
				OnPropertyChanged(nameof(TotalHours));
			}
		}
		private double _thursdayHours;

		public double ThursdayHours {
			get { return _thursdayHours; }
			set {
				_thursdayHours = value;
				OnPropertyChanged();
				OnPropertyChanged(nameof(TotalHours));
			}
		}
		private double _fridayHours;

		public double FridayHours {
			get { return _fridayHours; }
			set {
				_fridayHours = value;
				OnPropertyChanged();
				OnPropertyChanged(nameof(TotalHours));
			}
		}
		private double _weekendHours;

		public double WeekendHours {
			get { return _weekendHours; }
			set {
				_weekendHours = value;
				OnPropertyChanged();
				OnPropertyChanged(nameof(TotalHours));
			}
		}

		public double TotalHours => MondayHours + TuesdayHours + WednesdayHours + ThursdayHours + FridayHours + WeekendHours;

		public event PropertyChangedEventHandler PropertyChanged;
		public void OnPropertyChanged([CallerMemberName] string propertyName = null) {
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}
