﻿using Framework;
using Framework.GoogleApi;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Input;

namespace ViewModels {
	public class EmployeesWorkViewModel : INotifyPropertyChanged {

		public EmployeesWorkViewModel() { }

		private DateTime _weekStartingFrom = GetStartOfWeek(DateTime.Now);

		public DateTime WeekStartingFrom {
			get { return _weekStartingFrom; }
			set {
				_weekStartingFrom = GetStartOfWeek(value);
				OnPropertyChanged();
			}
		}

		private static DateTime GetStartOfWeek(DateTime dateTime) {
			var diff = (7 + ((int)dateTime.DayOfWeek - 1)) % 7;
			return dateTime.AddDays(-1 * diff).Date;
		}

		private double _socialSecurity = 6.20;

		public double SocialSecurity {
			get { return _socialSecurity; }
			set {
				_socialSecurity = value;
				OnPropertyChanged();
			}
		}

		private double _medicare = 1.45;

		public double Medicare {
			get { return _medicare; }
			set {
				_medicare = value;
				OnPropertyChanged();
			}
		}



		public ObservableCollection<EmployeeWorkWeekViewModel> EmployeesWorkWeeks { get; set; } = new ObservableCollection<EmployeeWorkWeekViewModel>();

		private ICommand _getWorkWeekStartingCommand;

		public event PropertyChangedEventHandler PropertyChanged;
		public void OnPropertyChanged([CallerMemberName] string propertyName = null) {
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}

		public ICommand GetWorkWeekStartingCommand => _getWorkWeekStartingCommand ?? (_getWorkWeekStartingCommand = new ActionCommand1P<DateTime>(GetWorkWeekStarting));

		private void GetWorkWeekStarting(DateTime from) {
			var messages = Datastore.GetWorkMessagesBetweenDates(from, from.AddDays(7));

			EmployeesWorkWeeks.Clear();
			Employee.Employees.Clear();

			foreach (var message in messages) {
				var employee = Employee.Create(message.Name);
				employee.AddWorkMessage(message);
			}

			foreach (var employee in Employee.Employees.Values) {
				EmployeesWorkWeeks.Add(new EmployeeWorkWeekViewModel(employee, from));
			}
		}

	}
}
