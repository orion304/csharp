﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorldGenerator.NPC {
    public class Stat {

        public string name;
        public string shortname;

        public static readonly Stat STR = new Stat("Strength", "STR");
        public static readonly Stat DEX = new Stat("Dexterity", "DEX");
        public static readonly Stat CON = new Stat("Constitution", "CON");
        public static readonly Stat INT = new Stat("Intelligence", "INT");
        public static readonly Stat WIS = new Stat("Wisdom", "WIS");
        public static readonly Stat CHA = new Stat("Charisma", "CHA");

        private Stat(string name, string shortname) {
            this.name = name;
        }

        public override string ToString() {
            return name;
        }
    }
}
