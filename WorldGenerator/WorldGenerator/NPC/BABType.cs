﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorldGenerator.NPC {
    public class BABType {

        public string name;
        private double multiplier;

        public static readonly BABType GOOD = new BABType("Good", 1);
        public static readonly BABType AVERAGE = new BABType("Average", .75);
        public static readonly BABType BAD = new BABType("Bad", .5);

        private BABType(string name, double multiplier) {
            this.name = name;
            this.multiplier = multiplier;
        }

        public int getBAB(int level) {
            return (int) Math.Floor(multiplier * level);
        }

        public override string ToString() {
            return name;
        }
    }
}
