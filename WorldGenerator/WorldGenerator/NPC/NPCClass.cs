﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorldGenerator.NPC {
    public class NPCClass : IComparable<NPCClass> {

        static NPCClass() {
            //Add class skills here


            //Add ability score preferences here
            Adept.availableArchetypes.Add(StatArchetype.DIVINE);

            Aristocrat.availableArchetypes.Add(StatArchetype.MELEE);
            Aristocrat.availableArchetypes.Add(StatArchetype.RANGED);
            Aristocrat.availableArchetypes.Add(StatArchetype.SKILL);

            Barbarian.availableArchetypes.Add(StatArchetype.MELEE);

            Bard.availableArchetypes.Add(StatArchetype.ARCANE_CHA);
            Bard.availableArchetypes.Add(StatArchetype.RANGED);
            Bard.availableArchetypes.Add(StatArchetype.SKILL);

            Child.availableArchetypes.Add(StatArchetype.MELEE);
            Child.availableArchetypes.Add(StatArchetype.RANGED);
            Child.availableArchetypes.Add(StatArchetype.SKILL);

            Cleric.availableArchetypes.Add(StatArchetype.DIVINE);

            Commoner.availableArchetypes.Add(StatArchetype.MELEE);
            Commoner.availableArchetypes.Add(StatArchetype.RANGED);
            Commoner.availableArchetypes.Add(StatArchetype.SKILL);

            Druid.availableArchetypes.Add(StatArchetype.DIVINE);
            Druid.availableArchetypes.Add(StatArchetype.MELEE);
            Druid.availableArchetypes.Add(StatArchetype.RANGED);

            Expert.availableArchetypes.Add(StatArchetype.SKILL);

            Fighter.availableArchetypes.Add(StatArchetype.MELEE);
            Fighter.availableArchetypes.Add(StatArchetype.RANGED);

            Monk.availableArchetypes.Add(StatArchetype.MELEE);

            Paladin.availableArchetypes.Add(StatArchetype.MELEE);
            Paladin.availableArchetypes.Add(StatArchetype.DIVINE);

            Rogue.availableArchetypes.Add(StatArchetype.MELEE);
            Rogue.availableArchetypes.Add(StatArchetype.RANGED);
            Rogue.availableArchetypes.Add(StatArchetype.SKILL);

            Ranger.availableArchetypes.Add(StatArchetype.MELEE);
            Ranger.availableArchetypes.Add(StatArchetype.RANGED);
            Ranger.availableArchetypes.Add(StatArchetype.SKILL);

            Sorcerer.availableArchetypes.Add(StatArchetype.ARCANE_CHA);

            Warrior.availableArchetypes.Add(StatArchetype.MELEE);
            Warrior.availableArchetypes.Add(StatArchetype.RANGED);

            Wizard.availableArchetypes.Add(StatArchetype.ARCANE);
        }

        public static readonly NPCClass Adept = new NPCClass("Adept", 1, 6, BABType.BAD, 6, 2, false, false, true);
        public static readonly NPCClass Aristocrat = new NPCClass("Aristocrat", 1, 4, BABType.AVERAGE, 8, 4, false, false, true);
        public static readonly NPCClass Barbarian = new NPCClass("Barbarian", 1, 4, BABType.GOOD, 12, 4, true, false, false);
        public static readonly NPCClass Bard = new NPCClass("Bard", 1, 6, BABType.AVERAGE, 8, 6, false, true, true);
        public static readonly NPCClass Child = new NPCClass("Child", 0, 0, BABType.BAD, 6, 2, false, false, false);
        public static readonly NPCClass Cleric = new NPCClass("Cleric", 1, 6, BABType.AVERAGE, 8, 2, true, false, true);
        public static readonly NPCClass Commoner = new NPCClass("Commoner", 4, 4, BABType.BAD, 6, 2, false, false, false);
        public static readonly NPCClass Druid = new NPCClass("Druid", 1, 6, BABType.AVERAGE, 8, 4, true, false, true);
        public static readonly NPCClass Expert = new NPCClass("Expert", 3, 4, BABType.AVERAGE, 8, 6, false, false, true);
        public static readonly NPCClass Fighter = new NPCClass("Fighter", 1, 8, BABType.GOOD, 10, 2, true, false, false);
        public static readonly NPCClass Monk = new NPCClass("Monk", 1, 4, BABType.AVERAGE, 8, 4, true, true, true);
        public static readonly NPCClass Paladin = new NPCClass("Paladin", 1, 3, BABType.GOOD, 10, 2, true, false, true);
        public static readonly NPCClass Rogue = new NPCClass("Rogue", 1, 8, BABType.AVERAGE, 8, 8, false, true, false);
        public static readonly NPCClass Ranger = new NPCClass("Ranger", 1, 3, BABType.GOOD, 10, 6, true, true, false);
        public static readonly NPCClass Sorcerer = new NPCClass("Sorcerer", 1, 4, BABType.BAD, 6, 2, false, false, true);
        public static readonly NPCClass Warrior = new NPCClass("Warrior", 2, 4, BABType.AVERAGE, 10, 2, true, false, false);
        public static readonly NPCClass Wizard = new NPCClass("Wizard", 1, 4, BABType.BAD, 6, 2, false, false, true);

        public static IEnumerable<NPCClass> Values {
            get {
                yield return Adept;
                yield return Aristocrat;
                yield return Barbarian;
                yield return Bard;
                yield return Child;
                yield return Cleric;
                yield return Commoner;
                yield return Druid;
                yield return Expert;
                yield return Fighter;
                yield return Monk;
                yield return Paladin;
                yield return Rogue;
                yield return Ranger;
                yield return Sorcerer;
                yield return Warrior;
                yield return Wizard;
            }
        }

        public readonly string className;
        public readonly int roll;
        public readonly int die;
        public readonly BABType babType;
        public readonly int hitDie;
        public readonly int baseSkills;
        public readonly bool isFortGood, isRefGood, isWillGood;

        public List<StatArchetype> availableArchetypes = new List<StatArchetype>();
        private NPCClass(string className, int roll, int die, BABType babType, int hitDie, int baseSkills, bool isFortGood, bool isRefGood, bool isWillGood) {
            this.className = className;
            this.roll = roll;
            this.die = die;
            this.babType = babType;
            this.hitDie = hitDie;
            this.baseSkills = baseSkills;
            this.isFortGood = isFortGood;
            this.isRefGood = isRefGood;
            this.isWillGood = isWillGood;
        }

        public StatArchetype getRandomArchetype() {
            return availableArchetypes[RandomSeed.rnd.Next(availableArchetypes.Count)];
        }

        public override string ToString() {
            return className;
        }

        public int CompareTo(NPCClass obj) {
            return className.CompareTo(obj.className);
        }
    }
}
