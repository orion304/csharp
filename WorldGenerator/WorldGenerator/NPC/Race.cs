﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorldGenerator.Distributions;
using WorldGenerator.NPC.Names;

namespace WorldGenerator.NPC {
    public class Race {

        private static readonly Dictionary<CityEnums.CityMixType, double[]> weights = new Dictionary<CityEnums.CityMixType, double[]>();
        public static PercentageDistribution<Race> raceDistribution = null;

        public static void weightRacialDistribution(CityEnums.CityMixType mixType, Race primaryRace) {
            raceDistribution.clear();
            int i = -1;
            
            foreach (double weight in weights[mixType]) {
                if (i == -1) {
                    raceDistribution.setPercentage(weight, primaryRace);
                    i++;
                    continue;
                } else if (races[i] == primaryRace) {
                    i++;
                    
                }
                raceDistribution.setPercentage(weight, races[i]);
                i++;
            }
        }

        static Race() {
            raceDistribution = new PercentageDistribution<Race>();
            
            weights[CityEnums.CityMixType.Isolated] = new double[] { .96, .02, .01, .01, 0, 0, 0 };
            weights[CityEnums.CityMixType.Mixed] = new double[] { .79, .09, .05, .03, .02, .01, .01 };
            weights[CityEnums.CityMixType.Integrated] = new double[] { .37, .20, .18, .10, .07, .05, .03 };

            weightRacialDistribution(CityEnums.CityMixType.Integrated, HUMAN);
        }

        public struct Age {
            public int HumanAge;
            public int RacialAge;

            public override string ToString() {
                return String.Format("{0} ({1})", RacialAge, HumanAge);
            }
        }

        public struct HeightAndWeight {
            public int Height;
            public int Weight;

            public override string ToString() {
                return String.Format("{0}' {1}\", {2} lb", Height / 12, Height - Height / 12, Weight);
            }
        }

        public static readonly Race HUMAN = new Race("Human", 15, 70);
        public static readonly Race HALF_ELF = new Race("Half-Elf", 20, 125);
        public static readonly Race ELF = new Race("Elf", 110, 350);
        public static readonly Race DWARF = new Race("Dwarf", 40, 250);
        public static readonly Race GNOME = new Race("Gnome", 40, 200);
        public static readonly Race HALFLING = new Race("Halfling", 20, 100);
        public static readonly Race HALF_ORC = new Race("Half-Orc", 14, 60);

        private static readonly Race[] races = { HUMAN, HALFLING, ELF, DWARF, GNOME, HALF_ELF, HALF_ORC };

        public readonly string name;
        private readonly int adulthoodAge;
        private readonly int venerableAge;
        private Race(string name, int adulthoodAge, int venerableAge) {
            this.name = name;
            this.adulthoodAge = adulthoodAge;
            this.venerableAge = venerableAge;
        }

        public HeightAndWeight getRandomHeightAndWeight(NPCEnums.Sex sex, Age age) {
            HeightAndWeight heightAndWeight = new HeightAndWeight();

            int height, weight;
            int r1, r2;

            if (this == HALF_ELF) {
                r1 = Dice.d8() + Dice.d8();
                r2 = Dice.d4() + Dice.d4();

                if (sex == NPCEnums.Sex.Male) {
                    height = 48 + 5 + r1;
                    weight = 100 + r1 * r2;
                } else {
                    height = 48 + 5 + r1;
                    weight = 80 + r1 * r2;
                }
            } else if (this == ELF) {
                r1 = Dice.d6() + Dice.d6();
                r2 = Dice.d4();

                if (sex == NPCEnums.Sex.Male) {
                    height = 48 + 5 + r1;
                    weight = 85 + r1 * r2;
                } else {
                    height = 48 + 5 + r1;
                    weight = 80 + r1 * r2;
                }
            } else if (this == DWARF) {
                r1 = Dice.d4() + Dice.d4();
                r2 = Dice.d6() + Dice.d6();

                if (sex == NPCEnums.Sex.Male) {
                    height = 36 + 9 + r1;
                    weight = 130 + r1 * r2;
                } else {
                    height = 36 + 7 + r1;
                    weight = 100 + r1 * r2;
                }
            } else if (this == GNOME) {
                r1 = Dice.d4() + Dice.d4();
                r2 = 1;

                if (sex == NPCEnums.Sex.Male) {
                    height = 36 + 0 + r1;
                    weight = 40 + r1 * r2;
                } else {
                    height = 24 + 10 + r1;
                    weight = 35 + r1 * r2;
                }
            } else if (this == HALFLING) {
                r1 = Dice.d4() + Dice.d4();
                r2 = 1;

                if (sex == NPCEnums.Sex.Male) {
                    height = 24 + 8 + r1;
                    weight = 30 + r1 * r2;
                } else {
                    height = 24 + 6 + r1;
                    weight = 25 + r1 * r2;
                }
            } else if (this == HALF_ORC) {
                r1 = Dice.d12() + Dice.d12();
                r2 = Dice.d6() + Dice.d6();

                if (sex == NPCEnums.Sex.Male) {
                    height = 48 + 10 + r1;
                    weight = 150 + r1 * r2;
                } else {
                    height = 48 + 5 + r1;
                    weight = 110 + r1 * r2;
                }
            } else {
                r1 = Dice.d10() + Dice.d10();
                r2 = Dice.d4() + Dice.d4();

                if (sex == NPCEnums.Sex.Male) {
                    height = 48 + 10 + r1;
                    weight = 120 + r1 * r2;
                } else {
                    height = 48 + 5 + r1;
                    weight = 85 + r1 * r2;
                }
            }

            if (age.HumanAge < HUMAN.adulthoodAge) {
                double mult = (double)age.HumanAge / (double)HUMAN.adulthoodAge;
                height = (int)(height * mult);
                weight = (int)(weight * mult);
            }

            heightAndWeight.Height = height;
            heightAndWeight.Weight = weight;
            return heightAndWeight;
        }

        public static int getHumanEquivalentAge(Race race, int raceAge) {

            if (race == HUMAN) {
                return raceAge;
            }

            if (raceAge < race.adulthoodAge) {
                return (int)((double)raceAge * (double)HUMAN.adulthoodAge / (double)race.adulthoodAge);
            }
            else {
                return (int)(((double)raceAge - (double)race.adulthoodAge) * (double)HUMAN.venerableAge / (double)race.venerableAge + (double)HUMAN.adulthoodAge);
            }

        }

        public static int getRaceEquivalentAge(Race race, int humanAge) {

            if (race == HUMAN) {
                return humanAge;
            }

            if (humanAge < HUMAN.adulthoodAge) {
                return (int)((double)humanAge * (double)race.adulthoodAge / (double)HUMAN.adulthoodAge);
            }
            else {
                return (int)(((double)humanAge - (double)HUMAN.adulthoodAge) * (double)race.venerableAge / (double)HUMAN.venerableAge + (double)race.adulthoodAge);
            }

        }

        public Age getRandomChildAge() {
            double r = RandomSeed.rnd.NextDouble();

            Age age = new Age();

            if (r <= 0.4) {
                age.HumanAge = RandomSeed.rnd.Next(0, 5);
            }
            else if (r <= 0.72) {
                age.HumanAge = RandomSeed.rnd.Next(5, 10);
            }
            else {
                age.HumanAge = RandomSeed.rnd.Next(10, 15);
            }

            age.RacialAge = getRaceEquivalentAge(this, age.HumanAge);

            return age;
        }

        public Age getRandomAdultAge(int level = 1) {

            Age age = new Age();

            int r = RandomSeed.rnd.Next(0, 71) + level;

            if (r <= 7) {
                age.HumanAge = RandomSeed.rnd.Next(15, 18);
            }
            else if (r <= 21) {
                age.HumanAge = RandomSeed.rnd.Next(18, 26);
            }
            else if (r <= 29) {
                age.HumanAge = RandomSeed.rnd.Next(26, 31);
            }
            else if (r <= 43) {
                age.HumanAge = RandomSeed.rnd.Next(31, 41);
            }
            else if (r <= 55) {
                age.HumanAge = RandomSeed.rnd.Next(41, 51);
            }
            else if (r <= 64) {
                age.HumanAge = RandomSeed.rnd.Next(51, 61);
            }
            else if (r <= 69) {
                age.HumanAge = RandomSeed.rnd.Next(61, 71);
            }
            else if (RandomSeed.rnd.NextDouble() >= .01) {
                age.HumanAge = RandomSeed.rnd.Next(71, 81);
            }
            else {
                age.HumanAge = RandomSeed.rnd.Next(81, 101);
            }

            age.RacialAge = getRaceEquivalentAge(this, age.HumanAge);

            return age;
        }

        public string getRandomName(NPCEnums.Sex sex) {

            if (this == HUMAN || (this == HALF_ELF && RandomSeed.rnd.NextDouble() < .5) || (this == HALF_ORC && RandomSeed.rnd.NextDouble() < .5)) {
                return HumanNames.getRandomName(sex);
            } else if (this == GNOME) {
                return GnomeNames.getRandomName(sex);
            } else if (this == ELF) {
                return ElfNames.getRandomName(sex);
            } else if (this == DWARF || this == HALF_ORC) {
                return DwarfNames.getRandomName(sex);
            } else if (this == HALFLING) {
                return HalflingNames.getRandomName(sex);
            } else {
                return HumanNames.getRandomName(sex);
            }
        }

        public static Race getRandomRace() {
            return raceDistribution.getRandomObject();
        }

        public override string ToString() {
            return name;
        }
    }
}
