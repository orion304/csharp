﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorldGenerator.NPC {
    public class StatArchetype {

        public static readonly StatArchetype MELEE = new StatArchetype("Melee", new Stat[]{ Stat.STR, Stat.CON}, new Stat[] { Stat.DEX}, new Stat[] { Stat.INT, Stat.WIS, Stat.CHA});
        public static readonly StatArchetype RANGED = new StatArchetype("Ranged", new Stat[] { Stat.DEX }, new Stat[] { }, new Stat[] { Stat.CON, Stat.STR, Stat.INT, Stat.WIS, Stat.CHA });
        public static readonly StatArchetype ARCANE = new StatArchetype("Arcane", new Stat[] { Stat.INT}, new Stat[] { }, new Stat[] { Stat.CON, Stat.STR, Stat.DEX, Stat.WIS, Stat.CHA });
        public static readonly StatArchetype ARCANE_CHA = new StatArchetype("Arcane", new Stat[] { Stat.CHA }, new Stat[] { }, new Stat[] { Stat.CON, Stat.STR, Stat.DEX, Stat.WIS, Stat.INT });
        public static readonly StatArchetype DIVINE = new StatArchetype("Divine", new Stat[] { Stat.WIS}, new Stat[] { }, new Stat[] { Stat.CON, Stat.STR, Stat.DEX, Stat.INT, Stat.CHA });
        public static readonly StatArchetype SKILL = new StatArchetype("Skill", new Stat[] { Stat.INT, Stat.STR, Stat.DEX}, new Stat[] { }, new Stat[] { Stat.CON, Stat.WIS, Stat.CHA});

        public readonly string name;
        private Stat[] greatStats, goodStats, dumpStats;

        private StatArchetype(string name, Stat[] greatStats, Stat[] goodStats, Stat[] dumpStats) {
            this.name = name;
            this.greatStats = greatStats;
            this.goodStats = goodStats;
            this.dumpStats = dumpStats;
        }

        public List<Stat> getStatOrder() {
            List<Stat> order = new List<Stat>();

            order.AddRange(greatStats.OrderBy(x => RandomSeed.rnd.Next()).ToArray());
            order.AddRange(goodStats.OrderBy(x => RandomSeed.rnd.Next()).ToArray());
            order.AddRange(dumpStats.OrderBy(x => RandomSeed.rnd.Next()).ToArray());

            return order;
        }

        public override string ToString() {
            return name;
        }
    }
}
