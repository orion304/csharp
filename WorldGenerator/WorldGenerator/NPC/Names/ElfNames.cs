﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorldGenerator.NPC.Names {
    public class ElfNames {

        private static readonly string[] femaleEndings = { "", "a", "e", "i" };

        private static readonly string[] table1 = { "Ael", "Aer", "Af", "Ah", "Al", "Am", "Ama", "An", "Ang", "Ansr", "Ar", "Ari", "Arn", "Aza", "Bael", "Bes", "Cael", "Cal", "Cas", "Cla", "Cor", "Cy", "Dae", "Dho", "Dre", "Du", "Eli", "Eir", "Eli", "Er", "Ev", "Fera", "Fi", "Fir", "Fis", "Gael", "Gar", "Gil", "Ha", "Hu", "Ia", "Il", "Ja", "Jar", "Ka", "Kan", "Ker", "Keth", "Koeh", "Kor", "Ky", "La", "Laf", "Lam", "Lue", "Ly", "Mai", "Mal", "Mara", "My", "Na", "Nai", "Nim", "Nu", "Ny", "Py", "Raer", "Re", "Ren", "Rid", "Ru", "Rua", "Rum", "Ry", "Sae", "She", "Sel", "Sha", "She", "Si", "Sim", "Sol", "Sum", "Syl", "Ta", "Tahl", "Tha", "Tho", "Ther", "Thro", "Tia", "Tra", "Ty", "Uth", "Ver", "Vil", "Von", "Ya", "Za", "Zy" };
        private static readonly string[] table2 = { "ae", "ael", "aer", "aias", "ah", "aith", "al", "aias", "am", "an", "ar", "ari", "aro", "as", "ath", "avel", "brar", "dar", "deth", "dre", "drim", "dul", "ean", "el", "emar", "en", "er", "ess", "evar", "fel", "hal", "har", "hel", "ian", "iat", "ik", "il", "im", "in", "ir", "is", "ith", "kash", "ki", "lan", "lam", "lar", "las", "lian", "lis", "lon", "lyn", "mah", "mil", "mus", "nal", "nes", "nin", "nis", "on", "or", "oth", "que", "quis", "rah", "rad", "rail", "ran", "reth", "ro", "ruil", "sal", "san", "sar", "sel", "sha", "spar", "tae", "tas", "ten", "thal", "thar", "ther", "this", "thus", "ti", "tril", "ual", "uath", "us", "van", "var", "vain", "via", "vin", "wyn", "ya", "yr", "yth", "zair" };

        public static string getRandomName(NPCEnums.Sex sex) {
            int a = Dice.d100();

            string name;
            if (a <= 40) {
                name = generateRandomNameMethod1(sex);
            } else if (a <= 70) {
                name = generateRandomNameMethod2(sex);
            } else if (a <= 90) {
                name = generateRandomNameMethod3(sex);
            } else {
                name = generateRandomNameMethod4(sex);
            }

            if (sex == NPCEnums.Sex.Female && !GnomeNames.vowels.Contains(name.Last())) {
                name += RandomSeed.randomChoice(femaleEndings);
            }

            return name;
        }

        private static string generateRandomNameMethod1(NPCEnums.Sex sex) {
            string name1 = RandomSeed.randomChoice(table1);
            string name2 = RandomSeed.randomChoice(table2);

            return name1 + name2;
        }

        private static string generateRandomNameMethod2(NPCEnums.Sex sex) {
            string name1 = RandomSeed.randomChoice(table1);
            string name2 = RandomSeed.randomChoice(table2);
            string name3 = RandomSeed.randomChoice(table2);

            return name1 + name2 + name3;
        }

        private static string generateRandomNameMethod3(NPCEnums.Sex sex) {
            string name1 = RandomSeed.randomChoice(table1);
            string name2 = RandomSeed.randomChoice(table2);
            string name3 = RandomSeed.randomChoice(table1);
            string name4 = RandomSeed.randomChoice(table2);

            return name1 + name2 + " " + name3 + name4;
        }

        private static string generateRandomNameMethod4(NPCEnums.Sex sex) {
            string name1 = RandomSeed.randomChoice(table2);
            string name2 = RandomSeed.randomChoice(table1);
            string name3 = RandomSeed.randomChoice(table2);
            string name4 = RandomSeed.randomChoice(table2);

            return name1.Substring(0, 1).ToUpper() + name1.Substring(1) + "'" + name2 + name3 + name4;
        }
    }
}
