﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorldGenerator.NPC.Names {
    public class DwarfNames {

        private static readonly string[] table1 = { "A", "A", "An", "An", "Ar", "Ar", "Az", "Az", "B", "B", "Bar", "Bar", "Baz", "Baz", "Bel", "Bel", "Bof", "Bof", "Bol", "Bol", "D", "D", "Dar", "Dar", "Del", "Del", "Dol", "Dol", "Dor", "Dor", "Duer", "Duer", "Dur", "Dur", "Dw", "Dw", "El", "El", "Er", "Er", "Fal", "Fal", "Far", "Far", "Gar", "Gar", "Gil", "Gil", "Gim", "Gim", "Glan", "Glan", "Glor", "Glor", "Har", "Har", "Hel", "Hel", "Jar", "Jar", "Kil", "Kil", "Ma", "Ma", "Mor", "Mor", "Nal", "Nal", "Nor", "Nor", "Nur", "Nur", "O", "O", "Or", "Or", "Ov", "Ov", "Rei", "Rei", "Th", "Th", "Tho", "Tho", "Thr", "Thr", "Tor", "Tor", "Ur", "Ur", "Val", "Val", "Von", "Von", "Wer", "Wer", "Whur", "Whur", "Yur", "Yur" };
        private static readonly string[] table2 = { "aim", "aim", "ain", "ain", "ak", "ak", "ar", "ar", "auk", "auk", "bere", "bere", "bir", "bir", "dak", "dak", "dal", "dal", "din", "din", "el", "el", "ent", "ent", "erl", "erl", "gal", "gal", "gan", "gan", "gar", "gar", "gen", "gen", "grim", "grim", "gur", "gur", "i", "i", "ias", "ias", "ili", "ili", "im", "im", "in", "in", "ir", "ir", "kas", "kas", "kral", "kral", "lond", "lond", "o", "o", "on", "on", "or", "or", "oril", "oril", "rak", "rak", "ral", "ral", "ric", "ric", "rid", "rid", "rim", "rim", "ring", "ring", "ster", "ster", "Sun", "Sun", "ten", "ten", "thal", "thal", "then", "then", "thic", "thic", "thur", "thur", "ur", "ur", "urt", "urt", "ut", "ut", "val", "val", "var", "var" };
        private static readonly string[] table3 = { "a", "a", "ala", "ala", "alsia", "alsia", "ana", "ana", "ani", "ani", "astr", "astr", "bela", "bela", "bera", "bera", "bo", "bo", "bryn", "bryn", "deth", "deth", "dis", "dis", "dred", "dred", "drid", "drid", "dris", "dris", "esli", "esli", "gret", "gret", "gunn", "gunn", "hild", "hild", "ia", "ia", "ida", "ida", "iess", "iess", "iff", "iff", "ifra", "ifra", "ila", "ila", "ild", "ild", "ina", "ina", "ip", "ip", "isi", "isi", "iz", "iz", "ja", "ja", "kara", "kara", "li", "li", "lin", "lin", "lydd", "lydd", "mora", "mora", "ola", "ola", "on", "on", "ora", "ora", "re", "re", "ren", "ren", "serd", "serd", "shar", "shar", "thra", "thra", "tia", "tia", "tryd", "tryd", "unn", "unn", "wynn", "wynn", "ya", "ya", "ydd", "ydd" };

        public static string getRandomName(NPCEnums.Sex sex) {
            string name = RandomSeed.randomChoice(table1);

            if (sex == NPCEnums.Sex.Male) {
                name += RandomSeed.randomChoice(table2);
            } else {
                name += RandomSeed.randomChoice(table3);
            }

            return name;
        }

    }
}
