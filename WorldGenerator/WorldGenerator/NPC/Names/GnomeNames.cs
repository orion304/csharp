﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorldGenerator.NPC.Names {
    public class GnomeNames {

        public static readonly char[] vowels = {'a', 'e', 'i', 'o', 'u' };

        private static readonly string[] elEndings = { "l", "el" };
        private static readonly string[] aEndings = { "al", "a", "anna" };
        private static readonly string[] otherEndings = {"l", "anna" };
        private static readonly string[] ae = { "a", "e" };
        private static readonly string[] aae = { "a", "ae" };

        private static readonly string[] table1 = { "Add", "Add", "Arum", "Arum", "Arum", "Baer", "Baer", "Baer", "Bar", "Bar", "Bar", "Callad", "Callad", "Callad", "Chik", "Chik", "Dal", "Dal", "Dal", "Din", "Din", "Din", "Eaus", "Eaus", "Eaus", "Enn", "Enn", "Erf", "Erf", "Faer", "Faer", "Fen", "Fen", "Fen", "Flan", "Flan", "Gaer", "Gaer", "Gaer", "Gar", "Gar", "Gar", "Hed", "Hed", "Herl", "Herl", "Ien", "Ien", "Jan", "Jan", "Jan", "Kaer", "Kaer", "Len", "Len", "Len", "Lun", "Lun", "Lun", "Mikk", "Mikk", "Neb", "Neb", "Neb", "Oaen", "Oaen", "Ow", "Ow", "Pall", "Pall", "Pin", "Pin", "Raer", "Raer", "Ras", "Ras", "Seg", "Seg", "Skor", "Skor", "Skor", "Tikk", "Tikk", "Tikk", "Uran", "Uran", "Uran", "Urd", "Urd", "Urd", "Van", "Van", "Van", "Var", "Var", "Var", "Wann", "Wann", "Wed", "Wed" };
        private static readonly string[] table2 = { "Ale", "Ale", "Ash", "Ash", "Badger", "Badger", "Badger", "Bones", "Bones", "Caller", "Caller", "Cloak", "Cloak", "Cloak", "Drinker", "Drinker", "Earth", "Earth", "Earth", "Eye", "Eye", "Fast", "Fast", "Fast", "Foot", "Foot", "Fox", "Fox", "Gem", "Gem", "Gem", "Glitter", "Glitter", "Glitter", "Gold", "Gold", "Gold", "Hand", "Hand", "Heart", "Heart", "Heart", "Hill", "Hill", "Honor", "Honor", "Iron", "Iron", "Laughing", "Laughing", "Lightning", "Lightning", "Lightning", "Little", "Little", "Lock", "Lock", "Man/Lady", "Man/Lady", "Moon", "Moon", "Phantom", "Phantom", "Phantom", "Shadow", "Shadow", "Shadow", "Silver", "Silver", "Silver", "Skin", "Skin", "Slosh", "Slosh", "Sly", "Sly", "Sparkle", "Sparkle", "Steel", "Steel", "Stone", "Stone", "Strider", "Strider", "Strider", "Stumble", "Stumble", "Sun", "Sun", "Swift", "Swift", "Swift", "Wanderer", "Wanderer", "Wanderer", "Whisper", "Whisper", "Whisper", "Wild", "Wild" };

        public static string getRandomName(NPCEnums.Sex sex) {

            int a = Dice.d10();

            if (a <= 4) {
                return getRandomNameMethod1(sex);
            }
            else if (a <= 7) {
                return getRandomNameMethod2(sex);
            }
            else if (a <= 9) {
                return getRandomNameMethod3(sex);
            }
            else {
                return getRandomNameMethod4(sex);
            }

        }

        private static string getRandomNameMethod1(NPCEnums.Sex sex) {
            string name = RandomSeed.randomChoice(table1);

            if (sex == NPCEnums.Sex.Male && !vowels.Contains(name.Last())) {
                name += "el";
            }
            else if (sex == NPCEnums.Sex.Male) {
                name += RandomSeed.randomChoice(elEndings);
            }
            else if (sex == NPCEnums.Sex.Female && name.Last() != 'a') {
                name += RandomSeed.randomChoice(aEndings);
            }
            else {
                name += RandomSeed.randomChoice(otherEndings);
            }

            return name;
        }

        private static string getRandomNameMethod2(NPCEnums.Sex sex) {
            string name;

            string name1 = RandomSeed.randomChoice(table1);
            string name2 = RandomSeed.randomChoice(table1).ToLower();

            if (!vowels.Contains(name1.Last()) && !vowels.Contains(name2.First())) {
                name = name1 + RandomSeed.randomChoice(ae) + name2;
            } else {
                name = name1 + name2;
            }

            return name;
        }

        private static string getRandomNameMethod3(NPCEnums.Sex sex) {
            string name;

            string name1 = RandomSeed.randomChoice(table1);
            string name2 = RandomSeed.randomChoice(table1).ToLower();

            string nick = " " + RandomSeed.randomChoice(table2);
            string replace = "Man";
            if (sex == NPCEnums.Sex.Female) {
                replace = "Lady";
            }

            nick = nick.Replace("Man/Lady", replace);

            if (!vowels.Contains(name1.Last()) && !vowels.Contains(name2.First())) {
                name = name1 + RandomSeed.randomChoice(ae) + name2;
            } else {
                name = name1 + name2;
            }

            if (sex == NPCEnums.Sex.Female) {
                name = femaleifyName(name);
            }

            return name + nick;
        }
        private static string getRandomNameMethod4(NPCEnums.Sex sex) {
            string name;

            string name1 = RandomSeed.randomChoice(table1);
            string name2 = RandomSeed.randomChoice(table1).ToLower();
            string name3 = RandomSeed.randomChoice(table1).ToLower();

            if (!vowels.Contains(name1.Last()) && !vowels.Contains(name2.First())) {
                name = name1 + RandomSeed.randomChoice(ae) + name2;
            } else {
                name = name1 + name2;
            }

            if (!vowels.Contains(name.Last()) && !vowels.Contains(name3.First())) {
                name = name2 + RandomSeed.randomChoice(ae) + name3;
            } else {
                name = name + name3;
            }

            if (sex == NPCEnums.Sex.Female) {
                name = femaleifyName(name);
            }

            return name;
        }

        private static string femaleifyName(string name) {
            if (name.Last() == 'a') {
                name += "e";
            } else {
                name += RandomSeed.randomChoice(aae);
            }
            return name;
        }

    }
}
