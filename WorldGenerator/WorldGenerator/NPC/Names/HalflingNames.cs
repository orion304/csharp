﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorldGenerator.NPC.Names {
    public class HalflingNames {

        private static readonly string[] joinVowels = { "o", "ee" };

        private static readonly string[] table1 = { "Arv", "Arv", "Arv", "Baris", "Baris", "Brand", "Brand", "Brand", "Bren", "Bren", "Cal", "Cal", "Cal", "Chen", "Chen", "Cyrr", "Cyrr", "Cyrr", "Dair", "Dair", "Dal", "Dal", "Dal", "Deree", "Deree", "Dric", "Dric", "Dric", "Eere", "Eere", "Essel", "Essel", "Essel", "Fur", "Fur", "Galan", "Galan", "Galan", "Gen", "Gen", "Gren", "Gren", "Gren", "Ien", "Ien", "Illi", "Illi", "Illi", "Indy", "Indy", "Iss", "Iss", "Iss", "kal", "kal", "kal", "Kep", "Kep", "Kin", "Kin", "Kin", "Li", "Li", "Llalee", "Llalee", "Llalee", "Lur", "Lur", "Mel", "Mel", "Mel", "Opee", "Opee", "Ped", "Ped", "Ped", "Pery", "Pery", "Pery", "Penel", "Penel", "Penel", "Reen", "Reen", "Reen", "Rill", "Rill", "Rill", "Royl", "Royl", "Sheel", "Sheel", "Thea", "Thea", "Thea", "Ur", "Ur", "Wort", "Wort", "Yon" };
        private static readonly string[] table2 = { "Bones", "Bones", "Caller", "Caller", "Caller", "Cloak", "Cloak", "Cloak", "Earth", "Earth", "Eye", "Eye", "Fast", "Fast", "Fast", "Foot", "Foot", "Foot", "Glen", "Glen", "Glitter", "Glitter", "Gold", "Gold", "Hand", "Hand", "Heart", "Heart", "Heart", "Hill", "Hill", "Hill", "Hollow", "Hollow", "Honor", "Honor", "Honor", "Laughing", "Laughing", "Laughing", "Leaf", "Leaf", "Leaf", "Lightning", "Lightning", "Man/Lady", "Man/Lady", "Meadow", "Meadow", "Meadow", "Moon", "Moon", "Moon", "Nimble", "Nimble", "Nimble", "Quick", "Quick", "Reed", "Reed", "Shadow", "Shadow", "Shadow", "Silver", "Silver", "Silver", "Skin", "Skin", "Sly", "Sly", "Sly", "Small", "Small", "Small", "Smooth", "Smooth", "Stout", "Stout", "Stout", "Strider", "Strider", "Strider", "Sun", "Sun", "Sun", "Swift", "Swift", "Thistle", "Thistle", "Wanderer", "Wanderer", "Warm", "Warm", "Wild", "Wild", "Wild", "Will", "Will", "Whisper", "Whisper" };

        public static string getRandomName(NPCEnums.Sex sex) {
            int a = Dice.d20();

            if (a <= 3) {
                return getRandomNameMethod1(sex);
            } else if (a <= 9) {
                return getRandomNameMethod2(sex);
            } else if (a <= 13) {
                return getRandomNameMethod3(sex);
            } else if (a <= 17) {
                return getRandomNameMethod4(sex);
            } else {
                return getRandomNameMethod5(sex);
            }
        }

        private static string getRandomNameMethod1(NPCEnums.Sex sex) {
            string name = RandomSeed.randomChoice(table1);

            if (sex == NPCEnums.Sex.Female && !GnomeNames.vowels.Contains(name.Last())) {
                name += name.Last() + "a";
            }

            return name;
        }

        private static string getRandomNameMethod2(NPCEnums.Sex sex) {
            string name1 = RandomSeed.randomChoice(table1);
            string name2 = RandomSeed.randomChoice(table1).ToLower();

            if (sex == NPCEnums.Sex.Female && !GnomeNames.vowels.Contains(name2.Last())) {
                name2 += name2.Last() + "a";
            }

            if (!GnomeNames.vowels.Contains(name1.Last()) && !GnomeNames.vowels.Contains(name2.First())) {
                return name1 + RandomSeed.randomChoice(joinVowels) + name2;
            } else {
                return name1 + name2;
            }
        }

        private static string getRandomNameMethod3(NPCEnums.Sex sex) {
            string name1 = RandomSeed.randomChoice(table1);
            string name2 = RandomSeed.randomChoice(table1);
            string name3 = RandomSeed.randomChoice(table1).ToLower();

            if (sex == NPCEnums.Sex.Female && !GnomeNames.vowels.Contains(name1.Last())) {
                name1 += name1.Last() + "a";
            }

            if (!GnomeNames.vowels.Contains(name2.Last()) && !GnomeNames.vowels.Contains(name3.First())) {
                return name1 + " " + name2 + RandomSeed.randomChoice(joinVowels) + name3;
            } else {
                return name1 + " " + name2 + name3;
            }
        }

        private static string getRandomNameMethod4(NPCEnums.Sex sex) {
            string name1 = RandomSeed.randomChoice(table1);
            string name2 = RandomSeed.randomChoice(table1).ToLower();
            string name3 = RandomSeed.randomChoice(table1);
            string name4 = RandomSeed.randomChoice(table1).ToLower();

            if (sex == NPCEnums.Sex.Female && !GnomeNames.vowels.Contains(name2.Last())) {
                name2 += name2.Last() + "a";
            }

            string firstname, lastname;

            if (!GnomeNames.vowels.Contains(name1.Last()) && !GnomeNames.vowels.Contains(name2.First())) {
                firstname = name1 + RandomSeed.randomChoice(joinVowels) + name2;
            } else {
                firstname = name1 + name2;
            }

            if (!GnomeNames.vowels.Contains(name3.Last()) && !GnomeNames.vowels.Contains(name4.First())) {
                lastname = name3 + RandomSeed.randomChoice(joinVowels) + name4;
            } else {
                lastname = name3 + name4;
            }

            return firstname + " " + lastname;
        }

        private static string getRandomNameMethod5(NPCEnums.Sex sex) {
            string name1 = RandomSeed.randomChoice(table1);
            string name2 = RandomSeed.randomChoice(table1).ToLower();
            string name3 = RandomSeed.randomChoice(table2);
            string name4 = RandomSeed.randomChoice(table2).ToLower();

            string replace = "Man";
            if (sex == NPCEnums.Sex.Female) {
                replace = "Lady";
            }

            name3 = name3.Replace("Man/Lady", replace);
            name4 = name4.Replace("man/lady", replace.ToLower());

            if (sex == NPCEnums.Sex.Female && !GnomeNames.vowels.Contains(name2.Last())) {
                name2 += name2.Last() + "a";
            }

            string firstname;

            if (!GnomeNames.vowels.Contains(name1.Last()) && !GnomeNames.vowels.Contains(name2.First())) {
                firstname = name1 + RandomSeed.randomChoice(joinVowels) + name2;
            } else {
                firstname = name1 + name2;
            }

            if (name3.ToLower() == name4.ToLower()) {
                return firstname;
            } else {
                return firstname + " " + name3 + name4;
            }
            
        }
    }
}
