﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorldGenerator.Cities;

namespace WorldGenerator.NPC {
    public class NPC {

        public Race race;
        public string Race {
            get {
                return race.ToString();
            }
        }
        public NPCClass npcClass;
        public string NPCClass {
            get {
                return npcClass.ToString();
            }
        }
        public NPCEnums.Sex Sex { get; set; }
        public int Level { get; set; }
        public string Name { get; set; }
        public string Trait { get; set; }
        public Alignment alignment;
        public string Alignment {
            get {
                return alignment.ToString();
            }
        }
        public string Quirk { get; set; }
        public int Height {
            set {
                //Height = value;
                int feet = value / 12;
                HeightStr = String.Format("{0}' {1}\"", feet, value - feet * 12);
            }
        }
        public string HeightStr { get; set; }
        public int Weight { get; set; }
        public int HumanAge { get; set; }
        public int RacialAge { get; set; }
        public string Profession { get; set; }
        public Building LivesAt;
        public string Lives {
            get {
                return LivesAt.ToString();
            }
        }
        public Building WorksAt;
        public string Works {
            get {
                return WorksAt.ToString();
            }
        }

        public StatArchetype statArchetype;

        public string Archetype {
            get {
                return statArchetype.ToString();
            }
        }

        public int HP { get; set; }

        public Dictionary<Stat, int> stats = new Dictionary<Stat, int>();
        public int STR {
            get {
                return stats[Stat.CHA];
            }
        }
        public int DEX {
            get {
                return stats[Stat.DEX];
            }
        }
        public int CON {
            get {
                return stats[Stat.CON];
            }
        }
        public int INT {
            get {
                return stats[Stat.INT];
            }
        }
        public int WIS {
            get {
                return stats[Stat.WIS];
            }
        }
        public int CHA {
            get {
                return stats[Stat.CHA];
            }
        }

        public int FORT {
            get {
                return getSave(Stat.CON, npcClass.isFortGood);
            }
        }

        public int REF {
            get {
                return getSave(Stat.DEX, npcClass.isRefGood);
            }
        }

        public int WILL {
            get {
                return getSave(Stat.WIS, npcClass.isWillGood);
            }
        }

        public int BAB {
            get {
                return npcClass.babType.getBAB(Level);
            }
        }

        public NPC(int level, NPCClass classType) {
            race = WorldGenerator.NPC.Race.getRandomRace();
            npcClass = classType;
            Sex = RandomSeed.randomEnumValue<NPCEnums.Sex>();
            Level = level;
            Name = race.getRandomName(Sex);
            Trait = WorldGenerator.NPC.Trait.getRandomTrait();
            Quirk = WorldGenerator.NPC.Quirk.getRandomQuirk();
            alignment = WorldGenerator.NPC.Alignment.getRandomAlignment();

            Race.Age age;
            if (classType == WorldGenerator.NPC.NPCClass.Child) {
                age = race.getRandomChildAge();
            } else {
                age = race.getRandomAdultAge(Level);
            }

            Race.HeightAndWeight heightAndWeight = race.getRandomHeightAndWeight(Sex, age);


            Height = heightAndWeight.Height;
            Weight = heightAndWeight.Weight;
            HumanAge = age.HumanAge;
            RacialAge = age.RacialAge;

            generateStats();
        }

        private void generateStats() {
            generateAbilityScores();

            HP = npcClass.hitDie + getAbilityMod(Stat.CON) * Level;

            for (int i = 1; i < Level; i++ ) {
                HP += RandomSeed.rnd.Next(npcClass.hitDie) + 1;
            }
        }

        public override string ToString() {
            return String.Format("Level {0} {1} {2} - {3} ({4}) - {5}, {6}, {7}, {8}, {9}, {10}, {11} ({12}) yo, lives at {13}, works at {14}", Level, race.name, npcClass.className, Name, Sex, alignment.shortName, Profession, Trait, Quirk, HeightStr, Weight, RacialAge, HumanAge, LivesAt.Name, WorksAt.Name);
        }

        private int getRandomAbilityScore() {
            return Dice.d6() + Dice.d6() + Dice.d6();
        }

        private void generateAbilityScores() {
            List<int> scores = new List<int>();

            for (int i = 0; i < 6; i++) {
                scores.Add(getRandomAbilityScore());
            }

            scores.Sort();

            statArchetype = npcClass.getRandomArchetype();
            List<Stat> statOrder = statArchetype.getStatOrder();

            Stat stat;
            int score;
            int highestIndex = scores.Count - 1;
            for (int i = 0; i < scores.Count; i++) {
                stat = statOrder[highestIndex - i];
                score = scores[i];

                if (i == highestIndex && (race == WorldGenerator.NPC.Race.HALF_ELF || race == WorldGenerator.NPC.Race.HALF_ORC || race == WorldGenerator.NPC.Race.HUMAN)) {
                    score += 2;
                }

                stats[stat] = score;
            }

            applyRacialAdjustments();

        }

        private void applyRacialAdjustments() {

            if (race == WorldGenerator.NPC.Race.DWARF) {
                stats[Stat.CON] += 2;
                stats[Stat.WIS] += 2;
                stats[Stat.CHA] -= 2;
            } else if (race == WorldGenerator.NPC.Race.ELF) {
                stats[Stat.DEX] += 2;
                stats[Stat.INT] += 2;
                stats[Stat.CON] -= 2;
            } else if (race == WorldGenerator.NPC.Race.GNOME) {
                stats[Stat.CON] += 2;
                stats[Stat.CHA] += 2;
                stats[Stat.STR] -= 2;
            } else if (race == WorldGenerator.NPC.Race.HALFLING) {
                stats[Stat.DEX] += 2;
                stats[Stat.CHA] += 2;
                stats[Stat.STR] -= 2;
            }

        }

        private int getAbilityMod(Stat stat) {
            double value = stats[stat];
            return (int)Math.Floor((value - 10) / 2);
        }

        private int getSave(Stat stat, bool isGood) {

            int mod = getAbilityMod(stat);
            int saveBase;

            if (isGood) {
                saveBase = Level / 2 + 2;
            } else {
                saveBase = Level / 3;
            }

            return saveBase + mod;
        }

    }
}
