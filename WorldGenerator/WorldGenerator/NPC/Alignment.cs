﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorldGenerator.Distributions;

namespace WorldGenerator.NPC {
    public class Alignment : IComparable<Alignment> {

        public static Alignment currentWeight = null;
        private static PercentageDistribution<Alignment> distribution = null;

        public static readonly Alignment LAWFUL_GOOD = new Alignment("Lawful Good", 0.35);
        public static readonly Alignment NEUTRAL_GOOD = new Alignment("Neutral Good", 0.04);
        public static readonly Alignment CHAOTIC_GOOD = new Alignment("Chaotic Good", 0.02);
        public static readonly Alignment LAWFUL_NEUTRAL = new Alignment("Lawful Neutral", 0.20);
        public static readonly Alignment NEUTRAL_NEUTRAL = new Alignment("Neutral Neutral", 0.02);
        public static readonly Alignment CHAOTIC_NEUTRAL = new Alignment("Chaotic Neutral", 0.01);
        public static readonly Alignment LAWFUL_EVIL = new Alignment("Lawful Evil", 0.26);
        public static readonly Alignment NEUTRAL_EVIL = new Alignment("Neutral Evil", 0.08);
        public static readonly Alignment CHAOTIC_EVIL = new Alignment("Chaotic Evil", 0.02);
       
        public readonly string name;
        public readonly string shortName;
        public readonly double baseIncidence;
        public double currentIncidence;
        private Alignment(string name, double baseIncidence) {
            this.name = name;
            this.baseIncidence = baseIncidence;
            this.currentIncidence = baseIncidence;

            string[] splits = name.Split(' ');
            shortName = splits[0].Substring(0, 1) + splits[1].Substring(0, 1);
        }

        static Alignment() {
            distribution = new PercentageDistribution<Alignment>();
            initializeDistribution();
        }

        private static void initializeDistribution() {
            distribution.clear();
            foreach (Alignment alignment in Values) {
                distribution.setPercentage(alignment.currentIncidence, alignment);
            }
        }

        public static IEnumerable<Alignment> Values {
            get {
                yield return LAWFUL_GOOD;
                yield return NEUTRAL_GOOD;
                yield return CHAOTIC_GOOD;
                yield return LAWFUL_NEUTRAL;
                yield return NEUTRAL_NEUTRAL;
                yield return CHAOTIC_NEUTRAL;
                yield return LAWFUL_EVIL;
                yield return NEUTRAL_EVIL;
                yield return CHAOTIC_EVIL;
            }
        }

        public static Alignment getRandomAlignment() {
            return distribution.getRandomObject();
        }

        public static void changeWeight(Alignment newWeight) {
            currentWeight = newWeight;

            if (newWeight == null) {
                foreach (Alignment alignment in Values) {
                    alignment.currentIncidence = alignment.baseIncidence;
                }
                return;
            }

            double chosenProb;
            double cumProbs = 0;

            foreach (Alignment alignment in Values) {
                if (alignment.Equals(newWeight)) {
                    chosenProb = alignment.baseIncidence;
                }
                else {
                    cumProbs += alignment.baseIncidence;
                }
            }

            double mult = 0.3 / cumProbs;

            foreach (Alignment alignment in Values) {
                if (alignment.Equals(newWeight)) {
                    alignment.currentIncidence = 0.7;
                }
                else {
                    alignment.currentIncidence = alignment.baseIncidence * mult;
                }
            }

            initializeDistribution();

        }

        public override string ToString() {
            return name;
        }

        public int CompareTo(Alignment other) {
            return name.CompareTo(other);
        }
    }
}
