﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;
using WorldGenerator.Tools;

namespace WorldGenerator.Cities {
	public class Ward {

		public static int counter = 0;
		public static int counterToMark = -1;
		private static SolidColorBrush buildingBrushColor = MainWindow.redBrush;
        private static double useableAreaFactor = 0.8;
        private static double buildingAreaLowerBound = 0.6;

        private double minAlleySeparation = 60;
        private double maxAlleySeparation = 100;
        private double minBuildingWidth = 30;
        private double maxBuildingWidth = 60;
        private double minBuildingHeight = 30;
        private double maxBuildingHeight = 60;

        private double halfMinDistanceBetweenBuildings = 2.5;

        private double scaleFactor = 1;

        public readonly double area;
		public Point center;
		public Point scaledCenter;
		public Polygon wardPolygon { get; set; }
        public Polygon scaledPolygon { get; set; }
        public City ParentCity { get; set; }
        public string Name { get; set; }
        public readonly List<Building> buildings = new List<Building>();
		public readonly List<Alley> alleys = new List<Alley>();
		public readonly WardType wardType;

		private readonly int WardSeed;
		private readonly int AlleySeed;
		private readonly int BuildingSeed;

		private ExtendedPolygon extendedPolygon;
		private List<ExtendedPolygon> blocks = new List<ExtendedPolygon>();


		public Ward(Point center, Polygon wardPolygon, TownType townType, WardType wardType, City city) {
            ParentCity = city;
			WardSeed = RandomSeed.rnd.Next();
			AlleySeed = RandomSeed.rnd.Next();
			BuildingSeed = RandomSeed.rnd.Next();

			RandomSeed.rnd = new Random(WardSeed);

			this.center = center;
			scaledCenter = new Point(center.X, center.Y);
			this.wardPolygon = wardPolygon;
            area = CommonFunctions.Area(wardPolygon);
			this.wardType = wardType;

            PointCollection points = new PointCollection(wardPolygon.Points);
            scaledPolygon = new Polygon();
            scaledPolygon.Points = points;

            scaledPolygon.Stroke = MainWindow.blackBrush;
			scaledPolygon.Fill = MainWindow.transparentBrush;
			scaledPolygon.ToolTip = String.Format("{0}", wardType.name);
            MainWindow.WindowBox.Children.Add(scaledPolygon);

			extendedPolygon = new ExtendedPolygon(wardPolygon);
			blocks.Add(extendedPolygon);

            int numberOfBuildings = (int)(wardType.buildingDensityPerAcre * area / CommonFunctions.sqftInAcre);
            double assumedUsableArea = useableAreaFactor * area;
            double maxAverageAreaPerBuilding = assumedUsableArea / numberOfBuildings;

            

			createAlleys();

            int numberOfBuildingGenerationAttempts = 0;
            int maxNumberOfBuildingGenerationAttempts = 10;

            while (buildings.Count < numberOfBuildings && numberOfBuildings != 0) {

                numberOfBuildingGenerationAttempts++;

                maxBuildingWidth = Math.Sqrt(maxAverageAreaPerBuilding);
                minBuildingWidth = maxBuildingWidth * buildingAreaLowerBound;

                maxBuildingHeight = maxBuildingWidth;
                minBuildingHeight = minBuildingWidth;

                createBuildings();

                if (buildings.Count < numberOfBuildings) {

                    if (numberOfBuildingGenerationAttempts > maxNumberOfBuildingGenerationAttempts) {
                        if (System.Diagnostics.Debugger.IsAttached)
                            System.Diagnostics.Debugger.Break();
                        CommonFunctions.output("Building generator didn't create enough buildings");
                    }

                    maxAverageAreaPerBuilding *= useableAreaFactor;
                    buildings.Clear();
                }
                
			}

            while (buildings.Count > numberOfBuildings) {
                buildings.RemoveAt(RandomSeed.rnd.Next(buildings.Count));
            }

            foreach (Building building in buildings) {
				building.draw(MainWindow.greenBrush);
			}

			//blocks[blocks.Count - 1].draw();
			//foreach (ExtendedPolygon block in blocks) {
			//	block.draw();
			//}


			//blocks[0].draw();

			//drawNthEdge(wardPolygon.Points.Count - 2);
		}

        public void updateTooltip() {
            scaledPolygon.ToolTip = String.Format("{0} ({1})", Name, wardType.name);
        }

		private void drawNthEdge(int n) {
			Point fromPoint = wardPolygon.Points[n];
			Point toPoint = wardPolygon.Points[n+1];

			Line line = new Line();
			line.X1 = fromPoint.X;
			line.X2 = toPoint.X;
			line.Y1 = fromPoint.Y;
			line.Y2 = toPoint.Y;

			line.Stroke = MainWindow.yellowBrush;
			line.StrokeThickness = 2;

			MainWindow.WindowBox.Children.Add(line);
		}

        public void scale(double scale) {
            double factor = scale / scaleFactor;
            scaleFactor = scale;

			scaledCenter.X *= factor;
			scaledCenter.Y *= factor;

			CommonFunctions.scalePolygon(scaledPolygon, factor);

            foreach (Building building in buildings) {
                building.scale(scale);
            }

			foreach (Alley alley in alleys) {
				alley.scale(scale);
			}
		}

		private void createAlleys() {
			RandomSeed.rnd = new Random(AlleySeed);

			PointCollection points = new PointCollection(wardPolygon.Points);

            List<int> edgeIndices = new List<int>();
            for (int i = 0; i < points.Count - 1; i++) {
                edgeIndices.Add(i);
            }

            int choice;
            while (true) {
                choice = RandomSeed.rnd.Next(edgeIndices.Count);
                createAlleysOnEdge(edgeIndices[choice]);
                edgeIndices.RemoveAt(choice);

                if (edgeIndices.Count == 0) {
                    break;
                }
            }

			

		}

		private void createAlleysOnEdge(int edgeIndex) {
			Point fromPoint = wardPolygon.Points[edgeIndex];

			Point toPoint;
			if (edgeIndex == wardPolygon.Points.Count - 1) {
				toPoint = wardPolygon.Points[0];
			} else {
				toPoint = wardPolygon.Points[edgeIndex + 1];
			}

			
			double edgeSlope = extendedPolygon.slopes[edgeIndex];

			
			double dx = toPoint.X - fromPoint.X;
			double dy = toPoint.Y - fromPoint.Y;
			double maxDistanceFromStart = dx * dx + dy * dy;
			double normalizationFactor = Math.Sqrt(maxDistanceFromStart);
			dx /= normalizationFactor;
			dy /= normalizationFactor;

            double edgeSlopeAngle = Math.Atan(edgeSlope);
			double maxAngleOffset = Math.PI / 4;
			double randomSlopeAngle = RandomSeed.randomDoubleBetween(edgeSlopeAngle + Math.PI / 2 - maxAngleOffset, edgeSlopeAngle + Math.PI / 2 + maxAngleOffset);
			//double randomSlopeAngle = edgeSlopeAngle + Math.PI / 2;
			double randomSlope = Math.Tan(randomSlopeAngle);

			double minAlleySeparationSquared = minAlleySeparation * minAlleySeparation;
			double minBlockArea = minAlleySeparationSquared;

			maxDistanceFromStart -= minAlleySeparationSquared;

			Alley alley = null;
			Point startPoint = new Point(fromPoint.X, fromPoint.Y);
			double distanceAlongEdge;
			double currentDistanceFromStart = 0;

			int maxIterations = 1000;
			int iter = 0;
			while (true) {

				iter++;

				if (iter > maxIterations) {
                    if (System.Diagnostics.Debugger.IsAttached)
                        System.Diagnostics.Debugger.Break();
                    CommonFunctions.output("Alley generation has gone too long");
				}

				distanceAlongEdge = RandomSeed.randomDoubleBetween(minAlleySeparation, maxAlleySeparation);

				if (iter == 1) {
					//distanceAlongEdge /= 1.5;
				}
				startPoint.X += dx * distanceAlongEdge;
				startPoint.Y += dy * distanceAlongEdge;

				currentDistanceFromStart += distanceAlongEdge;
				if (currentDistanceFromStart * currentDistanceFromStart >= maxDistanceFromStart) {

					break;
				}

				Point oppositeEdgeIntersection = CommonFunctions.getIntersectPointOppositeEdge(startPoint, randomSlope, wardPolygon, edgeIndex, extendedPolygon.slopes);
				Point alleyEndPoint = oppositeEdgeIntersection;

                if (oppositeEdgeIntersection.X != -1 && oppositeEdgeIntersection.Y != -1) {

					Point alleyIntersectPoint;
					double distx, disty;
					double distanceToAlleyIntersectPoint;

					distx = startPoint.X - oppositeEdgeIntersection.X;
					disty = startPoint.Y - oppositeEdgeIntersection.Y;

					double distanceToOppositeEdgeIntersectPoint = distx * distx + disty * disty;
					double shortestDistance = distanceToOppositeEdgeIntersectPoint;

					bool addThisAlley = true;

					foreach (Alley currentAlley in alleys) {
						//Is this necessary after blocks?
						if (CommonFunctions.getDistanceSquared(startPoint, currentAlley.Start) < minAlleySeparationSquared || CommonFunctions.getDistanceSquared(startPoint, currentAlley.End) < minAlleySeparationSquared) {
							addThisAlley = false;
							break;
						}

						if (CommonFunctions.areValuesClose(currentAlley.Slope, randomSlope)) {
							continue;
						}

						alleyIntersectPoint = CommonFunctions.getIntersection(startPoint, randomSlope, currentAlley.Start, currentAlley.Slope);

						distx = startPoint.X - alleyIntersectPoint.X;
						disty = startPoint.Y - alleyIntersectPoint.Y;
						distanceToAlleyIntersectPoint = distx * distx + disty * disty;

						if (currentAlley.isPointOnAlley(alleyIntersectPoint) && distanceToAlleyIntersectPoint < shortestDistance) {
							alleyEndPoint = alleyIntersectPoint;
							shortestDistance = distanceToAlleyIntersectPoint;
						}
							
						//if (distanceToAlleyIntersectPoint < shortestDistance) {
						//	alleyEndPoint = alleyIntersectPoint;
						//	shortestDistance = distanceToAlleyIntersectPoint;
						//}
							


					}

					if (addThisAlley && shortestDistance > minAlleySeparationSquared) {

						alley = new Alley(new Point(startPoint.X, startPoint.Y), alleyEndPoint, randomSlope, ParentCity.availableSteetNames.getStreetName());

						//Split polygons/blocks
						ExtendedPolygon polygonToSplit = null;
						if (blocks.Count == 1) {
							polygonToSplit = blocks[0];
						} else {
							foreach (ExtendedPolygon poly in blocks) {
								if (poly.isPointOnPolygon(alley.Start) && poly.isPointOnPolygon(alley.End)) {
									polygonToSplit = poly;
									break;
								}
							}
						}

						if (polygonToSplit == null) {
                            if (System.Diagnostics.Debugger.IsAttached)
                                System.Diagnostics.Debugger.Break();
                            CommonFunctions.output("Alley doesn't split a polygon somehow");
						}

						PointCollection leftPolygonPoints = new PointCollection();
						PointCollection rightPolygonPoints = new PointCollection();

						int previousDirection = -2;
						int currentDirection;
						Point point, secondPoint;
                        double smallerX = 0;
                        double biggerX = 0;
						for (int i = 0; i < polygonToSplit.points.Count; i++) {
							point = polygonToSplit.points[i];

                            if (i > 0) {
                                secondPoint = polygonToSplit.points[i - 1];

                                if (point.X < secondPoint.X) {
                                    smallerX = point.X;
                                    biggerX = secondPoint.X;
                                }
                                else {
                                    smallerX = secondPoint.X;
                                    biggerX = point.X;
                                }
                            }
							currentDirection = CommonFunctions.whichSideOfLineIsPointOn(alley.Start, alley.End, point);

							if (previousDirection != -2 && previousDirection != currentDirection) {
                                if (CommonFunctions.isPointOnLineSegment(alley.Start, polygonToSplit.slopes[i-1], polygonToSplit.yIntercepts[i-1], smallerX, biggerX)) {
                                    leftPolygonPoints.Add(alley.Start);
                                    rightPolygonPoints.Add(alley.Start);
                                } else {
                                    leftPolygonPoints.Add(alley.End);
                                    rightPolygonPoints.Add(alley.End);
                                }
							}

                            if (i < polygonToSplit.points.Count - 1) {
                                if (currentDirection == 1) {
                                    rightPolygonPoints.Add(point);
                                }
                                else if (currentDirection == -1) {
                                    leftPolygonPoints.Add(point);
                                }
                            }

							previousDirection = currentDirection;
						}

                        leftPolygonPoints.Add(leftPolygonPoints[0]);
                        rightPolygonPoints.Add(rightPolygonPoints[0]);

						ExtendedPolygon leftPolygon = new ExtendedPolygon(leftPolygonPoints);
						ExtendedPolygon rightPolygon = new ExtendedPolygon(rightPolygonPoints);

						if (CommonFunctions.Area(leftPolygon.polygon) > minBlockArea && CommonFunctions.Area(rightPolygon.polygon) > minBlockArea) {
							blocks.Remove(polygonToSplit);
							blocks.Add(leftPolygon);
							blocks.Add(rightPolygon);

							//If new blocks are of the minumum area or higher, add the alley. Otherwise don't
							
							alleys.Add(alley);
                            alley.draw();

							if (edgeIndex == wardPolygon.Points.Count - 2) {
								alley.Polygon.Stroke = MainWindow.yellowBrush;
							}
						}
						
					}
                }
			}
			
		}

        private void createBuildingsFromBlock(ExtendedPolygon block) {
			if (counter == counterToMark) {
                if (System.Diagnostics.Debugger.IsAttached)
                    System.Diagnostics.Debugger.Break();
                CommonFunctions.output("To test wonky behavior in this block");
			}

            Point pointOnLongestEdge = block.points[block.longestEdgeIndex];
            double longestEdgeSlope = block.slopes[block.longestEdgeIndex];
            double perpendicularSlope = -1 / longestEdgeSlope;

            Point start = new Point(pointOnLongestEdge.X, pointOnLongestEdge.Y);
            Point end = new Point(pointOnLongestEdge.X, pointOnLongestEdge.Y);
            double distanceToGo = -1;

            double slope, distance;
            Point intersection, point;
            bool isPerpendicularDirectionDetermined = false;
            int direction = 1;
            for (int i = 0; i < block.points.Count - 1; i++) {
                point = block.points[i];
                intersection = CommonFunctions.getIntersection(point, perpendicularSlope, pointOnLongestEdge, longestEdgeSlope);
				
                distance = CommonFunctions.getDistanceSquared(intersection, point);

                if (!isPerpendicularDirectionDetermined && distance > CommonFunctions.minDifference) {
                    if (point.X < intersection.X) {
                        direction = -1;
                    }
                    isPerpendicularDirectionDetermined = true;
                }

                if (distance > distanceToGo) {
                    distanceToGo = distance;
                }

                if (intersection.X < start.X) {
                    start = intersection;
                }

                if (intersection.X > end.X) {
                    end = intersection;
                }
            }

			if (counter == counterToMark) {
				CommonFunctions.drawTestPoint(start, MainWindow.greenBrush);
				CommonFunctions.drawTestPoint(end, MainWindow.greenBrush);
			}


			double dxAlongEdge = Math.Sqrt(1 / (1 + longestEdgeSlope * longestEdgeSlope));
            double dyAlongEdge = longestEdgeSlope * dxAlongEdge;
            double dxAlongPerpendicular = direction * Math.Sqrt(1 / (1 + perpendicularSlope * perpendicularSlope));
            double dyAlongPerpendicular = perpendicularSlope * dxAlongPerpendicular;

            if (start.X > end.X) {
                dxAlongEdge *= -1;
                dyAlongEdge *= -1;
            }

            //double minDistanceFromRoad = 0;
            double minDistanceFromRoad = 7;
            double edgeDistance = RandomSeed.randomDoubleBetween(minBuildingWidth, maxBuildingWidth)/2;
            //double edgeDistance = maxBuildingWidth / 2;
            double edgeDistanceSquared = edgeDistance * edgeDistance;
            double perpendicularDistance, perpendicularDistanceSquared;
            double lastEdgeDistance = edgeDistance * 2;

            double maxDistanceAlongEdge = CommonFunctions.getDistanceSquared(start, end);
            Building building = null;
            double buildingWidth, buildingHeight;
            double lastPerpendicularDistance;
            Point currentPoint;
            double x, y;
            bool addBuilding;
            Point corner;

            double maxBuildingWidthInThisGeneration;
            double maxXOffset, maxYOffset;
            double randomOffsetX, randomOffsetY;

            int maxIterations = 10000;
            int iter = 0;
            while (edgeDistanceSquared < maxDistanceAlongEdge) {

				//perpendicularDistance = RandomSeed.randomDoubleBetween(minBuildingHeight, maxBuildingHeight)/2 + minDistanceFromRoad;
				perpendicularDistance = maxBuildingHeight/2;

                perpendicularDistanceSquared = perpendicularDistance * perpendicularDistance + minDistanceFromRoad * minDistanceFromRoad;
                lastPerpendicularDistance = perpendicularDistance * 2;

                maxBuildingWidthInThisGeneration = 0;
                while (perpendicularDistanceSquared < distanceToGo) {

                    iter++;

                    if (iter > maxIterations) {
                        if (System.Diagnostics.Debugger.IsAttached)
                            System.Diagnostics.Debugger.Break();
                        CommonFunctions.output("Building generation has gone too long");
                    }

                    x = start.X + dxAlongEdge * edgeDistance + dxAlongPerpendicular * perpendicularDistance;
                    y = start.Y + dyAlongEdge * edgeDistance + dyAlongPerpendicular * perpendicularDistance;
                    currentPoint = new Point(x, y);

					if (counter == counterToMark) {
						CommonFunctions.drawTestPoint(currentPoint, MainWindow.greenBrush);
					}

					//buildingWidth = RandomSeed.randomDoubleBetween(minBuildingWidth, lastEdgeDistance) - halfMinDistanceBetweenBuildings;
					//buildingHeight = RandomSeed.randomDoubleBetween(minBuildingHeight, lastPerpendicularDistance) - halfMinDistanceBetweenBuildings;

                    buildingWidth = lastEdgeDistance;
                    buildingHeight = lastPerpendicularDistance;

                    if (buildingWidth > maxBuildingWidthInThisGeneration) {
                        maxBuildingWidthInThisGeneration = buildingWidth;
                    }

                    //maxXOffset = 0;
                    //maxYOffset = 0;

                    //randomOffsetX = RandomSeed.randomDoubleBetween(-maxXOffset, maxXOffset);
                    //randomOffsetY = RandomSeed.randomDoubleBetween(-maxYOffset, maxYOffset);

                    //currentPoint.X += randomOffsetX * (dxAlongEdge) + randomOffsetY * (dxAlongPerpendicular);
                    //currentPoint.Y += randomOffsetX * (dyAlongEdge) + randomOffsetY * (dyAlongPerpendicular);

                    building = new Building(currentPoint, buildingWidth, buildingHeight, dxAlongEdge, dxAlongPerpendicular, dyAlongEdge, dyAlongPerpendicular, wardType);

					addBuilding = true;
					for (int j = 0; j < building.Polygon.Points.Count - 1; j++) {
						corner = building.Polygon.Points[j];
						if (!CommonFunctions.IsPointInPolygon4(corner, block.polygon)) {
							addBuilding = false;
							break;
						}
					}

					if (addBuilding) {
                        buildings.Add(building);
                    }

					lastPerpendicularDistance = maxBuildingHeight;
                    //lastPerpendicularDistance = RandomSeed.randomDoubleBetween(buildingHeight, maxBuildingHeight);

                    perpendicularDistance += lastPerpendicularDistance;
                    perpendicularDistanceSquared = perpendicularDistance * perpendicularDistance;

                }

				//lastEdgeDistance = RandomSeed.randomDoubleBetween(maxBuildingWidthInThisGeneration, maxBuildingWidth);
				lastEdgeDistance = maxBuildingWidth;

                edgeDistance += lastEdgeDistance;
                edgeDistanceSquared = edgeDistance * edgeDistance;

            }
        }

        private void createBuildings() {
			RandomSeed.rnd = new Random(BuildingSeed);

            foreach (ExtendedPolygon block in blocks) {
				if (counter == counterToMark) {
					buildingBrushColor = MainWindow.redBrush;
				} else {
					buildingBrushColor = MainWindow.greenBrush;
				}

				createBuildingsFromBlock(block);

				
				counter++;
			}

			

        }
	}
}
