﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorldGenerator.Cities {
    class BuildingBaseType {

        public static readonly BuildingBaseType ADMINISTRATION = new BuildingBaseType("Administration", 0, 0, 0, 0);
        public static readonly BuildingBaseType ASYLUM = new BuildingBaseType("Asylum", 0, 0, 0, 0);
        public static readonly BuildingBaseType BARRACK = new BuildingBaseType("Barrack", 5, 20, 0, 0);
        public static readonly BuildingBaseType BATH = new BuildingBaseType("Bath", 0, 0, 0, 0);
        public static readonly BuildingBaseType BOARDING_HOUSE = new BuildingBaseType("Boarding House", 3, 10, 0, 0);
        public static readonly BuildingBaseType CEMETERY = new BuildingBaseType("Cemetery", 0, 0, 0, 0);
        public static readonly BuildingBaseType CISTERN = new BuildingBaseType("Cistern", 0, 0, 0, 0);
        public static readonly BuildingBaseType COLISEUM = new BuildingBaseType("Coliseum", 0, 0, 0, 0);
        public static readonly BuildingBaseType CORRAL = new BuildingBaseType("Corral", 0, 0, 0, 0);
        public static readonly BuildingBaseType FOUNTAIN = new BuildingBaseType("Fountain", 0, 0, 0, 0);
        public static readonly BuildingBaseType GARDEN = new BuildingBaseType("Garden", 0, 0, 0, 0);
        public static readonly BuildingBaseType GRANARY = new BuildingBaseType("Granary", 0, 0, 0, 0);
        public static readonly BuildingBaseType GUILD_HOUSE = new BuildingBaseType("Guild House", 3, 10, 0, 0);
        public static readonly BuildingBaseType HOSPITAL = new BuildingBaseType("Hospital", 0, 0, 0, 0);
        public static readonly BuildingBaseType HOUSE = new BuildingBaseType("House", 1, 4, 0, 0);
        public static readonly BuildingBaseType INFIRMARY = new BuildingBaseType("Infirmary", 0, 0, 0, 0);
        public static readonly BuildingBaseType INN = new BuildingBaseType("Inn", 1, 3, 0, 0);
        public static readonly BuildingBaseType LIBRARY = new BuildingBaseType("Library", 0, 0, 0, 0);
        public static readonly BuildingBaseType MILL = new BuildingBaseType("Mill", 0, 0, 0, 0);
        public static readonly BuildingBaseType OFFICE = new BuildingBaseType("Office", 0, 0, 0, 0);
        public static readonly BuildingBaseType PLAZA = new BuildingBaseType("Plaza", 0, 0, 0, 0);
        public static readonly BuildingBaseType PRISON = new BuildingBaseType("Prison", 0, 0, 0, 0);
        public static readonly BuildingBaseType RELIGIOUS = new BuildingBaseType("Religious", 0, 10, 0, 0);
        public static readonly BuildingBaseType RESTAURANT = new BuildingBaseType("Restaurant", 0, 3, 0, 0);
        public static readonly BuildingBaseType SHOP = new BuildingBaseType("Shop", 1, 3, 0, 0);
        public static readonly BuildingBaseType STABLE = new BuildingBaseType("Stable", 0, 0, 0, 0);
        public static readonly BuildingBaseType TAVERN = new BuildingBaseType("Tavern", 1, 4, 0, 0);
        public static readonly BuildingBaseType TENEMENT = new BuildingBaseType("Tenement", 4, 20, 0, 0);
        public static readonly BuildingBaseType THEATER = new BuildingBaseType("Theater", 0, 0, 0, 0);
        public static readonly BuildingBaseType UNIVERSITY = new BuildingBaseType("University", 0, 0, 0, 0);
        public static readonly BuildingBaseType WAREHOUSE = new BuildingBaseType("Warehouse", 0, 0, 0, 0);
        public static readonly BuildingBaseType WELL = new BuildingBaseType("Well", 0, 0, 0, 0);
        public static readonly BuildingBaseType WORKSHOP = new BuildingBaseType("Workshop", 1, 3, 0, 0);

        public string Name { get; private set; }
        public int MinNumberOfResidents { get; private set; }
        public int MaxNumberOfResidents { get; private set; }
        public int MinNumberOfWorkers { get; private set; }
        public int MaxNumberOfWorkers { get; private set; }

        private BuildingBaseType(string name, int minNumberOfResidents, int maxNumberOfResidents, int minNumberOfWorkers, int maxNumberOfWorkers) {
            Name = name;
            MinNumberOfResidents = minNumberOfResidents;
            MaxNumberOfResidents = maxNumberOfResidents;
            MinNumberOfWorkers = minNumberOfWorkers;
            MaxNumberOfWorkers = maxNumberOfWorkers;
        }
    }
}
