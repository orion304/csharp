﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using WorldGenerator.Tools;

namespace WorldGenerator.Cities {
	public class Alley {

		private double scaleFactor = 1;

		public Point Start;
		public Point End;
		public Line Polygon;
		public Line ScaledPolygon;
		public readonly double Slope;
        public string Name { get; set; }

        private readonly double startx, endx, starty, endy;

		public Alley(Point start, Point end, double slope, string name) {
            this.Name = name;
			this.Start = start;
			this.End = end;
			Slope = slope;
            
			Polygon = new Line();
			ScaledPolygon = new Line();
            Polygon.X1 = start.X;
            Polygon.Y1 = start.Y;
            Polygon.X2 = end.X;
            Polygon.Y2 = end.Y;

			if (start.X < end.X) {
				startx = start.X;
				endx = end.X;
			} else {
				startx = end.X;
				endx = start.X;
			}

			if (start.Y < end.Y) {
				starty = start.Y;
				endy = end.Y;
			} else {
				starty = end.Y;
				endy = start.Y;
			}

			ScaledPolygon.X1 = start.X;
            ScaledPolygon.Y1 = start.Y;
            ScaledPolygon.X2 = end.X;
            ScaledPolygon.Y2 = end.Y;

        }

		public bool isPointOnAlley(Point point) {
			return (point.X >= startx && point.X <= endx && point.Y >= starty && point.Y <= endy);
		}

		public void scale(double factor) {
			double mult = factor / scaleFactor;
			scaleFactor = factor;

			Start.X *= mult;
			Start.Y *= mult;
			End.X *= mult;
			End.Y *= mult;

            ScaledPolygon.X1 = Start.X;
            ScaledPolygon.Y1 = Start.Y;
            ScaledPolygon.X2 = End.X;
            ScaledPolygon.Y2 = End.Y;

		}

        public void draw() {
            ScaledPolygon.Stroke = MainWindow.blueBrush;
            ScaledPolygon.StrokeThickness = 3;
            MainWindow.WindowBox.Children.Add(ScaledPolygon);

            ScaledPolygon.ToolTip = Name;
            ToolTipService.SetShowDuration(ScaledPolygon, Building.tooltipDuration);
        }

	}
}
