﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorldGenerator.Cities {
    public class WardType {

		public static WardType PATRICIATE = new WardType("Patriciate", CityEnums.BuildingQuality.A, CityEnums.BuildingQuality.B);
		public static WardType MERCHANT = new WardType("Merchant", CityEnums.BuildingQuality.A, CityEnums.BuildingQuality.B, CityEnums.BuildingQuality.C);
		public static WardType MILITARY = new WardType("Military", CityEnums.BuildingQuality.B, CityEnums.BuildingQuality.C, CityEnums.BuildingQuality.D);
		public static WardType ADMINISTRATION = new WardType("Administration", CityEnums.BuildingQuality.B, CityEnums.BuildingQuality.C);
		public static WardType ODERIFOROUSBUSINESS = new WardType("Oderiforous Business", CityEnums.BuildingQuality.C, CityEnums.BuildingQuality.D);
		public static WardType CRAFTSMEN = new WardType("Craftsmen", CityEnums.BuildingQuality.B, CityEnums.BuildingQuality.C, CityEnums.BuildingQuality.D);
		public static WardType SEA = new WardType("Sea", CityEnums.BuildingQuality.C, CityEnums.BuildingQuality.D);
		public static WardType RIVER = new WardType("River", CityEnums.BuildingQuality.B, CityEnums.BuildingQuality.C, CityEnums.BuildingQuality.D);
		public static WardType MARKET = new WardType("Market", CityEnums.BuildingQuality.A, CityEnums.BuildingQuality.B, CityEnums.BuildingQuality.C);
		public static WardType GATE = new WardType("Gate", CityEnums.BuildingQuality.B, CityEnums.BuildingQuality.C, CityEnums.BuildingQuality.D);
		public static WardType SLUM = new WardType("Slum", CityEnums.BuildingQuality.D);
		public static WardType SHANTYTOWN = new WardType("Shanty Town", CityEnums.BuildingQuality.D);

		public string name;
		public CityEnums.BuildingQuality[] qualities;
		public double buildingDensityPerAcre;

		public static void setBuildingDensities(TownType townType) {
			double lowerDensity = townType.structuresPerAcreLower;
			double higherDensity = townType.structuresPerAcreHigher;

			double step = (higherDensity - lowerDensity) / Values.Count();
			double density = lowerDensity;
			foreach (WardType wardType in Values) {
				wardType.buildingDensityPerAcre = density;
				density += step;
			}

		}

		private WardType(string name, params CityEnums.BuildingQuality[] qualities) {
			this.name = name;
			this.qualities = qualities; 
		}

		public static IEnumerable<WardType> Values {
            get {
				yield return PATRICIATE;
				yield return MERCHANT;
				yield return MILITARY;
				yield return ADMINISTRATION;
				yield return ODERIFOROUSBUSINESS;
				yield return CRAFTSMEN;
				yield return SEA;
				yield return RIVER;
				yield return MARKET;
				yield return GATE;
				yield return SLUM;
				yield return SHANTYTOWN;
			}
        }

        public override string ToString() {
            return name;
        }

    }
}
