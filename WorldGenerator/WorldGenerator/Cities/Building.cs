﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;
using WorldGenerator.NPC;
using WorldGenerator.Tools;

namespace WorldGenerator.Cities {
    public class Building {

        public static int tooltipDuration = 100000;

        private SolidColorBrush defaultBrushColor = null;
        private double scaleFactor = 1;

        public Point Center;
        public Point ScaledCenter;
        public Polygon Polygon;
        public Polygon ScaledPolygon;
		public List<NPC.NPC> NPCs = new List<NPC.NPC>();
        public List<NPC.NPC> workers = new List<NPC.NPC>();

        private double width, height;
        private double dxWidth, dyWidth;
        private double dxHeight, dyHeight;

        public string Name { get; set; }
		public string Type { get; set; }
        public string Quality { get; set; }
        public string BaseType { get; set; }
        public WardType WardType { get; set; }
        public string buildingQualitySymbol;
        public string tooltip = "";

        public Building(Point center, double width, double height, double dxWidth, double dxHeight, double dyWidth, double dyHeight, WardType wardType) {
            this.WardType = wardType;

            this.width = width;
			this.height = height;
            double halfWidth = width / 2;
            double halfHeight = height / 2;

            this.dxHeight = dxHeight * halfHeight;
            this.dxWidth = dxWidth * halfWidth;
            this.dyHeight = dyHeight * halfHeight;
            this.dyWidth = dyWidth * halfWidth;

            

            this.Center = center;
            this.ScaledCenter = new Point(center.X, center.Y);

            this.Polygon = new Polygon();
            this.ScaledPolygon = new Polygon();
            PointCollection points = new PointCollection();

            double x = Center.X;
            double y = Center.Y;

            points.Add(new Point(x - this.dxHeight - this.dxWidth, y - this.dyHeight - this.dyWidth));
            points.Add(new Point(x - this.dxHeight + this.dxWidth, y - this.dyHeight + this.dyWidth));
            points.Add(new Point(x + this.dxHeight + this.dxWidth, y + this.dyHeight + this.dyWidth));
            points.Add(new Point(x + this.dxHeight - this.dxWidth, y + this.dyHeight - this.dyWidth));
            points.Add(points[0]);

            this.Polygon.Points = points;
            this.ScaledPolygon.Points = new PointCollection(points);

			createBuildingType(wardType);

        }

        public void select(SolidColorBrush brushColor) {
            ScaledPolygon.Stroke = brushColor;
            ScaledPolygon.Fill = brushColor;
        }

        public void deselect() {
            ScaledPolygon.Stroke = defaultBrushColor;
            ScaledPolygon.Fill = defaultBrushColor;
        }

		public void addNPC(NPC.NPC npc) {
            if (NPCs.Count == 0) {
                string name = npc.Name;
                if (name.Contains(" ")) {
                    name = name.Substring(name.LastIndexOf(" ") + 1);
                }
                Name = String.Format("{0}'s {1}", name, Type);

                if (BaseType == "Religious") {
                    Name = String.Format("{0} of {1}", Name, Tools.Distributions.gods.randomObject(npc.alignment));
                }
            }

            if (npc.npcClass == NPCClass.Child) {
                npc.Profession = "Child";
            }
            else {
                npc.Profession = Type;
            }
            npc.LivesAt = this;
            npc.WorksAt = this;
			NPCs.Add(npc);
		}

        public void addWorker(NPC.NPC npc) {
            npc.Profession = Type;
            npc.WorksAt = this;
            workers.Add(npc);
        }

        public void updateTooltip() {

            string symbol = "";
            if (buildingQualitySymbol.Length > 0) {
                symbol = String.Format("({0}) ", buildingQualitySymbol);
            }
            
            tooltip = String.Format("{0}{1}\n  {2} - {3}\n", symbol, Name, Type, Quality);

            foreach (NPC.NPC npc in NPCs) {
				tooltip += npc + "\n";
			}

            foreach (NPC.NPC npc in workers) {
                tooltip += String.Format("- {0}\n", npc);
            }

            ScaledPolygon.ToolTip = tooltip.Substring(0, tooltip.Length - 1);
            ToolTipService.SetShowDuration(ScaledPolygon, tooltipDuration);
		}

        public void draw(SolidColorBrush brushColor) {
            if (defaultBrushColor == null) {
                defaultBrushColor = brushColor;
            }

			ScaledPolygon.Stroke = brushColor;
			ScaledPolygon.Fill = brushColor;
            
			updateTooltip();
            MainWindow.WindowBox.Children.Add(ScaledPolygon);

            ScaledPolygon.MouseUp += ScaledPolygon_MouseUp;
        }

        private void ScaledPolygon_MouseUp(object sender, System.Windows.Input.MouseButtonEventArgs e) {
            MainWindow.currentInstance?.buildingWindow?.deselectBuildings();
            MainWindow.currentInstance?.buildingWindow?.selectBuilding(this);
        }

        public void scale(double factor) {
            double mult = factor / scaleFactor;
			scaleFactor = factor;

            ScaledCenter.X *= mult;
            ScaledCenter.X *= mult;

			CommonFunctions.scalePolygon(ScaledPolygon, mult);

        }

		private void createBuildingType(WardType wardType) {

			string type = Tools.Distributions.buildingDistribution.randomObject(wardType);

			int i = type.LastIndexOf(" ");
			Type = type.Substring(0, i);
            BaseType = Type;
			Quality = type.Substring(i + 1);

			int j = RandomSeed.rnd.Next(Quality.Length);
			Quality = Quality.Substring(j, 1);

			if (Type == "Office") {
				Type = Tools.Distributions.offices.getRandomObject();
			} else if (Type == "Workshop") {
				Type = Tools.Distributions.workshops.getRandomObject();
			} else if (Type == "Shop") {
				Type = Tools.Distributions.shops.getRandomObject();
			} else if (Type == "Administration") {
                Type = Tools.Distributions.admin.getRandomObject();
            } else if (Type == "Religious") {
                Type = Tools.Distributions.religious.getRandomObject();
            }

			switch (Quality) {
				case "A":
					Quality = "Exceptional";
                    buildingQualitySymbol = "*";
					break;
				case "B":
					Quality = "Fine";
                    buildingQualitySymbol = "+";
					break;
				case "C":
					Quality = "Average";
                    buildingQualitySymbol = "";
					break;
				case "D":
					Quality = "Poor";
                    buildingQualitySymbol = "-";
					break;
			}

        }

        public override string ToString() {
            return Name;
        }

    }
}
