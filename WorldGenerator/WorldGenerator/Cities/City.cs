﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Shapes;
using WorldGenerator.NPC;
using WorldGenerator.Tools;

namespace WorldGenerator.Cities {
    public class City {

        private static double beggarChance = .3;

        public string Name { get; set; }
        public int AdultPopulation { get; set; }
        public int ChildPopulation { get; set; }
        public int Population { get; set; }
        public TownType TownType { get; set; }
        public int BuyingPower { get; set; }
        public int GoldLimit { get; set; }
        public readonly List<PowerCenter> PowerCenters = new List<PowerCenter>();
        public Dictionary<NPCClass, int[]> levelSpreadByClass = new Dictionary<NPCClass, int[]>();
        public List<NPC.NPC> NPCs = new List<NPC.NPC>();
		public double AreaAcres { get; set; }
		public readonly List<Ward> Wards = new List<Ward>();
        public readonly List<Building> Buildings = new List<Building>();

        public StreetNames availableSteetNames;
		private int NPCGenerationSeed;
        private int nextNPCSeed;
		private int MapGenerationSeed;
		private int nextMapSeed;
		private double populationDensity;

        public City(string name, TownType type = null, CityEnums.CityMixType mixType = CityEnums.CityMixType.Integrated, Race primaryRace = null, Alignment primaryAlignment = null) {
            availableSteetNames = new StreetNames();

            NPCGenerationSeed = RandomSeed.rnd.Next();
			MapGenerationSeed = RandomSeed.rnd.Next();

            if (type == null) {
                type = TownType.getRandomTownType();
            }

            if (primaryRace == null) {
                primaryRace = Race.HUMAN;
            }

            Race.weightRacialDistribution(mixType, primaryRace);
            Alignment.changeWeight(primaryAlignment);

            TownType = type;
            AdultPopulation = type.getPopulation();
            ChildPopulation = (int)(0.01 * (RandomSeed.rnd.Next(30) + 10) * AdultPopulation);
            Population = AdultPopulation + ChildPopulation;
            GoldLimit = type.goldLimit;
            BuyingPower = (int)(AdultPopulation * 0.1 * 0.5 * type.goldLimit);

			Tools.Distributions.buildWardTypeDistribution(TownType);

            generatePowerCenters();
            generateNPCs();
			generateMap();

			assignNPCsToBuildings();
            assignHousedNPCsToJobs();

            generateDistrictNames();

            updateBuildingTooltips();
        }

        private void generateDistrictNames() {
            foreach (Ward ward in Wards) {
                ward.Name = DistrictNames.getDistrictName(ward.wardType);
                ward.updateTooltip();
            }
        }

        private void updateBuildingTooltips() {
            foreach (Ward ward in Wards) {
                foreach (Building building in ward.buildings) {
                    building.updateTooltip();
                }
            }
        }

		private void assignNPCsToBuildings() {

			List<Building> allBuildings = new List<Building>();
			List<NPC.NPC> adultNPCsToAssign = new List<NPC.NPC>();
			List<NPC.NPC> childNPCsToAssign = new List<NPC.NPC>();

			foreach (Ward ward in Wards) {
				allBuildings.AddRange(ward.buildings);
			}

			foreach (NPC.NPC npc in NPCs) {
				if (npc.npcClass != NPCClass.Child) {
					adultNPCsToAssign.Add(npc);
				} else {
					childNPCsToAssign.Add(npc);
				}
			}

			//assign 1 NPC to each building
			int i;
			NPC.NPC chosenNPC;
			foreach (Building building in allBuildings) {
				i = RandomSeed.rnd.Next(adultNPCsToAssign.Count);
				chosenNPC = adultNPCsToAssign[i];

				adultNPCsToAssign.RemoveAt(i);
				building.addNPC(chosenNPC);
			}

			Building randomBuilding;
			while (adultNPCsToAssign.Count > 0) {
				randomBuilding = allBuildings[RandomSeed.rnd.Next(allBuildings.Count)];
				randomBuilding.addNPC(adultNPCsToAssign[0]);
				adultNPCsToAssign.RemoveAt(0);
			}

			int numberOfAttempts = 0;
			int maxNumberOfPlacementAttempts = 5;
			while (childNPCsToAssign.Count > 0) {
				randomBuilding = allBuildings[RandomSeed.rnd.Next(allBuildings.Count)];

				if (randomBuilding.NPCs.Count == 1 && numberOfAttempts < maxNumberOfPlacementAttempts) {
					numberOfAttempts++;
				} else {
					randomBuilding.addNPC(childNPCsToAssign[0]);
					childNPCsToAssign.RemoveAt(0);
					numberOfAttempts = 0;
				}
			}

		}

        private void assignHousedNPCsToJobs() {

            List<NPC.NPC> housedAdultCommoners = new List<NPC.NPC>();
            List<NPC.NPC> housedAdultOther = new List<NPC.NPC>();

            List<Building> nonHouseBuildings = new List<Building>();

            string[] houseTypes = { "House", "Tenement", "Boarding House"};

            foreach (Ward ward in Wards) {
                foreach (Building building in ward.buildings) {

                    if (houseTypes.Contains(building.Type)) {
                        foreach (NPC.NPC npc in building.NPCs) {
                            if (npc.npcClass == NPCClass.Commoner) {
                                housedAdultCommoners.Add(npc);
                            } else if (npc.npcClass != NPCClass.Child) {
                                housedAdultOther.Add(npc);
                            }
                        }
                    } else {
                        nonHouseBuildings.Add(building);
                    }

                }
            }

            NPC.NPC worker;
            Building randomBuilding;
            while (housedAdultCommoners.Count > 0) {
                worker = housedAdultCommoners[0];

                if (RandomSeed.rnd.NextDouble() > beggarChance) {
                    randomBuilding = nonHouseBuildings[RandomSeed.rnd.Next(nonHouseBuildings.Count)];
                    randomBuilding.addWorker(worker);
                } else {
                    worker.Profession = "Beggar";
                }

                housedAdultCommoners.RemoveAt(0);

            }

            while (housedAdultOther.Count > 0) {
                worker = housedAdultOther[0];

                randomBuilding = nonHouseBuildings[RandomSeed.rnd.Next(nonHouseBuildings.Count)];
                randomBuilding.addWorker(worker);

                housedAdultOther.RemoveAt(0);

            }

        }

        public void scaleWards(double scale) {
            foreach (Ward ward in Wards) {
                ward.scale(scale);
            }
        }

        private void generatePowerCenters() {
            int numberOfPowerCenters = 1;
            if (TownType.centerModifier >= 4) {
                numberOfPowerCenters += TownType.centerModifier - 3;
            }

            if (numberOfPowerCenters > 4) {
                numberOfPowerCenters = 4;
            }

            for (int i = 0; i < numberOfPowerCenters; i++) {
                PowerCenters.Add(new PowerCenter(TownType.centerModifier));
            }
        }

        private void generateNPCs() {
            int numberOfHighestLevels = 1;
            int rangerDruidMod = 0;

            if (TownType.levelModifier <= -2 && Dice.d100() >= 96) {
                rangerDruidMod = 10;
            }

            if (TownType.levelModifier >= 6) {
                numberOfHighestLevels++;
            }

            if (TownType.levelModifier >= 9) {
                numberOfHighestLevels++;
            }

            if (TownType.levelModifier >= 12) {
                numberOfHighestLevels++;
            }

            initializeLevelSpreadByClass();

            int mod;
            int level;
            int amount;
            int total = 0;
            foreach (NPCClass classType in NPCClass.Values) {
                if (classType == NPCClass.Child) {
                    continue;
                }

                for (int i = 0; i < numberOfHighestLevels; i++) {
                    mod = 0;

                    if (classType == NPCClass.Ranger || classType == NPCClass.Druid) {
                        mod = rangerDruidMod;
                    }

                    level = Dice.Roll(classType.roll, classType.die) + TownType.levelModifier + mod;
                    if (level > 20) {
                        level = 20;
                    } else if (level <= 0) {
                        break;
                    }

                    levelSpreadByClass[classType][level - 1]++;

                    amount = 1;
                    total += amount;
                    if (level > 1) {
                        while (true) {
                            level = level / 2;
                            amount *= 2;

                            levelSpreadByClass[classType][level - 1] += amount;

                            if (level <= 1) {
                                break;
                            }
                        }
                    }
                }
            }

            int rest = AdultPopulation - total;

            if (AdultPopulation < total) {
                AdultPopulation = total;
            }

            if (rest > 0) {
                int numberOfWarriors =(int)( 0.05 * rest);
                int numberOfExperts = (int)(0.03 * rest);
                int numberOfAristocrats = (int)(0.005 * rest);
                int numberOfAdepts = (int)(0.005 * rest);
                int numberOfCommoners = rest - (numberOfAdepts + numberOfAristocrats + numberOfExperts + numberOfWarriors);

                levelSpreadByClass[NPCClass.Warrior][0] += numberOfWarriors;
                levelSpreadByClass[NPCClass.Expert][0] += numberOfExperts;
                levelSpreadByClass[NPCClass.Aristocrat][0] += numberOfAristocrats;
                levelSpreadByClass[NPCClass.Adept][0] += numberOfAdepts;
                levelSpreadByClass[NPCClass.Commoner][0] += numberOfCommoners;
            }

            levelSpreadByClass[NPCClass.Child][0] = ChildPopulation;
            Population = AdultPopulation + ChildPopulation;

            generateNPCsFromLevelSpread();

        }

        private void generateNPCsFromLevelSpread() {

            RandomSeed.rnd = new Random(NPCGenerationSeed);
            nextNPCSeed = RandomSeed.rnd.Next();

            int[] levels;
            foreach (NPCClass classType in levelSpreadByClass.Keys) {
                levels = levelSpreadByClass[classType];
                for (int i = 0; i < levels.Length; i++) {
                    for (int j = 0; j < levels[i]; j++) {
                        RandomSeed.rnd = new Random(nextNPCSeed);
                        nextNPCSeed = RandomSeed.rnd.Next();
                        NPCs.Add(new NPC.NPC(i + 1, classType));
                    }
                }
            }

        }

        private void initializeLevelSpreadByClass() {
            foreach (NPCClass classType in NPCClass.Values) {
                levelSpreadByClass[classType] = new int[20];
            }
        }

		public void outputCityDiagnostic() {
			int numberOfNpcs = NPCs.Count;
			int displayXNpcs = 10;
			int mod = numberOfNpcs / displayXNpcs;
			Dictionary<Race, int> raceDist = new Dictionary<Race, int>();
			Dictionary<Alignment, int> alignDist = new Dictionary<Alignment, int>();

			int i = 0;
			foreach (NPC.NPC npc in NPCs) {
				if (!raceDist.ContainsKey(npc.race)) {
					raceDist[npc.race] = 1;
				} else {
					raceDist[npc.race] += 1;
				}

				if (!alignDist.ContainsKey(npc.alignment)) {
					alignDist[npc.alignment] = 1;
				} else {
					alignDist[npc.alignment] += 1;
				}

				if (i % mod == 0) {
					CommonFunctions.output(npc);
				}
				i++;
			}

			foreach (Race race in raceDist.Keys) {
				int count = raceDist[race];
				double fraction = (double)count / (double)numberOfNpcs;
				CommonFunctions.output("{0}: {1} ({2})", race.name, count, fraction);
			}

			foreach (Alignment align in alignDist.Keys) {
				int count = alignDist[align];
				double fraction = (double)count / (double)numberOfNpcs;
				CommonFunctions.output("{0}: {1} ({2})", align.name, count, fraction);
			}
			
		}

		private void generateMap() {
            

			RandomSeed.rnd = new Random(MapGenerationSeed);
			if (TownType.populationDensityHigher == 0) {
				return;
			}

			populationDensity = RandomSeed.randomDoubleBetween(TownType.populationDensityLower, TownType.populationDensityHigher);
			AreaAcres = AdultPopulation / populationDensity;
			double areaSqFt = AreaAcres * CommonFunctions.sqftInAcre;
			double length = Math.Sqrt(areaSqFt);
			//double length = 300;

			int numberOfWards = (int) RandomSeed.randomDoubleBetween(0.1 * AreaAcres, 0.5 * AreaAcres);
			//int numberOfWards = 4;
            int numberOfGeneratedWards = 4 * numberOfWards;

			double minX = 0;
			double maxX = length * 2;
			double minY = 0;
			double maxY = length * 2;

			double[] xValuesIn = new double[numberOfGeneratedWards];
			double[] yValuesIn = new double[numberOfGeneratedWards];
			for (int i = 0; i < numberOfGeneratedWards; i++) {
				xValuesIn[i] = RandomSeed.randomDoubleBetween(minX, maxX);
				yValuesIn[i] = RandomSeed.randomDoubleBetween(minY, maxY);
			}

			VoronoiGenerator voronoi = new VoronoiGenerator(xValuesIn, yValuesIn, minX, maxX, minY, maxY, true);
			WardType.setBuildingDensities(TownType);

            double totalArea = 0;
            Ward ward;
			Point center;
			Polygon wardPolygon;
			foreach (double key in voronoi.CentersByDistanceFromCenter.Keys) { 
				center = voronoi.CentersByDistanceFromCenter[key];
				wardPolygon = voronoi.PolygonsByCenter[center];
                ward = new Ward(center, wardPolygon, TownType, Tools.Distributions.wardTypeDistribution.getRandomObject(), this);
                totalArea += ward.area;
                Wards.Add(ward);
                Buildings.AddRange(ward.buildings);
				if (Wards.Count >= numberOfWards || totalArea > areaSqFt) {
					break;
				}
			}

            AreaAcres = totalArea / CommonFunctions.sqftInAcre;

		}

    }
}
