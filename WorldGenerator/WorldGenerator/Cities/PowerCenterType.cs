﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorldGenerator.Cities {
    public class PowerCenterType {

        public static readonly PowerCenterType CONVENTIONAL = new PowerCenterType("Conventional");
        public static readonly PowerCenterType CONVENTIONALMONSTROUS = new PowerCenterType("Conventional (Monstrous)");
        public static readonly PowerCenterType NONSTANDARD = new PowerCenterType("Nonstandard");
        public static readonly PowerCenterType MAGICAL = new PowerCenterType("Magical");

        public string name;

        private PowerCenterType(string name) {
            this.name = name;
        }

        public override string ToString() {
            return name;
        }
    }
}
