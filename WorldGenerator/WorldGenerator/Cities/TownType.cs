﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorldGenerator.Cities {
    public class TownType {

        public static readonly TownType THORP = new TownType("Thorp", 20, 80, 40, -1, -3, 0, 0, 0, 0);
        public static readonly TownType HAMLET = new TownType("Hamlet", 81, 400, 100, 0, -2, 0, 0, 0, 0);
        public static readonly TownType VILLAGE = new TownType("Village", 401, 900, 200, 1, -1, 0, 0, 0, 0);
        public static readonly TownType SMALLTOWN = new TownType("Small Town", 901, 2000, 800, 2, 30, 40, 0, 15, 20);
        public static readonly TownType LARGETOWN = new TownType("Large Town", 2001, 5000, 3000, 3, 3, 40, 60, 20, 30);
        public static readonly TownType SMALLCITY = new TownType("Small City", 5001, 12000, 15000, 4, 6, 80, 120, 40, 60);
        public static readonly TownType LARGECITY = new TownType("Large City", 12001, 25000, 40000, 5, 9, 125, 145, 50, 70);
        public static readonly TownType METROPOLIS = new TownType("Metropolis", 25001, 100000, 100000, 6, 12, 150, 200, 60, 80);
        
        public string name;
        public int fromPopulation;
        public int toPopulation;
        public int goldLimit;
        public int centerModifier;
        public int levelModifier;
        public int populationDensityLower;
        public int populationDensityHigher;
        public int structuresPerAcreLower;
        public int structuresPerAcreHigher;

        private TownType(string name, int fromPopulation, int toPopulation, int goldLimit, int centerModifier, int levelModifier, int populationDensityLower, int populationDensityHigher, int structuresPerAcreLower, int structuresPerAcreHigher) {
            this.name = name;
            this.fromPopulation = fromPopulation;
            this.toPopulation = toPopulation;
            this.goldLimit = goldLimit;
            this.centerModifier = centerModifier;
            this.levelModifier = levelModifier;
            this.populationDensityLower = populationDensityLower;
            this.populationDensityHigher = populationDensityHigher;
            this.structuresPerAcreLower = structuresPerAcreLower;
            this.structuresPerAcreHigher = structuresPerAcreHigher;
        }

        public int getPopulation() {
            return RandomSeed.rnd.Next(toPopulation - fromPopulation) + fromPopulation;
        }

        public static TownType getRandomTownType() {
            int roll = Dice.d100();
            if (roll <= 10) {
                return THORP;
            }
            else if (roll <= 30) {
                return HAMLET;
            }
            else if (roll <= 50) {
                return VILLAGE;
            }
            else if (roll <= 70) {
                return SMALLTOWN;
            }
            else if (roll <= 85) {
                return LARGETOWN;
            }
            else if (roll <= 95) {
                return SMALLCITY;
            }
            else if (roll <= 99) {
                return LARGECITY;
            }
            else {
                return METROPOLIS;
            }
        }

        public override string ToString() {
            return name;
        }

    }
}
