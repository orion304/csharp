﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorldGenerator.Cities {
    class DistrictNames {

        public static string getDistrictName(WardType wardType) {
            string name = "";
            if (wardType == WardType.PATRICIATE) {
                name = getPatriciateDistrictName();

            } else if (wardType == WardType.MERCHANT) {
                name = getMerchantDistrictName();

            } else if (wardType == WardType.MILITARY) {
                name = getMilitaryDistrictName();

            } else if (wardType == WardType.ADMINISTRATION) {
                name = getAdministrationDistrictName();

            } else if (wardType == WardType.ODERIFOROUSBUSINESS) {
                name = getOderiforousBusinessDistrictName();

            } else if (wardType == WardType.CRAFTSMEN) {
                name = getCraftsmenDistrictName();

            } else if (wardType == WardType.SEA) {
                name = getSeaDistrictName();

            } else if (wardType == WardType.RIVER) {
                name = getRiverDistrictName();

            } else if (wardType == WardType.MARKET) {
                name = getMarketDistrictName();

            } else if (wardType == WardType.GATE) {
                name = getGateDistrictName();

            } else if (wardType == WardType.SLUM) {
                name = getSlumDistrictName();

            } else if (wardType == WardType.SHANTYTOWN) {
                name = getShantyTownDistrictName();
            }


            return name;
        }

        private static string getPatriciateDistrictName() {
            return "Patriciate";
        }
        private static string getMerchantDistrictName() {
            return "Merchant";
        }
        private static string getMilitaryDistrictName() {
            return "Military";
        }
        private static string getAdministrationDistrictName() {
            return "Administration";
        }
        private static string getOderiforousBusinessDistrictName() {
            return "Oderiforous Business";
        }
        private static string getCraftsmenDistrictName() {
            return "Craftsmen";
        }
        private static string getSeaDistrictName() {
            return "Sea";
        }
        private static string getRiverDistrictName() {
            return "River";
        }
        private static string getMarketDistrictName() {
            return "Market";
        }
        private static string getGateDistrictName() {
            return "Gate";
        }
        private static string getSlumDistrictName() {
            return "Slum";
        }
        private static string getShantyTownDistrictName() {
            return "Shanty Town";
        }

    }
}
