﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorldGenerator.NPC;
using WorldGenerator.Tools;

namespace WorldGenerator.Cities {
    public class PowerCenter {

        private static readonly RandomDiceDistribution<Alignment> powerCenterAlignment = new RandomDiceDistribution<Alignment>();

        static PowerCenter() {
            powerCenterAlignment.setBound(35, Alignment.LAWFUL_GOOD);
            powerCenterAlignment.setBound(39, Alignment.NEUTRAL_GOOD);
            powerCenterAlignment.setBound(41, Alignment.CHAOTIC_GOOD);
            powerCenterAlignment.setBound(61, Alignment.LAWFUL_NEUTRAL);
            powerCenterAlignment.setBound(63, Alignment.NEUTRAL_NEUTRAL);
            powerCenterAlignment.setBound(64, Alignment.CHAOTIC_NEUTRAL);
            powerCenterAlignment.setBound(90, Alignment.LAWFUL_EVIL);
            powerCenterAlignment.setBound(98, Alignment.NEUTRAL_EVIL);
            powerCenterAlignment.setBound(100, Alignment.CHAOTIC_EVIL);
        }

        public PowerCenterType PowerCenterType { get; set; }
        public Alignment PowerCenterAlignment { get; set; }

        public PowerCenter(int rollModifier) {
            int roll = Dice.d20() + rollModifier;

            if (roll <= 13) {
                if (Dice.d100() <= 5) {
                    PowerCenterType = PowerCenterType.CONVENTIONALMONSTROUS;
                } else {
                    PowerCenterType = PowerCenterType.CONVENTIONAL;
                }
            }
            else if (roll <= 18) {
                PowerCenterType = PowerCenterType.NONSTANDARD;
            }
            else {
                PowerCenterType = PowerCenterType.MAGICAL;
            }

            PowerCenterAlignment = getRandomPowerCenterAlignment();
        }

        public static Alignment getRandomPowerCenterAlignment() {
            return powerCenterAlignment.getRandomObject();
        }

        public override string ToString() {
            return String.Format("{0} - {1}", PowerCenterType, PowerCenterAlignment);
        }

    }
}
