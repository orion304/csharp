﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;

namespace WorldGenerator.Tools {
	public class VoronoiGenerator {

        private static double closeVertexDistance = 100;
        private static double closeVertexDistanceSquared = closeVertexDistance * closeVertexDistance;

		private Voronoi2.Voronoi voronoi;
		private double minX, minY, maxX, maxY, centerX, centerY;
		private double[] xValuesIn, yValuesIn;
        private bool mergeCloseVertices;

		public SortedDictionary<double, Point> CentersByDistanceFromCenter = new SortedDictionary<double, Point>();
		public Dictionary<Point, Polygon> PolygonsByCenter = new Dictionary<Point, Polygon>();
		public List<Voronoi2.GraphEdge> GraphEdges { get; set; }
        public List<ExtendedGraphEdge> ExtendedGraphEdges;
		public List<Polygon> Polygons { get; set; }

		public VoronoiGenerator(double[] xValuesIn, double[] yValuesIn, double minX, double maxX, double minY, double maxY, bool mergeCloseVertices = false) {
			voronoi = new Voronoi2.Voronoi(1);

            this.mergeCloseVertices = mergeCloseVertices;

			this.xValuesIn = xValuesIn;
			this.yValuesIn = yValuesIn;

			this.minX = minX;
			this.maxX = maxX;
			this.minY = minY;
			this.maxY = maxY;

			centerX = (maxX - minX) / 2;
			centerY = (maxY - minY) / 2;

			GraphEdges = voronoi.generateVoronoi(xValuesIn, yValuesIn, minX, maxX, minY, maxY);

            ExtendedGraphEdges = new List<ExtendedGraphEdge>();

            foreach (Voronoi2.GraphEdge edge in GraphEdges) {
                ExtendedGraphEdges.Add(new ExtendedGraphEdge(edge));
            }

            if (this.mergeCloseVertices) {
                MergeCloseVertices();
            }

            //foreach (ExtendedGraphEdge edge in ExtendedGraphEdges) {
            //    Line line = new Line();
            //    line.X1 = edge.point1.X;
            //    line.X2 = edge.point2.X;
            //    line.Y1 = edge.point1.Y;
            //    line.Y2 = edge.point2.Y;
            //    line.Stroke = MainWindow.blackBrush;
            //    MainWindow.WindowBox.Children.Add(line);
            //}

            Polygons = generatePolygonsFromGraphEdges();

            if (mergeCloseVertices) {
                removeConcavePolygons();
            }
        }

        private void MergeCloseVertices() {
            ExtendedGraphEdge edge, edge2;
            Point midpoint;

            int lastMergeI, lastMergeJ;
            int numberOfMerges = 0;
            bool merged = false;

            double edgeLength;

            List<int> collapsedEdges = new List<int>();

            while (true) {
                for (int i = 0; i < ExtendedGraphEdges.Count; i++) {
                    if (collapsedEdges.Contains(i)) {
                        continue;
                    }

                    edge = ExtendedGraphEdges[i];
                    edgeLength = CommonFunctions.getDistanceSquared(edge.point1, edge.point2);

                    if (edgeLength < closeVertexDistanceSquared) {
                        midpoint = CommonFunctions.getMidpoint(edge.point1, edge.point2);

                        lastMergeI = i;
                        collapsedEdges.Add(i);

                        for (int j = 0; j < ExtendedGraphEdges.Count; j++) {
                            if (i == j) {
                                continue;
                            }

                            edge2 = ExtendedGraphEdges[j];

                            if (CommonFunctions.arePointsClose(edge2.point1, edge.point1) || CommonFunctions.arePointsClose(edge2.point1, edge.point2)) {
                                edge2.setPoint1(midpoint);
                            }

                            if (CommonFunctions.arePointsClose(edge2.point2, edge.point1) || CommonFunctions.arePointsClose(edge2.point2, edge.point2)) {
                                edge2.setPoint2(midpoint);
                            }
                        }

                        edge.setPoint1(midpoint);
                        edge.setPoint2(midpoint);

                        merged = true;
                        break;
                    }
                }

                if (merged) {
                    numberOfMerges++;
                    merged = false;
                } else {
                    break;
                }

            }
        }

        private void removeConcavePolygons() {
            List<int> PolygonIndicesToRemove = new List<int>();

            Polygon polygon;
            Point point1, point2, point3;

            Vector a, b;

            double angle;
            bool didRemove = false;
            for (int i = Polygons.Count - 1; i >= 0; i--) {
                polygon = Polygons[i];
                didRemove = false;
                for (int j = 1; j < polygon.Points.Count - 1; j++) {
                    point1 = polygon.Points[j - 1];
                    point2 = polygon.Points[j];
                    point3 = polygon.Points[j + 1];

                    a = new Vector(point2.X - point1.X, point2.Y - point1.Y);
                    b = new Vector(point3.X - point2.X, point3.Y - point2.Y);

                    angle = Vector.AngleBetween(a, b);

                    if (angle > Math.PI) {
                        didRemove = true;
                        Polygons.RemoveAt(i);
                        break;
                    }
                }

                if (!didRemove && CommonFunctions.areValuesClose(CommonFunctions.Area(polygon), 0)) {
                    didRemove = true;
                    Polygons.RemoveAt(i);
                }

            }
        }

        private static Point[] getPointsFromEdge(Voronoi2.GraphEdge edge) {
            Point point1 = new Point(edge.x1, edge.y1);
            Point point2 = new Point(edge.x2, edge.y2);

            return new Point[] { point1, point2 };
        }

		private List<Polygon> generatePolygonsFromGraphEdges() {
			List<Polygon> polygons = new List<Polygon>();

			SortedDictionary<int, HashSet<Voronoi2.GraphEdge>> edgesPerSite = new SortedDictionary<int, HashSet<Voronoi2.GraphEdge>>();

			SolidColorBrush blackBrush = new SolidColorBrush();
			blackBrush.Color = Colors.Black;

			SolidColorBrush yellowBrush = new SolidColorBrush();
			yellowBrush.Color = Colors.Yellow;

			Polygon polygon;
			//PointCollection polygonPoints;
			foreach (Voronoi2.GraphEdge edge in GraphEdges) {
                if (CommonFunctions.arePointsClose(new Point(edge.x1, edge.y1), new Point(edge.x2, edge.y2))) {
                    continue;
                }

                foreach (int site in new int[] { edge.site1, edge.site2 }) {
					if (!edgesPerSite.ContainsKey(site)) {
						edgesPerSite[site] = new HashSet<Voronoi2.GraphEdge>();
					}

					edgesPerSite[site].Add(edge);
				}

				//polygon = new Polygon();
				//polygonPoints = new PointCollection();
				//polygonPoints.Add(new Point(edge.x1, edge.y1));
				//polygonPoints.Add(new Point(edge.x2, edge.y2));
				//polygon.Points = polygonPoints;
				//polygon.Stroke = yellowBrush;
				//polygon.StrokeThickness = 4;
				//polygons.Add(polygon);
			}


			double distance, diffx, diffy;
			foreach (int site in edgesPerSite.Keys) {
				Point center = new Point(xValuesIn[site], yValuesIn[site]);
				diffx = center.X - centerX;
				diffy = center.Y - centerY;
				distance = diffx * diffx + diffy * diffy;
				CentersByDistanceFromCenter.Add(distance, center);

				polygon = getPolygonFromEdges(edgesPerSite[site]);
				polygon.Stroke = blackBrush;
				polygons.Add(polygon);
				PolygonsByCenter.Add(center, polygon);
			}

			return polygons;
		}

		private Polygon getPolygonFromEdges(HashSet<Voronoi2.GraphEdge> graphEdges) {
			Polygon polygon = new Polygon();
			PointCollection polygonPoints = new PointCollection();

			List<Voronoi2.GraphEdge> edges = new List<Voronoi2.GraphEdge>(graphEdges);
			Dictionary<Voronoi2.GraphEdge, Point[]> pointsByEdge = new Dictionary<Voronoi2.GraphEdge, Point[]>();

			Point point1, point2;
			foreach (Voronoi2.GraphEdge edge in edges) {
				point1 = new Point(edge.x1, edge.y1);
				point2 = new Point(edge.x2, edge.y2);
				pointsByEdge[edge] = new Point[] { point1, point2 };
				if (arePointsClose(point1, point2)) {
                    if (System.Diagnostics.Debugger.IsAttached)
                        System.Diagnostics.Debugger.Break();
                    CommonFunctions.output("Voronoi has a zero-length edge");
				}
			}

			LinkedList<Point> orderedPoints = new LinkedList<Point>();

			bool didBreak = false;
			Point firstPoint, lastPoint;
			while (edges.Count > 0) {
				foreach (Voronoi2.GraphEdge edge in edges) {
					point1 = pointsByEdge[edge][0];
					point2 = pointsByEdge[edge][1];

					if (arePointsClose(point1, point2)) {
						continue;
					}

					if (orderedPoints.Count == 0) {
						orderedPoints.AddFirst(point1);
						orderedPoints.AddLast(point2);
						edges.Remove(edge);
						didBreak = true;
						break;
					}

					firstPoint = orderedPoints.First();
					lastPoint = orderedPoints.Last();
					if (arePointsClose(point1, firstPoint)) {
						orderedPoints.AddFirst(point2);
						edges.Remove(edge);
						didBreak = true;
						break;
					} else if (arePointsClose(point2, firstPoint)) {
						orderedPoints.AddFirst(point1);
						edges.Remove(edge);
						didBreak = true;
						break;
					} else if (arePointsClose(point1, lastPoint)) {
						orderedPoints.AddLast(point2);
						edges.Remove(edge);
						didBreak = true;
						break;
					} else if (arePointsClose(point2, lastPoint)) {
						orderedPoints.AddLast(point1);
						edges.Remove(edge);
						didBreak = true;
						break;
					}
				}

				if (!didBreak) {
                    if (System.Diagnostics.Debugger.IsAttached)
                        System.Diagnostics.Debugger.Break();
                    CommonFunctions.output("Couldn't find an edge that connects to this one");
					break;
				} else {
					didBreak = false;
				}
			}

			Point prevPoint = new Point(0, 0);
			bool first = true;
			foreach (Point point in orderedPoints) {
				polygonPoints.Add(point);
				if (first) {
					first = false;
				} else {
					if (arePointsClose(point, prevPoint)) {
                        if (System.Diagnostics.Debugger.IsAttached)
                            System.Diagnostics.Debugger.Break();
                        CommonFunctions.output("For some reason the polygon edge has 0 length");
					}
				}
				prevPoint = point;

			}

			if (!arePointsClose(polygonPoints.First(), polygonPoints.Last())) {
				polygonPoints.Add(polygonPoints.First());
			}
			polygon.Points = polygonPoints;
			return polygon;
		}

		private static bool arePointsClose(Point point1, Point point2) {
			return CommonFunctions.areValuesClose(point1.X, point2.X) && CommonFunctions.areValuesClose(point1.Y, point2.Y);
		}
	}
}
