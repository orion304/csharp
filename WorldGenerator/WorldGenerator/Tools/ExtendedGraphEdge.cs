﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace WorldGenerator.Tools {
    public class ExtendedGraphEdge {

        public Voronoi2.GraphEdge edge;
        public Point point1;
        public Point point2;

        public ExtendedGraphEdge(Voronoi2.GraphEdge edge) {
            this.edge = edge;

            point1 = new Point(edge.x1, edge.y1);
            point2 = new Point(edge.x2, edge.y2);

        }

        public void setPoint1(Point point) {
            point1 = new Point(point.X, point.Y);
            edge.x1 = point.X;
            edge.y1 = point.Y;
        }

        public void setPoint2(Point point) {
            point2 = new Point(point.X, point.Y);
            edge.x2 = point.X;
            edge.y2 = point.Y;
        }

    }
}
