﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorldGenerator {
    public class Dice {

        public static int Roll(int number, int die) {
            int result = 0;
            for (int i = 0; i < number; i++) {
                result += Roll(die);
            }
            return result;
        }

        public static int Roll(int die) {
            return RandomSeed.rnd.Next(1, die + 1);
        }

        public static int d100() {
            return Roll(100);
        }

        public static int d20() {
            return Roll(20);
        }

        public static int d12() {
            return Roll(12);
        }

        public static int d10() {
            return Roll(10);
        }

        public static int d8() {
            return Roll(8);
        }

        public static int d6() {
            return Roll(6);
        }

        public static int d4() {
            return Roll(4);
        }

        public static int StandardAbilityScore() {
            return Roll(3, 6);
        }
    }
}
