﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;

namespace WorldGenerator.Tools {
	public class CommonFunctions {

        public static double sqftInAcre = 43560;
        public static double minDifference = 0.00000000001;

        public static void output(Object message, params Object[] objects) {
			Console.WriteLine(String.Format(message.ToString(), objects));
		}

        public static bool IsPointInPolygon4(Point testPoint, Polygon polygon) {
            PointCollection vertices = polygon.Points;

            bool result = false;
            int j = vertices.Count() - 1;
            for (int i = 0; i < vertices.Count(); i++) {
                if (vertices[i].Y < testPoint.Y && vertices[j].Y >= testPoint.Y || vertices[j].Y < testPoint.Y && vertices[i].Y >= testPoint.Y) {
                    if (vertices[i].X + (testPoint.Y - vertices[i].Y) / (vertices[j].Y - vertices[i].Y) * (vertices[j].X - vertices[i].X) < testPoint.X) {
                        result = !result;
                    }
                }
                j = i;
            }
            return result;
        }

		public static Point getIntersection(Point pointOnLine1, double slopeOfLine1, Point pointOnLine2, double slopeOfLine2) {

			double x = (slopeOfLine2 * pointOnLine2.X - slopeOfLine1 * pointOnLine1.X + pointOnLine1.Y - pointOnLine2.Y) / (slopeOfLine2 - slopeOfLine1);
			double y = slopeOfLine1 * (x - pointOnLine1.X) + pointOnLine1.Y;

			return new Point(x, y);
		}

		public static Point getIntersectPointOppositeEdge(Point pointOnLine1, double slopeOfLine1, Polygon polygon, int currentEdgeIndex, double[] slopesOfPolygon) {
			double bestDistance = 0;

			Point intersection, bestIntersection = new Point(-1, -1), midpoint;
			double diffX, diffY, distance;
			for (int i = 0; i < slopesOfPolygon.Length; i++) {
				if (i == currentEdgeIndex) {
					continue;
				}

				intersection = getIntersection(pointOnLine1, slopeOfLine1, polygon.Points[i], slopesOfPolygon[i]);
				diffX = intersection.X - pointOnLine1.X;
				diffY = intersection.Y - pointOnLine1.Y;


				midpoint = new Point(pointOnLine1.X + diffX / 2, pointOnLine1.Y + diffY / 2);

				if (IsPointInPolygon4(midpoint, polygon)) {
					distance = diffX * diffX + diffY * diffY;
					if (bestDistance == 0 || distance < bestDistance) {
						bestDistance = distance;
						bestIntersection = intersection;
					}
				}
			}

			return bestIntersection;
		}

        public static double Area(Polygon polygon) {
            PointCollection vertices = polygon.Points;
			return Math.Abs(vertices.Take(vertices.Count - 1).Select((p, i) => (p.X * vertices[i + 1].Y) - (p.Y * vertices[i + 1].X)).Sum() / 2);
        }

		public static void scalePolygon(Polygon polygon, double factor) {
			Point point;
			for (int i = 0; i < polygon.Points.Count; i++) {
				point = polygon.Points[i];
				point.X *= factor;
				point.Y *= factor;
				polygon.Points[i] = point;
			}
		}

		public static bool arePointsClose(Point point1, Point point2) {
			return getDistanceSquared(point1, point2) <= minDifference;
		}

		public static bool areValuesClose(double value1, double value2) {
			return Math.Abs(value1 - value2) <= minDifference;
		}

		public static double getDistanceSquared(Point point1, Point point2) {
            return getDistanceSquared(point1.X, point1.Y, point2.X, point2.Y);
		}

        public static double getDistanceSquared(double x, double y, Point point) {
            return getDistanceSquared(x, y, point.X, point.Y);
        }

        public static double getDistanceSquared(Point point, double x, double y) {
            return getDistanceSquared(x, y, point);
        }

        public static Point getMidpoint(Point point1, Point point2) {
            double dx = point2.X - point1.X;
            double dy = point2.Y - point1.Y;

            return new Point(point1.X + dx / 2, point1.Y + dy / 2);
        }

        public static double getDistanceSquared(double x1, double y1, double x2, double y2) {
            double dx = x1 - x2;
            double dy = y1 - y2;

            return dx * dx + dy * dy;
        }

		public static bool isPointOnLine(Point point, double slope, double yIntercept) {
			Point testPoint = new Point(point.X, slope * point.X + yIntercept);
			return arePointsClose(point, testPoint);
		}

		public static bool isPointOnLineSegment(Point point, double slope, double yIntercept, double minX, double maxX) {
			return (point.X >= minX && point.X <= maxX && isPointOnLine(point, slope, yIntercept));
		}

		public static int whichSideOfLineIsPointOn(Point startOfLine, Point endOfLine, Point pointToTest) {
			return Math.Sign((endOfLine.X - startOfLine.X) * (pointToTest.Y - startOfLine.Y) - (endOfLine.Y - startOfLine.Y) * (pointToTest.X - startOfLine.X));
		}

		public static void drawTestPoint(Point point, SolidColorBrush brush) {
			PointCollection points = new PointCollection();

			double size = 2;
			points.Add(new Point(point.X - size, point.Y - size));
			points.Add(new Point(point.X - size, point.Y + size));
			points.Add(new Point(point.X + size, point.Y + size));
			points.Add(new Point(point.X + size, point.Y - size));
			points.Add(points[0]);

			Polygon polygon = new Polygon();
			polygon.Points = points;
			polygon.Stroke = brush;

			MainWindow.WindowBox.Children.Add(polygon);
		}
	}
}
