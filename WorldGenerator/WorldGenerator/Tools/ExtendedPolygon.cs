﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;

namespace WorldGenerator.Tools {
	public class ExtendedPolygon {

		public Polygon polygon;
		public PointCollection points;
		public double[] slopes, yIntercepts, edgeLengths;
        public int longestEdgeIndex;

		public ExtendedPolygon(PointCollection points) {
			this.points = new PointCollection(points);

			polygon = new Polygon();
			polygon.Points = points;
			
			calculateLineFormulas();
		}

		public ExtendedPolygon(Polygon polygon) {
			this.polygon = polygon;
			points = new PointCollection(polygon.Points);

			calculateLineFormulas();
		}

		public ExtendedPolygon(ExtendedPolygon polygon) {
			points = new PointCollection(polygon.points);

			slopes = new double[polygon.slopes.Length];
			yIntercepts = new double[polygon.yIntercepts.Length];

			Array.Copy(polygon.slopes, slopes, slopes.Length);
			Array.Copy(polygon.yIntercepts, yIntercepts, yIntercepts.Length);
		}

		public bool isPointOnPolygon(Point point) {
			Point point1, point2;
			double slope, yIntercept;
			double smallerX, biggerX;
			for (int i = 0; i < slopes.Length; i++) {
				point1 = points[i];
				point2 = points[i + 1];
				slope = slopes[i];
				yIntercept = yIntercepts[i];

				if (point1.X < point2.X) {
					smallerX = point1.X;
					biggerX = point2.X;
				} else {
					smallerX = point2.X;
					biggerX = point1.X;
				}

				if (CommonFunctions.isPointOnLineSegment(point, slope, yIntercept, smallerX, biggerX)) {
					return true;
				}
			}

			return false;
		}

		private void calculateLineFormulas() {
			Point point1, point2;
			double slope, yIntercept, edgeLength;
            double longestEdgeLength = -1;

			int arrayLength = points.Count - 1;
			slopes = new double[arrayLength];
			yIntercepts = new double[arrayLength];
            edgeLengths = new double[arrayLength];
			for (int i = 0; i < arrayLength; i++) {
				point1 = points[i];
				point2 = points[i + 1];

				slope = (point1.Y - point2.Y) / (point1.X - point2.X);
				yIntercept = point1.Y - slope * point1.X;
                edgeLength = CommonFunctions.getDistanceSquared(point1, point2);

                if (edgeLength > longestEdgeLength) {
                    longestEdgeLength = edgeLength;
                    longestEdgeIndex = i;
                }

				slopes[i] = slope;
				yIntercepts[i] = yIntercept;
                edgeLengths[i] = edgeLength;

			}
		}

		public void draw() {
			polygon.Stroke = MainWindow.redBrush;
			MainWindow.WindowBox.Children.Add(polygon);

			//Line line = new Line();
			//Point fromPoint = points[longestEdgeIndex];
			//Point toPoint = points[longestEdgeIndex + 1];

			//line.X1 = fromPoint.X;
			//line.Y1 = fromPoint.Y;
			//line.X2 = toPoint.X;
			//line.Y2 = toPoint.Y;

			//line.Stroke = MainWindow.greenBrush;
			//line.StrokeThickness = 2;
			//MainWindow.WindowBox.Children.Add(line);
		}
	}
}
