﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorldGenerator {
    public class RandomSeed {

        public static Random rnd;

        static RandomSeed() {
            rnd = new Random();
        }

        public static T randomChoice<T>(T[] coll) {

            return coll[rnd.Next(coll.Length)];

        }

        public static T randomEnumValue<T>() {
            var v = Enum.GetValues(typeof(T));
            return (T)v.GetValue(rnd.Next(v.Length));
        }

		public static double randomDoubleBetween(double a, double b) {
			return rnd.NextDouble() * (b - a) + a;
		}
    }
}
