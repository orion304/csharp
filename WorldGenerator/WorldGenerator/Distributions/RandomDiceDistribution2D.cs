﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorldGenerator.Tools {
    public class RandomDiceDistribution2D<T, V> {

        private static HashSet<string> debug = new HashSet<string>();

		private Dictionary<T, RandomDiceDistribution<V>> dictionary = new Dictionary<T, RandomDiceDistribution<V>>();

		public void add(T key, int dice, V value) {
			if (!dictionary.ContainsKey(key)) {
				dictionary.Add(key, new RandomDiceDistribution<V>());
			}

            string test = value.ToString();
            int i = test.LastIndexOf(" ");
            test = test.Substring(0, i);
            if (!debug.Contains(test)) {
                debug.Add(test);
                CommonFunctions.output(test);
            }
            

			dictionary[key].setBound(dice, value);
		}

		public V randomObject(T key) {
			return dictionary[key].getRandomObject();
		}

    }
}
