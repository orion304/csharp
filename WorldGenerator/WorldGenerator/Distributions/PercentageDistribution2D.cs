﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorldGenerator.Distributions;

namespace WorldGenerator.Tools {
    public class PercentageDistribution2D<T, V> {
        
		private Dictionary<T, PercentageDistribution<V>> dictionary = new Dictionary<T, PercentageDistribution<V>>();

		public void add(T key, double percentage, V value) {
			if (!dictionary.ContainsKey(key)) {
				dictionary.Add(key, new PercentageDistribution<V>());
			}
            

			dictionary[key].setPercentage(percentage, value);
		}

		public V randomObject(T key) {
			return dictionary[key].getRandomObject();
		}

    }
}
