﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorldGenerator.Tools {
    public class RandomDiceDistribution<T> {

        private SortedDictionary<int, T> distributions = new SortedDictionary<int, T>();
        private int upperBound = 0;
        private bool isFullDictionaryInitialized = false;

		public void clear() {
			distributions.Clear();
		}

        public void setBound(int upperBound, T obj) {
            distributions.Add(upperBound, obj);
        }

        public T getRandomObject() {
            if (distributions.Count == 0) {
                return default(T);
            }

            if (!isFullDictionaryInitialized) {
                initializeDictionary();
            }

            int randomKey = RandomSeed.rnd.Next(1, upperBound);
            T returnObj;

            distributions.TryGetValue(randomKey, out returnObj);
            return returnObj;

        }

        private void initializeDictionary() {

            int lastKey = 1;
            int[] keys = distributions.Keys.ToArray();
            T obj;

            foreach (int key in keys) {

                distributions.TryGetValue(key, out obj);

                for (int newKey = lastKey; newKey < key; newKey++) {
                    distributions.Add(newKey, obj);
                }

                lastKey = key + 1;

            }

            upperBound = lastKey;

            Console.Out.WriteLine(distributions.Keys);

            isFullDictionaryInitialized = true;
        }

    }
}
