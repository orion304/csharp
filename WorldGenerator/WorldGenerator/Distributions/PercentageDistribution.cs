﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorldGenerator.NPC;

namespace WorldGenerator.Distributions {
    public class PercentageDistribution<T> {

        private SortedDictionary<double, T> distributions = new SortedDictionary<double, T>();
        private double upperBound = 0;

        public void setPercentage(double percentage, T obj) {
            if (percentage == 0) {
                return;
            }
            upperBound += percentage;
            distributions.Add(upperBound, obj);
        }

        public T getRandomObject() {
            if (distributions.Count == 0) {
                return default(T);
            }


            double randomKey = RandomSeed.rnd.NextDouble() * upperBound;

            foreach (double key in distributions.Keys) {
                if (randomKey <= key) {
                    return distributions[key];
                }
            }
            return distributions[distributions.Keys.Last()];

        }

        public void clear() {
            distributions.Clear();
            upperBound = 0;
        }

    }
}
