﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WorldGenerator {
    public class CityEnums {

        public enum CityMixType {
            Isolated,
            Mixed,
            Integrated
        }

		public enum BuildingQuality {
			A,
			B,
			C,
			D
		}

    }
}
