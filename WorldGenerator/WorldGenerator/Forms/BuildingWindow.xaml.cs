﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WorldGenerator.Cities;

namespace WorldGenerator.Forms {
    /// <summary>
    /// Interaction logic for BuildingWindow.xaml
    /// </summary>
    public partial class BuildingWindow : Window {

        private List<Building> selectedBuildings = new List<Building>();

        public MainWindow mainWindow { get; set; }

        public BuildingWindow(MainWindow window) {
            InitializeComponent();

            

            mainWindow = window;
            
            BuildingDataGrid.ItemsSource = window.city.Buildings;
            
        }

        

        public void deselectBuildings() {
            foreach (Building building in selectedBuildings) {
                deselectBuilding(building);
            }
            selectedBuildings.Clear();
        }

        public void deselectBuilding(Building building) {
            foreach (NPC.NPC npc in building.workers) {
                if (!selectedBuildings.Contains(npc.LivesAt)) {
                    npc.LivesAt.deselect();
                }
            }

            foreach (NPC.NPC worker in building.NPCs) {
                if (worker.LivesAt != worker.WorksAt && !selectedBuildings.Contains(worker.WorksAt)) {
                    worker.WorksAt.deselect();
                }
            }
            building.deselect();
        }

        public void selectBuilding(Building building) {
            selectedBuildings.Add(building);

            foreach (NPC.NPC npc in building.workers) {
                if (!selectedBuildings.Contains(npc.LivesAt)) {
                    npc.LivesAt.select(MainWindow.blueBrush);
                }
            }

            foreach (NPC.NPC worker in building.NPCs) {
                if (worker.LivesAt != worker.WorksAt && !selectedBuildings.Contains(worker.WorksAt)) {
                    worker.WorksAt.select(MainWindow.redBrush);
                }
            }

            building.select(MainWindow.yellowBrush);
        }

        private void BuildingDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e) {

            mainWindow.npcWindow?.deselectAll();
            deselectBuildings();

            foreach (Building building in BuildingDataGrid.SelectedItems) {
                selectBuilding(building);
            }

        }
    }
}
