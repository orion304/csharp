﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WorldGenerator.Cities;
using WorldGenerator.Forms;
using WorldGenerator.NPC;
using WorldGenerator.Tools;

namespace WorldGenerator {
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window {

        public static MainWindow currentInstance;

        public static Grid WindowBox;
        public static SolidColorBrush yellowBrush = new SolidColorBrush(Colors.Yellow);
		public static SolidColorBrush blackBrush = new SolidColorBrush(Colors.Black);
		public static SolidColorBrush blueBrush = new SolidColorBrush(Colors.Blue);
		public static SolidColorBrush redBrush = new SolidColorBrush(Colors.Red);
		public static SolidColorBrush greenBrush = new SolidColorBrush(Colors.Green);
		public static SolidColorBrush transparentBrush = new SolidColorBrush(Colors.Transparent);

        public NPCWindow npcWindow { get; set; }
        public BuildingWindow buildingWindow { get; set; }

        public City city { get; set; }

        public MainWindow() {
            
            InitializeComponent();

            currentInstance = this;
            WindowBox = LayoutRoot;

            double scale = .4;

            RandomSeed.rnd = new Random(10);
            city = new City("Orella", TownType.LARGETOWN, CityEnums.CityMixType.Mixed, Race.HUMAN, Alignment.NEUTRAL_GOOD);
            //test2();
            drawCityMap(city, scale);

            npcWindow = new NPCWindow(this);
            npcWindow.Show();

            buildingWindow = new BuildingWindow(this);
            buildingWindow.Show();

            test2();

		}

        private void test2() {

            Dictionary<string, int> buildingCount = new Dictionary<string, int>();

            foreach (Building building in city.Buildings) {

                if (!buildingCount.ContainsKey(building.BaseType)) {
                    buildingCount[building.BaseType] = 0;
                }

                buildingCount[building.BaseType] += 1;

            }

            foreach (string type in buildingCount.Keys) {
                CommonFunctions.output("{0}:\t{1}", type, buildingCount[type]);
            }
            

        }

		private void test() {

            foreach (Building building in city.Buildings) {
                if (building.Type == "House" || building.Type == "Tenement" || building.Type == "Guild House") {
                    if ((building.Quality == "Exceptional" || building.Quality == "Fine") && (building.NPCs[0].alignment == Alignment.LAWFUL_EVIL || building.NPCs.Count == 1)) {
                        CommonFunctions.output(building);
                        buildingWindow.selectBuilding(building);
                    }
                }
            }

        }
		

		private void drawCityMap(City city, double scale) {
			
			city.scaleWards(scale);
            
		}

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e) {
            npcWindow?.Close();
            buildingWindow?.Close();
        }
    }
}
