﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WorldGenerator.Cities;

namespace WorldGenerator {
    /// <summary>
    /// Interaction logic for NPCWindow.xaml
    /// </summary>
    public partial class NPCWindow : Window {

        private List<NPC.NPC> currentlySelectedNPCs = new List<NPC.NPC>();
        public MainWindow window { get; set; }

        public NPCWindow(MainWindow window) {
            InitializeComponent();

            this.window = window;

            NPCDataGrid.ItemsSource = window.city.NPCs;
            
        }

        public void deselectAll() {
            foreach (NPC.NPC currentNpc in currentlySelectedNPCs) {
                currentNpc.LivesAt.deselect();
                currentNpc.WorksAt.deselect();
            }

            currentlySelectedNPCs.Clear();
        }

        public void select(NPC.NPC npc) {
            currentlySelectedNPCs.Add(npc);
            npc.LivesAt.select(MainWindow.blueBrush);
            if (npc.LivesAt != npc.WorksAt) {
                npc.WorksAt.select(MainWindow.redBrush);
            }
        }

        private void NPCDataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e) {

            deselectAll();

            foreach (NPC.NPC selectedNpcs in NPCDataGrid.SelectedItems) {
                select(selectedNpcs);
            }

            
        }
    }
}
