﻿using Framework;
using System.Windows;
using System.Linq;

namespace PathfinderLookup {
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window {
		public MainWindow() {
			InitializeComponent();
			Test();
		}

		private void Test() {
			new DataRetriever();
			
		}
	}
}
