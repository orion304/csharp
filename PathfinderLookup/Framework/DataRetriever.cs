﻿using HtmlAgilityPack;
using System.Collections.Generic;
using System.Text;

namespace Framework {
	public class DataRetriever
    {

		private List<string> urls = new List<string>() {
			@"https://www.d20pfsrd.com/equipment/weapons",
			@"https://www.d20pfsrd.com/equipment/armor",
			@"https://www.d20pfsrd.com/equipment/goods-and-services/hunting-camping-survival-gear/",
			@"https://www.d20pfsrd.com/equipment/goods-and-services/herbs-oils-other-substances/",
			@"https://www.d20pfsrd.com/equipment/goods-and-services/animals-animal-gear/",
			@"https://www.d20pfsrd.com/equipment/goods-and-services/books-paper-writing-supplies/",
			@"https://www.d20pfsrd.com/equipment/goods-and-services/containers-bags-boxes-more/",
			@"https://www.d20pfsrd.com/equipment/goods-and-services/furniture-trade-goods-vehicles/",
			@"https://www.d20pfsrd.com/equipment/goods-and-services/hirelings-servants-services/",
			@"https://www.d20pfsrd.com/equipment/goods-and-services/tools-kits/",
			@"https://www.d20pfsrd.com/equipment/goods-and-services/toys-games-puzzles/",
			@"https://www.d20pfsrd.com/equipment/goods-and-services/technological-gear/"
		};

		private readonly HtmlWeb _webService;

		public DataRetriever() {

			_webService = new HtmlWeb();

			foreach (var url in urls) {
				var doc = _webService.Load(url);
				GetTablesFromDocument(doc);
			}
			
		}

		private IList<Table> GetTablesFromDocument(HtmlDocument document) {

			var results = new List<Table>();

			foreach (var tableNode in document.DocumentNode.SelectNodes("//table")) {

				var table = new Table();
				SubTable currentSubTable = null;
				TableHeader currentHeader = null;
				
				foreach (var tableHeaderBodyNode in tableNode.SelectNodes("thead | tbody")) {

					if (tableHeaderBodyNode.Name == "thead") {
						
						
						currentSubTable = new SubTable(table);
						currentHeader = new TableHeader(currentSubTable);
						currentSubTable.Header = currentHeader;

						foreach (var rowNode in tableHeaderBodyNode.SelectNodes("tr")) {
							currentHeader.Rows.Add(GetRowFromNode(rowNode, currentHeader, "th"));
						}

					} else if (tableHeaderBodyNode.Name == "tbody") {

						if (currentSubTable != null && currentHeader != null) {
							var body = new TableBody(currentSubTable);
							currentSubTable.Body = body;
							table.SubTables.Add(currentSubTable);

							foreach (var rowNode in tableHeaderBodyNode.SelectNodes("tr")) {
								body.Rows.Add(GetRowFromNode(rowNode, body, "td"));
							}
						}

					} else {
						if (System.Diagnostics.Debugger.IsAttached)
							System.Diagnostics.Debugger.Break();
					}

				}

			}



			return results;
		}

		private TableRow GetRowFromNode(HtmlNode node, TableSection owner, string columnNodeXpath) {

			var values = new List<string>();

			foreach (var columnNode in node.SelectNodes(columnNodeXpath)) {

				var text = new StringBuilder();
				foreach (var subNode in columnNode.ChildNodes) {
					text.Append(subNode.InnerText);
				}
				values.Add(text.ToString().Trim());

			}

			return new TableRow(owner, values);
		}

    }
}
