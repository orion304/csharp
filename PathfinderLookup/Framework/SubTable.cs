﻿namespace Framework {
	public class SubTable {

		public TableHeader Header { get; set; }

		public TableBody Body { get; set; }

		public Table Table { get; private set; }

		public SubTable(Table table) {
			Table = table;
		}

	}
}
