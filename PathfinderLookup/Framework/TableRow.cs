﻿using System.Collections.Generic;
using System.Linq;

namespace Framework {
	public class TableRow {

		public static SortedDictionary<string, TableRow> RowsIndexedByKey = new SortedDictionary<string, TableRow>();

		public static IEnumerable<KeyValuePair<string, TableRow>> GetResultsByKey(string key) {
			return RowsIndexedByKey.Where(kvp => kvp.Key.IndexOf(key, 0, System.StringComparison.CurrentCultureIgnoreCase) != -1);
		}

		public string Key { get; set; }

		public List<string> Values { get; set; } = new List<string>();

		public TableSection TableSection { get; private set; }

		public TableRow(TableSection tableSection, IList<string> values) {
			TableSection = tableSection;
			Values.AddRange(values);

			
			if (tableSection is TableBody) {
				var keyIndex = tableSection?.SubTable?.Header?.KeyIndex ?? 0;

				if (values.Count > keyIndex + 1)
				Key = values[keyIndex];

				RowsIndexedByKey[Key] = this;
			}
		}

	}
}
