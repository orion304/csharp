﻿using System.Collections.Generic;

namespace Framework {
	public class TableSection {

		public IList<TableRow> Rows { get; set; } = new List<TableRow>();

		public SubTable SubTable { get; private set; }

		public TableSection(SubTable subTable) {
			SubTable = subTable;
		}

		public virtual void AddTableRow(TableRow row) {
			Rows.Add(row);
		}

	}
}
