﻿using System.Collections.Generic;

namespace Framework {
	public class Table {

		public IList<SubTable> SubTables { get; set; } = new List<SubTable>();

	}
}
