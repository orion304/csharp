﻿using System.Collections.Generic;

namespace Framework {
	public class TableHeader : TableSection {

		public int KeyIndex { get; set; } = 0;

		public TableHeader(SubTable subTable) : base(subTable) {
		}

		public override void AddTableRow(TableRow row) {
			base.AddTableRow(row);

			if (row.Values.Count > 1 && row.Values[0].Contains("%d")) {
				KeyIndex = 1;
			}
		}
	}
}
