﻿using Framework;
using System.Collections.ObjectModel;

namespace ViewModels {
	public class TableViewModel {

		public ObservableCollection<ObservableCollection<string>> HeaderRows { get; set; } = new ObservableCollection<ObservableCollection<string>>();

		public ObservableCollection<ObservableCollection<string>> Rows { get; set; } = new ObservableCollection<ObservableCollection<string>>();


		internal void SetHeader(TableHeader header) {
			foreach (var row in header.Rows) {
				var rowValues = new ObservableCollection<string>();
				foreach (var value in row.Values) {
					rowValues.Add(value);
				}
				HeaderRows.Add(rowValues);
			}
		}
		
		internal void AddDisplayedRow(TableRow row) {
			var rowValues = new ObservableCollection<string>();
			foreach (var value in row.Values) {
				rowValues.Add(value);
			}
			Rows.Add(rowValues);
		}

	}
}
