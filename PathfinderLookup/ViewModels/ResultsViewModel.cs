﻿using Framework;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace ViewModels
{
    public class ResultsViewModel : INotifyPropertyChanged
    {

		private Dictionary<TableHeader, TableViewModel> ResultsByHeader = new Dictionary<TableHeader, TableViewModel>();

		public ObservableCollection<TableViewModel> Results { get; set; } = new ObservableCollection<TableViewModel>();

		private string _searchKey;
		public string SearchKey {
			get { return _searchKey; }
			set {
				_searchKey = value;
				GetResults();
				OnPropertyChanged();
			}
		}

		private void GetResults() {
			ResultsByHeader.Clear();
			Results.Clear();

			if (SearchKey.Length >= 3) {
				foreach (var result in TableRow.GetResultsByKey(SearchKey)) {

					if (ResultsByHeader.ContainsKey(result.Value.TableSection.SubTable.Header)) {
						ResultsByHeader[result.Value.TableSection.SubTable.Header].AddDisplayedRow(result.Value);
					} else {
						var tvm = new TableViewModel();
						tvm.SetHeader(result.Value.TableSection.SubTable.Header);
						tvm.AddDisplayedRow(result.Value);
						ResultsByHeader[result.Value.TableSection.SubTable.Header] = tvm;

						Results.Add(tvm);
					}

				}
			}
			
		}

		public event PropertyChangedEventHandler PropertyChanged;

		public void OnPropertyChanged([CallerMemberName] string propertyName = null) {
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}
