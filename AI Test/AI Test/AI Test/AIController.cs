﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static AI_Test.Enums;

namespace AI_Test {
    class AIController : GameController {

        private Random random = new Random();
        private AIControlType controlType;

        public AIController(string name, AIControlType controlType) : base(name) {
            this.controlType = controlType;
        }

        public override void takeTurn(Board board) {

            switch (controlType) {
                case AIControlType.Random:
                    takeTurn_Random(board);
                    break;
                case AIControlType.FirstTry:
                    takeTurn_FirstTry(board);
                    break;
            }

        }

        private void takeTurn_FirstTry(Board board) {

            int expBase = 3;

            int maxValue = (int) Math.Pow(expBase, board.boardLength);

            int boardState = 0;
            int power = 0;
            int value = 0;

            for (int x = 0; x < board.boardLength; x++) {
                for (int y = 0; y < board.boardLength; y++) {
                    GameController controller = board.getController(x, y);
                    if (controller == null) {
                        value = 0;
                    } else if (controller == this) {
                        value = 1;
                    } else {
                        value = 2;
                    }

                    boardState += (int)Math.Pow(expBase, power) * value;

                    power++;
                }
            }

        }

        private void takeTurn_Random(Board board) {
            List<Board.Point> list = new List<Board.Point>();

            foreach (Board.Point point in board.coordinates.Keys) {

                if (board.getController(point) == null) {
                    list.Add(point);
                }

            }

            board.takeControl(list[random.Next(list.Count)], this);
        }
    }
}
