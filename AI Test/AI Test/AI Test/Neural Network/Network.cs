﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static AI_Test.Neural_Network.NetworkEnums;

namespace AI_Test.Neural_Network {
    class Network {

        private static readonly double mutationChance = 0.01;
        public static Random random = new Random();

        private List<double[]> layers = new List<double[]>();
        private List<double[,]> weights = new List<double[,]>();
        private List<double[]> biases = new List<double[]>();

        private int numberOfInputs, numberOfOutputs;
        private int numberOfLayers;

        public double currentFitness = 0;
        public double totalFitness = 0;
        public int numberOfGames = 0;

        public Network(int numberOfInputs, int numberOfOutputs, params int[] neuronsInHiddenLayer) {

            //neuron layer is row vector (neurons in n layer)
            //weight is matrix (neurons in n layer x neurons in n+1 layer)
            //b is vector (neurons in n+1 layer)
            this.numberOfInputs = numberOfInputs;
            this.numberOfOutputs = numberOfOutputs;

            numberOfLayers = 2;

            AddLayer(numberOfInputs);
            if (neuronsInHiddenLayer != null) {
                numberOfLayers += neuronsInHiddenLayer.Length;
                for (int i = 0; i < neuronsInHiddenLayer.Length; i++) {
                    AddLayer(neuronsInHiddenLayer[i]);
                }
            }
            AddLayer(numberOfOutputs);

            GenerateRandomBiases();
            GenerateRandomWeights();
        }

        public Network(Network mom, Network dad, MixType mix) {

            //The network's number of inputs and outputs better match
            numberOfInputs = mom.numberOfInputs;
            numberOfOutputs = mom.numberOfOutputs;

            numberOfLayers = mom.numberOfLayers;

            for (int i = 0; i < numberOfLayers; i++) {
                AddLayer(mom.layers[i].Length);
            }

            PopulateWeightsFromParents(mom, dad, mix);
            PopulateBiasesFromParents(mom, dad, mix);

        }

        private void PopulateWeightsFromParents(Network mom, Network dad, MixType mix) {

            for (int i = 0; i < weights.Count; i++) {
                for (int j = 0; j < weights[i].GetLength(0); j++) {
                    for (int k = 0; k < weights[i].GetLength(1); k++) {
                        if (mix == MixType.RandomWithMutations && random.NextDouble() <= mutationChance) {
                            //mutate
                            weights[i][j, k] = random.NextDouble();
                        } else {
                            weights[i][j, k] = random.NextDouble() > .5 ? mom.weights[i][j, k] : dad.weights[i][j, k];
                        }
                    }
                }
            }

        }

        private void PopulateBiasesFromParents(Network mom, Network dad, MixType mix) {
            for (int i = 0; i < biases.Count; i++) {
                for (int j = 0; j < biases[i].Length; j++) {
                    if (mix == MixType.RandomWithMutations && random.NextDouble() <= mutationChance) {
                        //mutate
                        biases[i][j] = random.NextDouble() * 2 - 1;
                    } else {
                        biases[i][j] = random.NextDouble() > .5 ? mom.biases[i][j] : dad.biases[i][j];
                    }
                }
            }
        }

        private void GenerateRandomWeights() {
            for (int i = 0; i < weights.Count; i++) {
                for (int j = 0; j < weights[i].GetLength(0); j++) {
                    for (int k = 0; k < weights[i].GetLength(1); k++) {
                        weights[i][j, k] = random.NextDouble();
                    }
                }
            }
        }

        private void GenerateRandomBiases() {
            for (int i = 0; i < biases.Count; i++) {
                for (int j = 0; j < biases[i].Length; j++) {
                    biases[i][j] = random.NextDouble() * 2 - 1;
                }
            }
        }

        private void AddLayer(int numberOfNeurons) {
            int layerNumber = layers.Count;

            double[] layer = new double[numberOfNeurons];
            double[] layerBiases = new double[numberOfNeurons];
            layers.Add(layer);
            biases.Add(layerBiases);

            if (layerNumber > 0) {
                int neuronsInPreviousLayer = layers[layerNumber - 1].Length;

                weights.Add(new double[neuronsInPreviousLayer, numberOfNeurons]);
            }

        }

        private double Sigmoid(double x) {

            return 1 / (1 + Math.Exp(-x));
        }

        public double[] GetOutput(params double[] inputs) {
            if (inputs == null || inputs.Length != numberOfInputs) {
                return null;
            }

            double[] inputLayer = layers[0];
            for (int i = 0; i < numberOfInputs; i++) {
                inputLayer[i] = inputs[i];
            }

            double[] currentLayer = null;
            double[,] currentWeights;
            for (int i = 1; i < numberOfLayers; i++) {
                currentLayer = layers[i];
                Array.Clear(currentLayer, 0, currentLayer.Length);
                currentWeights = weights[i - 1];

                for (int k = 0; k < currentWeights.GetLength(1); k++) {
                    for (int j = 0; j < currentWeights.GetLength(0); j++) {
                        currentLayer[k] += inputLayer[j] * currentWeights[j, k];
                    }
                    currentLayer[k] = Sigmoid(currentLayer[k] - biases[i][k]);
                }

                inputLayer = currentLayer;
            }

            string result = "";
            for (int i = 0; i < currentLayer.Length; i++) {
                result += currentLayer[i] + ", ";
            }
            
            return currentLayer;
        }

    }
}
