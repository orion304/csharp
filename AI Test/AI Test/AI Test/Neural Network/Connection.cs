﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AI_Test.Neural_Network {
    class Connection {

        private Neuron from, to;
        private float weight;

        public Connection(Neuron from, Neuron to, float weight) {
            this.from = from;
            this.to = to;
            this.weight = weight;

            this.from.AddConnection(this);
        }

    }
}
