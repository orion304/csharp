﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AI_Test.Neural_Network {
    class Neuron {

        private List<Connection> connections;
        private float bias;

        public Neuron(float bias) {
            this.bias = bias;
        }

        internal void AddConnection(Connection connection) {
            connections.Add(connection);
        }

    }
}
