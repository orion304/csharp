﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AI_Test {
    class Board {

        public struct Point {
            public int x;
            public int y;
        }

        public static Point getPoint(int x, int y) {
            Point point = new Point();
            point.x = x;
            point.y = y;
            return point;
        }

        public readonly int boardLength;
        
        private readonly int minLineWinLength;
        private readonly int minDiagonalWinLength;

        private int movesTaken = 0;
        private readonly int maxMoves;

        public bool hasRemainingMoves = true;

        public Dictionary<Point, GameController> coordinates;

        public Board(int boardLength, int winLength) {
            this.boardLength = boardLength;
            minLineWinLength = winLength;
            minDiagonalWinLength = winLength;

            maxMoves = boardLength * boardLength;

            coordinates = new Dictionary<Point, GameController>();

            for (int x = 0; x < boardLength; x++) {
                for (int y = 0; y < boardLength; y++) {
                    Point point = new Point();
                    point.x = x;
                    point.y = y;

                    coordinates.Add(point, null);
                }
            }
        }

        public GameController getController(int x, int y) {
            return getController(getPoint(x, y));
        }

        public GameController getController(Point point) {
            return coordinates[point];
        }

        public void takeControl(int x, int y, GameController controller) {
            takeControl(getPoint(x, y), controller);
        }

        public void takeControl(Point point, GameController controller) {
            coordinates[point] = controller;
            movesTaken++;
            if (movesTaken >= maxMoves) {
                hasRemainingMoves = false;
            }
        }

        private GameController getLineWinner() {
            //Scan all horizontals and verticals for a winner
            GameController horPreviousController = null;
            GameController verPreviousController = null;
            GameController horController;
            GameController verController;
            int horLineLength = 0;
            int verLineLength = 0;
            for (int i = 0; i < boardLength; i++) {
                horPreviousController = null;
                verPreviousController = null;
                for (int j = 0; j < boardLength; j++) {
                    horController = getController(i, j);
                    verController = getController(j, i);

                    if (horController == null || horController != horPreviousController) {
                        horPreviousController = horController;
                        horLineLength = 1;
                    } else {
                        horLineLength++;
                    }

                    if (horLineLength >= minLineWinLength) {
                        //Console.WriteLine("Won vertically");
                        return horController;
                    }

                    if (verController == null || verController != verPreviousController) {
                        verPreviousController = verController;
                        verLineLength = 1;
                    } else {
                        verLineLength++;
                    }

                    if (verLineLength >= minLineWinLength) {
                        //Console.WriteLine("Won horizontally");
                        return verController;
                    }

                }
            }

            return null;
        }

        private GameController getDiagonalWinner() {

            //return null;

            GameController controller = getController(1, 1);

            if (controller != null) {
                if (controller == getController(0, 0) && controller == getController(2, 2)) {
                    //Console.WriteLine("Won diagonally left to right");
                    return controller;
                }

                if (controller == getController(2, 0) && controller == getController(0, 2)) {
                    //Console.WriteLine("Won diagonally right to left");
                    return controller;
                }
            }

            return null;
        }

        public GameController getWinner() {

            GameController winner = getLineWinner();

            if (winner != null) {
                return winner;
            }

            return getDiagonalWinner();


        }


    }
}
