﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AI_Test {
    class PlayerController : GameController {
        public PlayerController(string name) : base(name) {
        }

        public override void takeTurn(Board board) {

            GameController controller = null;

            for (int x = 0; x < board.boardLength; x++) {
                for (int y = 0; y < board.boardLength; y++) {
                    controller = board.getController(x, y);
                    if (controller == null) {
                        board.takeControl(x, y, this);
                        return;
                    }
                }
            }

        }
    }
}
