﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AI_Test {
    abstract class GameController {

        private static int lastId = 0;

        internal bool forfeited = false;
        protected readonly int id;
        public String name;

        public GameController(String name) {
            this.name = name;
            lastId++;
            id = lastId;
        }

        public abstract void takeTurn(Board board);

        protected void forfeit() {
            forfeited = true;
        }

    }
}
