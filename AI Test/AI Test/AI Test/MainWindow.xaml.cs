﻿using AI_Test.Neural_Network;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using static AI_Test.Neural_Network.NetworkEnums;

namespace AI_Test {
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window {

        private Dictionary<Board.Point, Button> displayArray = new Dictionary<Board.Point, Button>();

        private Game game;

        public MainWindow() {
            InitializeComponent();

            //Network network = new Network(9, 9, 9);
            //Network network2 = new Network(9, 9, 9);

            //network.GetOutput(1f, 0f);
            //network2.GetOutput(1f, 0f);

            //Network network3 = new Network(network, network2, MixType.Random);

            //network.GetOutput(1f, 0f);
            //network2.GetOutput(1f, 0f);
            //network3.GetOutput(1f, 0f);

            mainButOld();
        }

        private void mainButOld() {
            int boardLength = 3;
            int winLength = 3;
            int poolSize = 50;
            int keepBest = poolSize - 5;
            double slope = 3 * poolSize * poolSize / 2;
            int generations = 1000;
            int printEveryXGenerations = 50;
            int iterationsBeforeDisplay = 10000;

            int numOfTies = 0;
            int numOfWins = 0;

            createButtons(boardLength);

            List<NeuralNetworkController> geneticPool = new List<NeuralNetworkController>();
            List<NeuralNetworkController> newGeneticPool = new List<NeuralNetworkController>();
            for (int i = 0; i < poolSize; i++) {
                geneticPool.Add(new NeuralNetworkController(9));
            }

            for (int generation = 0; generation < generations; generation++) {
                //play each neural network against each other one
                GameController winner;
                for (int i = 0; i < poolSize; i++) {
                    for (int j = i + 1; j < poolSize; j++) {
                        winner = playFullGame(boardLength, winLength, geneticPool[i], geneticPool[j]);
                        geneticPool[i].newGame();
                        geneticPool[j].newGame();
                    }
                }

                //sort them by best fitness to worst
                geneticPool.Sort((x, y) => y.AverageFitness.CompareTo(x.AverageFitness));

                if (generation % printEveryXGenerations == 0) {
                    Console.WriteLine("Highest fitness: {0} \r\n Average fitness: {1}", geneticPool[0].AverageFitness, geneticPool.Average(x => x.AverageFitness));
                }

                //pair up 2 random networks, with a preference on the better fit ones
                newGeneticPool.Clear();
                int dadchoice;
                int momchoice;

                while (newGeneticPool.Count < keepBest) {
                    dadchoice = (int)(slope * Network.random.NextDouble() - poolSize);
                    NeuralNetworkController dad = geneticPool[dadchoice];

                    do {
                        momchoice = (int)(slope * Network.random.NextDouble() - poolSize);
                    } while (momchoice == dadchoice);

                    NeuralNetworkController mom = geneticPool[momchoice];

                    newGeneticPool.Add(new NeuralNetworkController(mom, dad, MixType.RandomWithMutations));

                }

                while (newGeneticPool.Count < poolSize) {
                    newGeneticPool.Add(new NeuralNetworkController(9));
                }

                geneticPool.Clear();
                geneticPool.AddRange(newGeneticPool);
                newGeneticPool.Clear();

            }

            //bool lastGame = false;

            //for (int i = 0; i < iterationsBeforeDisplay; i++) {

            //    lastGame = i == iterationsBeforeDisplay - 1;

            //    game = new Game(boardLength, winLength, controller1, controller2);

            //    GameController winner = null;
            //    while (game.hasRemainingMoves()) {
            //        game.playNextTurn();
            //        if (lastGame) {
            //            updateDisplay();
            //        }

            //        winner = game.getWinner();
            //        if (winner != null) {
            //            break;
            //        }
            //    }

            //    if (winner == null) {
            //        //Console.WriteLine("Tie!");
            //        numOfTies++;
            //    } else {
            //        //Console.WriteLine("Winner: " + winner.name);
            //        numOfWins++;
            //        if (lastGame) {
            //            displayWinner(winner.name);
            //        }
            //    }
            //}

            //Console.WriteLine("Number of ties: " + numOfTies.ToString());
            //Console.WriteLine("Number of wins: " + numOfWins.ToString());
        }

        private GameController playFullGame(int boardLength, int winLength, GameController controller1, GameController controller2) {
            controller1.forfeited = false;
            controller2.forfeited = false;
            Game game = new Game(boardLength, winLength, controller1, controller2);

            GameController winner = null;
            while (game.hasRemainingMoves()) {
                game.playNextTurn();

                winner = game.getWinner();
                if (winner != null) {
                    return winner;
                }
            }

            return null;
        }

        private void createButtons(int boardLength) {

            for (int x = 0; x < boardLength; x++) {
                grid.RowDefinitions.Add(new RowDefinition());
                grid.ColumnDefinitions.Add(new ColumnDefinition());
                for (int y = 0; y < boardLength; y++) {
                    
                    Button button = new Button();
                    button.Content = "";
                    button.SetValue(Grid.ColumnProperty, x);
                    button.SetValue(Grid.RowProperty, y);

                    displayArray.Add(Board.getPoint(x, y), button);

                    grid.Children.Add(button);
                }
            }
        }

        private void updateDisplay() {

            GameController controller = null;
            String name = "";

            for (int x = 0; x < game.board.boardLength; x++) {
                for (int y = 0; y < game.board.boardLength; y++) {
                    controller = game.board.getController(x, y);
                    name = controller == null ? "" : controller.name;
                    displayArray[Board.getPoint(x, y)].Content = name;
                }
            }

        }

        private void displayWinner(String winnerName) {
            foreach (Button button in displayArray.Values) {
                if (button.Content.ToString() == winnerName) {
                    button.FontWeight = FontWeights.Bold;
                }
            }
        }
    }

}
