﻿using AI_Test.Neural_Network;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static AI_Test.Neural_Network.NetworkEnums;

namespace AI_Test {
    class NeuralNetworkController : GameController {

        private static readonly double successfulTurn = 1;
        private static readonly double win = 12;

        public readonly Network network;

        public double AverageFitness {
            get {
                return network.totalFitness / network.numberOfGames;
            }
        }

        public NeuralNetworkController(params int[] neuronsInHiddenLayer) :
            base("") {
            network = new Network(9, 9, neuronsInHiddenLayer);

        }

        public NeuralNetworkController(NeuralNetworkController mom, NeuralNetworkController dad, MixType mix) :
            base("") {
            network = new Network(mom.network, dad.network, mix);

        }

        public void newGame() {
            network.totalFitness += network.currentFitness;
            network.currentFitness = 0;
            network.numberOfGames++;
        }

        public override void takeTurn(Board board) {
            double[] inputs = new double[9];

            double input;
            GameController controller;
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 3; j++) {
                    controller = board.getController(i, j);
                    if (controller == null) {
                        input = 0;
                    } else if (controller == this) {
                        input = 1;
                    } else {
                        input = 0;
                    }

                    inputs[i * 3 + j] = input;
                }
            }

            double[] output = network.GetOutput(inputs);

            double largestValue = 0;
            int largestIndex = 0;

            double value;
            for (int i = 0; i < output.Length; i++) {
                value = output[i];
                if (value > largestValue) {
                    largestIndex = i;
                    largestValue = value;
                }
            }

            int x = largestIndex / 3;
            int y = largestIndex % 3;

            controller = board.getController(x, y);

            if (controller == null) {
                board.takeControl(x, y, this);
                //encourage it to take good turns
                network.currentFitness += successfulTurn;

                if (board.getWinner() != null) {
                    //encourage it to win, but preferably in as few turns as possible
                    network.currentFitness = win - network.currentFitness;
                }
            } else {
                //made illegal move, kill it
                forfeit();
            }



            

        }
    }
}
