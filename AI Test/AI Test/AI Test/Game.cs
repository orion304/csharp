﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AI_Test {
    class Game {

        private static void Shuffle(List<GameController> list) {
            int n = list.Count;
            Random rnd = new Random();
            while (n > 1) {
                int k = (rnd.Next(0, n) % n);
                n--;
                GameController value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }

        public readonly Board board;
        private List<GameController> controllers = new List<GameController>();
        private int i = 0;

        public Game(int boardLength, int winLength, params GameController[] controllers) {

            foreach (GameController controller in controllers) {
                this.controllers.Add(controller);
            }

            Shuffle(this.controllers);

            board = new Board(boardLength, winLength);

        }

        public void playNextTurn() {
            if (!board.hasRemainingMoves) return;

            GameController controller = controllers[i];
            controller.takeTurn(board);

            i++;
            if (i >= controllers.Count) {
                i = 0;
            }

            //Thread.Sleep(1000);
        }

        private List<GameController> GetActivePlayers() {
            List<GameController> active = new List<GameController>();

            foreach (GameController controller in controllers) {
                if (!controller.forfeited) {
                    active.Add(controller);
                }
            }

            return active;
        }

        public GameController getWinner() {

            List<GameController> active = GetActivePlayers();
            if (active.Count == 1) {
                return null;
            } else if (active.Count == 0) {
                return null;
            }

            return board.getWinner();
        }

        public bool hasRemainingMoves() {
            if (GetActivePlayers().Count <= 1) {
                return false;
            }

            return board.hasRemainingMoves;
        }

    }
}
